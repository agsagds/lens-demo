﻿using LENSAnalytics.Model.STLModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace STLSlicer
{
    class Program
    {
        private static int ZLAYERS = 50;
        private static int ANGLESTEPS = 72;
        private static string outputPath = "slices.out";
        private static string inputPath = "model.stl";

        static void Main(string[] args)
        {
            if (args.Count() > 0)
                inputPath = args[0];
            if (args.Count() > 1)
                outputPath = args[1];
            if (args.Count() > 2)
                ZLAYERS = Convert.ToInt32(args[2]);
            if (args.Count() > 3)
                ANGLESTEPS = Convert.ToInt32(args[3]);

            STLModel model = Parser.ParseModel(inputPath);
            double minZ, maxZ;
            minZ = model.Triangles.OrderBy(t => t.Vertexes.OrderBy(v => v.Z).First().Z)
                    .First().Vertexes.OrderBy(v => v.Z).First().Z;
            maxZ = model.Triangles.OrderByDescending(t => t.Vertexes.OrderByDescending(v => v.Z).First().Z)
                    .First().Vertexes.OrderByDescending(v => v.Z).First().Z;

            StreamWriter writer = new StreamWriter(outputPath);
            writer.WriteLine("{0} {1}", ZLAYERS, ANGLESTEPS);
            for(double planeZ=minZ;planeZ<=maxZ;planeZ+=(maxZ-minZ)/ZLAYERS)
            {
                var lines = Slicer.GetPlaneAndTrianglesInrersections(planeZ, model.Triangles);
                writer.WriteLine("{0}", planeZ);
                for (double rayAngle = 0; rayAngle < Math.PI * 2; rayAngle += 2 * Math.PI / ANGLESTEPS)
                {
                    var points = Slicer.GetRayAndLineSegmentsIntersections(rayAngle, ref lines).OrderByDescending(p=>p.R);
                    writer.WriteLine("{0} {1}", rayAngle, points.Count());
                    foreach (var point in points)
                        writer.Write("{0} ",point.R);
                    writer.WriteLine();
                }                
            }
            writer.Flush();
            writer.Close();        
        }
    }
}
