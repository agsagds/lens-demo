﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Interfaces
{
    public interface IWordExporter
    {
        /// <summary>
        /// Вставка заголовка в конец файла
        /// </summary>
        /// <param name="text">текст заголовка</param>
        void PrintTitle(string text);

        /// <summary>
        /// Вставка подзаголовка в конец файла
        /// </summary>
        /// <param name="text">текст заголовка</param>
        void PrintSubtitle(string text);

        /// <summary>
        /// Вставка таблицы.
        /// Поддерживаются таблицы только с двумя или тремя столбцами
        /// </summary>
        /// <param name="header"></param>
        /// <param name="data"></param>
        void InsertTable(string[] header, IList<string[]> data);

        /// <summary>
        /// Вставка изображения в конец файла
        /// </summary>
        /// <param name="plot">Изображение</param>
        /// <param name="info">Подрисуночная подпись</param>
        void PrintPlot(Image plot, string info);

        /// <summary>
        /// Вставка изображения в конец файла
        /// </summary>
        /// <param name="plotimgpath">Путь к изображению</param>
        /// <param name="info">Подрисуночная подпись</param>
        void PrintPlot(string plotimgpath, string info);

        /// <summary>
        /// После вызова диалогового окна для выбора пути сохранения,
        /// сохраняет и закрывает текущий докумет Word.
        /// </summary>
        void CloseAndSave();
    }
}
