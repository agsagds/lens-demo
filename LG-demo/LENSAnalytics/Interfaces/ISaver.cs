﻿using LENSAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Interfaces
{
    public interface ISaver
    {
        void Save(string path, Gas gas, Laser laser, Material material, Melt melt, Powder powder);
    }
}
