﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Interfaces
{
    public interface ISavable
    {
        bool CanSave();
        void Save(string path);
        void Load(string path);
        string GetExtension();
    }
}
