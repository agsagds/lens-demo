﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using LENSAnalytics.Model.Geometry;
using LiveCharts;

namespace LENSAnalytics.Model.Calculated
{
    public interface IDrawable
    {
        /// <summary>
        /// Возвращает картину градиента 
        /// температуры осевого сечения
        /// </summary>
        /// <param name="angle">Угол, 0..pi</param>
        /// <param name="time">Время</param>
        /// <returns>Градиент темперауры осевого сечения</returns>
        BitmapSource GetCut(float angle, float time);

        IEnumerable<TempTimeCylindricalPoint> GetData();
    }
}
