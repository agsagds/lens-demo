﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Print = System.Drawing.Printing;


namespace LENSAnalytics.Printing
{
    class PrinterSaver
    {
        private PrintPreviewDialog ppd = new PrintPreviewDialog();
        private Print.PrintDocument pd= new Print.PrintDocument();
        private List<string[]> consist;
        private List<string[]> temperature;
        private List<string[]> extra;
        private string sval;
        private Font font;
        private Image img;
        private int page;

        public PrinterSaver(List<string[]> _consist, List<string[]> _temperature, List<string[]> _extra)
        {
            this.consist = _consist;
            this.temperature = _temperature;
            this.extra = _extra;
            pd.DocumentName = "Диаграмма";
            pd.PrintPage += new Print.PrintPageEventHandler(printPage);
            ppd.Document = pd;
            page = 0;
        }

       private void printPage( object sender, Print.PrintPageEventArgs e )
       {
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Far;
            e.Graphics.DrawString(DateTime.Now.ToString(), new Font(FontFamily.GenericMonospace, 8, FontStyle.Italic), Brushes.Black, e.MarginBounds.Width / 2 + 450, 2, sf);               
            float x = 40;
            float y = 50;
            if (page == 0)
            {
                // sf.Alignment = StringAlignment.Center;
                // e.Graphics.DrawString(pd.DocumentName, new Font(FontFamily.GenericSerif, 16, FontStyle.Bold) , Brushes.Black, e.MarginBounds.Width/2+100, e.MarginBounds.Y, sf);
              
                //состав
                font = new Font(FontFamily.GenericSerif, 14, FontStyle.Bold);
                e.Graphics.DrawString("Состав", font, Brushes.Black, x, y);
                y += this.font.Size + 6;
                float y_hello = y;
                font = new Font(FontFamily.GenericSerif, 12, FontStyle.Regular);
                for (int i = 0; i < consist.Count; i++)
                {
                    if (i % 6 == 0)
                        y_hello = y;
                    e.Graphics.DrawString(consist[i][0] + ": " + consist[i][1] + "%", font, Brushes.Black, x + 150 * (i / 6), y_hello, StringFormat.GenericTypographic);
                    y_hello += font.Size + 4;
                }
                if (consist.Count > 5)
                    y += (font.Size + 4) * 6;
                else y = y_hello;
                //таблица время/температура
                y += 25;
                font = new Font(FontFamily.GenericSerif, 14, FontStyle.Bold);
                e.Graphics.DrawString("Время/Температура", font, Brushes.Black, x, y);
                y += this.font.Size + 10;
                font = new Font(FontFamily.GenericSerif, 12, FontStyle.Regular);
                int half_of_page = 170;
                y_hello = y;
                Rectangle first, second;
                for (int i = 0; i < temperature.Count; i++)
                {
                    int bonus=(30 + half_of_page * 2) * (i / 25);
                    if (i % 25 == 0)
                    {
                        y_hello = y;
                        first = new Rectangle((int)x - 5 + bonus, (int)y_hello, half_of_page, (int)this.font.Size + 10);
                        second = new Rectangle((int)x + half_of_page - 5 + bonus, (int)y_hello, half_of_page, (int)this.font.Size + 10);
                        e.Graphics.DrawRectangles(Pens.Black, new Rectangle[] { first, second });
                        e.Graphics.DrawString("Момент времени, мин", font, Brushes.Black, x + bonus, y_hello + 2, StringFormat.GenericTypographic);
                        e.Graphics.DrawString("Температура, К", font, Brushes.Black, x + half_of_page + bonus, y_hello + 2, StringFormat.GenericTypographic);
                        y_hello += font.Size + 10;
                    }
                    first = new Rectangle((int)x - 5 + bonus, (int)y_hello, half_of_page, (int)this.font.Size + 10);
                    second = new Rectangle((int)x + half_of_page - 5 + bonus, (int)y_hello, half_of_page, (int)this.font.Size + 10);  
                    
                    e.Graphics.DrawRectangles(Pens.Black, new Rectangle[] { first, second });
                    e.Graphics.DrawString(temperature[i][0], font, Brushes.Black, x + bonus, y_hello + 2, StringFormat.GenericTypographic);
                    e.Graphics.DrawString(temperature[i][1], font, Brushes.Black, x + half_of_page + bonus, y_hello + 2, StringFormat.GenericTypographic);
                    y_hello += font.Size + 10;
                }
                if (temperature.Count > 24)
                    y += (font.Size + 10) * 26;
                else y = y_hello;
                //экстра
                y += 25;
                font = new Font(FontFamily.GenericSerif, 14, FontStyle.Bold);
                e.Graphics.DrawString("Параметры", font, Brushes.Black, x, y);
                y += this.font.Size + 6;               
                font = new Font(FontFamily.GenericSerif, 12, FontStyle.Regular);
                for (int i = 0; i < extra.Count; i++)
                {
                    e.Graphics.DrawString(extra[i][0] + ": " + extra[i][1], font, Brushes.Black, x, y, StringFormat.GenericTypographic);
                    y += font.Size + 4;
                }               
                y += 25;
               
            }
            //....
            if (y + 20 + img.Height + 20 > e.PageBounds.Height)
            {
                e.HasMorePages = true;
                page++;
                return;
            }
            //Значение доп параметра
            e.Graphics.DrawString(sval, font, Brushes.Black, e.PageBounds.Width / 2 - 50, y, StringFormat.GenericTypographic);
            y += 20;
            //Картинка
            e.Graphics.DrawImage(this.img, 0, y+5);
            e.HasMorePages = false;
            page = 0;
       }

       public void print(Bitmap _img, string _sval)
       {
           font = new Font(FontFamily.GenericSerif, 14, FontStyle.Regular);
           this.sval = _sval;
           this.img = _img;
           ppd.ShowDialog();   
       }
    }
}
