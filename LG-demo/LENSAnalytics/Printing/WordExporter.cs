﻿using Word = Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LENSAnalytics.Interfaces;

namespace LENSAnalytics.Printing
{
    public class WordExporter : IWordExporter
    {
        private WordDocument _worddoc;
        private int _countPictures;

        public WordExporter()
        {
            //Открываем ворд документ
            try
            {
                _worddoc = new WordDocument();
            }
            catch (Exception error)
            {
                MessageBox.Show("Ошибка при открытии шаблона Word. Подробности " + error.Message);
                return;
            }
            _worddoc.Visible = false;
            _countPictures = 0;
        }

        #region IWordExporter
        /// <summary>
        /// После вызова диалогового окна для выбора пути сохранения,
        /// сохраняет и закрывает текущий докумет Word.
        /// </summary>
        public void CloseAndSave()
        {
            if (!DocumentIsOpen())
                return;

            try
            {
                using (SaveFileDialog sfd = new SaveFileDialog())
                {

                    sfd.Filter = "Word97-2003 (*.doc)|*.doc|Word (*.docx)|*.docx";
                    sfd.FilterIndex = 1;
                    sfd.FileName = "Документ Word";
                    sfd.Title = "Выберите место для сохранения Word-файла";
                    if (sfd.ShowDialog() == DialogResult.OK) //если пользователь нажал ОК
                    {
                        Microsoft.Office.Interop.Word.WdSaveFormat format;
                        string path = sfd.FileName;
                        if (sfd.FilterIndex == 1)
                            format = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatDocument;
                        else
                            format = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatDocumentDefault;
                        //Записываем в файл по пути path и закрываем
                        _worddoc.Save(path, format);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Не удалось сохранить файл Word. Подробности " + e.StackTrace, "Ошибка сохранения");
            }
            finally
            {
                _worddoc.Close();
            }
        }               

        /// <summary>
        /// Вставка заголовка в конец файла
        /// </summary>
        /// <param name="text">текст заголовка</param>
        public void PrintTitle(string text)
        {
            if (!DocumentIsOpen())
                return;

            _worddoc.SelectEnd();
            InsertEmptyLine();
            _worddoc.InsertParagraphAfter(text);
            _worddoc.Selection.Bold = true;
            _worddoc.Selection.Aligment = TextAligment.Center;
            _worddoc.Selection.FontSize = 16;
            InsertEmptyLine();
        }
        /// <summary>
        /// Вставка заголовка в конец файла
        /// </summary>
        /// <param name="text">текст заголовка</param>
        public void PrintSubtitle(string text)
        {
            if (!DocumentIsOpen())
                return;

            _worddoc.SelectEnd();
            InsertEmptyLine();
            _worddoc.InsertParagraphAfter(text);
            _worddoc.Selection.Bold = true;
            _worddoc.Selection.Italic = true;
            _worddoc.Selection.Aligment = TextAligment.Center;
            _worddoc.Selection.FontSize = 12;
            InsertEmptyLine();  
        }

        /// <summary>
        /// Вставка таблицы.
        /// Поддерживаются таблицы только с двумя или тремя столбцами
        /// </summary>
        /// <param name="header"></param>
        /// <param name="data"></param>
        public void InsertTable(string[] header, IList<string[]> data)
        {
            if (!DocumentIsOpen())
                return;

            int countOfColumns = header.Count();
            switch (countOfColumns)
            {
                case 2:
                    AddTwoColumnTable(header, data);
                    break;
                case 3:
                    AddThreeColumnTable(header, data);
                    break;
                default:
                    throw new Exception("Некорректный формат данных для таблицы.");
            }
        }

        /// <summary>
        /// Вставка изображения в конец файла
        /// </summary>
        /// <param name="plot">Изображение</param>
        /// <param name="info">Подрисуночная подпись</param>
        public void PrintPlot(Image plot, string info)
        {
            if (!DocumentIsOpen())
                return;

            //Сохраним картинку на диск
            Stream strimg;
            string imgpath = Application.StartupPath + "\\calc\\temp.jpg";
            strimg = File.Create(Application.StartupPath + "\\calc\\temp.jpg");
            plot.Save(strimg, System.Drawing.Imaging.ImageFormat.Jpeg);
            strimg.Close();
            this.PrintPlot(imgpath, info);
            File.Delete(imgpath); //удалим картинку
        }

        /// <summary>
        /// Вставка изображения в конец файла
        /// </summary>
        /// <param name="plotimgpath">Путь к изображению</param>
        /// <param name="info">Подрисуночная подпись</param>
        public void PrintPlot(string plotimgpath, string info)
        {
            if (!DocumentIsOpen())
                return;

            _countPictures++;
            _worddoc.SelectEnd(); //каретку в конец         

            _worddoc.Selection.Aligment = TextAligment.Center; //выравнивание по центру
            _worddoc._currentRange.InlineShapes.AddPicture(plotimgpath);

            _worddoc._currentRange.MoveEnd();//Каретку в конец (.selectEnd - не работает в паре с InlineShapes)
            _worddoc.InsertParagraphAfter("Рис." + _countPictures + ". " + info); //запишем с новой строки
            _worddoc.Selection.FontSize = 12;    //размер шрифта
            _worddoc.Selection.Aligment = TextAligment.Center; //выравнивание по центру
        }

        #endregion

        private void InsertEmptyLine()
        {
            _worddoc.SelectEnd();
            _worddoc.InsertParagraphAfter(); // пустой
            _worddoc.Selection.Bold = false;
            _worddoc.Selection.Aligment = TextAligment.Center;
            _worddoc.Selection.FontSize = 1;
            _worddoc.SelectEnd();
        }

        private void AddTwoColumnTable(string[] tableHead, IList<string[]> tableData)
        {
            //добавим таблицу
            _worddoc.SelectEnd();
            _worddoc.InsertTable(tableData.Count + 1, 2);
            _worddoc.SetColumnWidth(1, 240);
            _worddoc.SetColumnWidth(2, 240);
            //заголовок          
            _worddoc.SetSelectionToCell(1, 1);
            _worddoc.Selection.Text = tableHead[0];
            _worddoc.Selection.Aligment = TextAligment.Center;
            _worddoc.Selection.FontSize = 12;
            _worddoc.Selection.Bold = true;
            _worddoc.SetSelectionToCell(1, 2);
            _worddoc.Selection.Text = tableHead[1];
            _worddoc.Selection.Aligment = TextAligment.Center;
            _worddoc.Selection.FontSize = 12;
            _worddoc.Selection.Bold = true;
            //данные           
            for (int i = 0; i < tableData.Count; i++)
            {
                _worddoc.SetSelectionToCell(i + 2, 1);
                _worddoc.Selection.Text = tableData[i][0];
                _worddoc.Selection.Bold = false;
                _worddoc.SetSelectionToCell(i + 2, 2);
                _worddoc.Selection.Text = tableData[i][1];
                _worddoc.Selection.Bold = false;
            }
        }

        private void AddThreeColumnTable(string[] tableHead, IList<string[]> tableData)
        {
            //добавим таблицу
            _worddoc.SelectEnd();
            _worddoc.InsertTable(tableData.Count + 1, 3);
            _worddoc.SetColumnWidth(1, 160);
            _worddoc.SetColumnWidth(2, 160);
            _worddoc.SetColumnWidth(3, 160);
            //заголовок           
            _worddoc.SetSelectionToCell(1, 1);
            _worddoc.Selection.Text = tableHead[0];
            _worddoc.Selection.Aligment = TextAligment.Center;
            _worddoc.Selection.FontSize = 12;
            _worddoc.Selection.Bold = true;
            _worddoc.SetSelectionToCell(1, 2);
            _worddoc.Selection.Text = tableHead[1];
            _worddoc.Selection.Aligment = TextAligment.Center;
            _worddoc.Selection.FontSize = 12;
            _worddoc.Selection.Bold = true;
            _worddoc.SetSelectionToCell(1, 3);
            _worddoc.Selection.Text = tableHead[2];
            _worddoc.Selection.Aligment = TextAligment.Center;
            _worddoc.Selection.FontSize = 12;
            _worddoc.Selection.Bold = true;
            //данные            
            for (int i = 0; i < tableData.Count; i++)
            {
                _worddoc.SetSelectionToCell(i + 2, 1);
                _worddoc.Selection.Text = tableData[i][0];
                _worddoc.SetSelectionToCell(i + 2, 2);
                _worddoc.Selection.Text = tableData[i][1];
                _worddoc.SetSelectionToCell(i + 2, 3);
                _worddoc.Selection.Text = tableData[i][2];
            }
        }

        public bool DocumentIsOpen()
        {
            return _worddoc != null && !_worddoc.Closed;
        }
    }
}
