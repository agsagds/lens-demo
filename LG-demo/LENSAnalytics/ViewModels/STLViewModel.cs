﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Interfaces;
using LENSAnalytics.Model.STLModel;
using LENSAnalytics.MVVM;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.ViewModels
{
    public class STLViewModel : NotifyErrorsModel, ISavable
    {
        private string _baseModelPath;
        private string _nozzleModelPath;
        private int _zLayersCountBase = 60;
        private int _angleStepsCountBase = 60;
        private int _zLayersCountNozzle = 60;
        private int _angleStepsCountNozzle = 60;
        #region Constants
        private static string AdditionParametersExtension = ".config";
        public static string ModelExtension = ".stl";
        public static string DefaultBaseModelName = "base";
        public static string DefaultNozzleModelName = "nozzle";
        #endregion
        public STLViewModel()
        {
            SetBaseModelPathCommand = new RelayCommand(SetBaseModelPathExecuted);
            SetNozzleModelPathCommand = new RelayCommand(SetNozzleModelPathExecuted);
            LoadSliceParameters();
        }

        private void LoadSliceParameters()
        {
            string path = Resources.ConfigFolderName+"\\"+ModelExtension + DefaultBaseModelName+ AdditionParametersExtension;
            if (File.Exists(path))
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.UTF8))
                {
                    var line = sr.ReadLine();
                    line = line.Substring(0, line.IndexOf('#'));
                    _zLayersCountBase = int.Parse(line);
                    line = sr.ReadLine();
                    line = line.Substring(0, line.IndexOf('#'));
                    _angleStepsCountBase = int.Parse(line);
                    sr.Close();
                }
            }
            path = Resources.ConfigFolderName + "\\" + ModelExtension + DefaultNozzleModelName + AdditionParametersExtension;
            if (File.Exists(path))
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.UTF8))
                {
                    var line = sr.ReadLine();
                    line = line.Substring(0, line.IndexOf('#'));
                    _zLayersCountNozzle = int.Parse(line);
                    line = sr.ReadLine();
                    line = line.Substring(0, line.IndexOf('#'));
                    _angleStepsCountNozzle = int.Parse(line);
                    sr.Close();
                }
            }
        }

        public void SliceModel(string modelPath, string slicedModelPath, ModelType type)
        {
            int zLayersCount, angleStepsCount;
            switch(type)
            {
                case ModelType.Base:
                    zLayersCount = _zLayersCountBase;
                    angleStepsCount = _angleStepsCountBase;
                    break;
                case ModelType.Nozzle:
                    zLayersCount = _zLayersCountNozzle;
                    angleStepsCount = _angleStepsCountNozzle;
                    break;
                default:
                    throw new ArgumentException(nameof(type));
            }

            STLModel model = Parser.ParseModel(modelPath);
            double minZ, maxZ, offset;
            minZ = model.Triangles.Min(t => t.Vertexes.Min(v => v.Z));              
            maxZ = model.Triangles.Max(t => t.Vertexes.Max(v => v.Z));
            offset = (maxZ - minZ) / (zLayersCount * 2);
            minZ += offset;
            maxZ -= offset;

            using (StreamWriter writer = new StreamWriter(slicedModelPath))
            {
                writer.WriteLine("{0} {1}", zLayersCount, angleStepsCount);
                var zStep = (maxZ - minZ) / (zLayersCount - 1);
                for (int i=0;i< zLayersCount; i++)
                {
                    var planeZ = minZ + i * zStep;
                    var lines = Slicer.GetPlaneAndTrianglesInrersections(planeZ, model.Triangles);
                    writer.WriteLine("{0}", planeZ);
                    var rayAngleStep = 2 * Math.PI / (angleStepsCount - 1);
                    for (double j = 0; j < angleStepsCount; j++)
                    {
                        var rayAngle = j * rayAngleStep;
                        var points = Slicer.GetRayAndLineSegmentsIntersections(rayAngle, ref lines).OrderByDescending(p => p.R);
                        writer.WriteLine("{0} {1}", rayAngle, points.Count());
                        foreach (var point in points)
                            writer.Write("{0} ", point.R);
                        writer.WriteLine();
                    }
                }
                writer.Flush();
                writer.Close();
            }
        }

        public void CopyBaseModel(string destinationPath)
        {
            File.Copy(_baseModelPath, destinationPath+DefaultBaseModelName+ModelExtension, true);
        }

        public void CopyNozzleModel(string destinationPath)
        {
            File.Copy(_nozzleModelPath, destinationPath+DefaultNozzleModelName+ModelExtension, true);
        }

        #region Commands
        public RelayCommand SetBaseModelPathCommand { get; private set; }
        public RelayCommand SetNozzleModelPathCommand { get; private set; }
        #endregion
        #region Properties
        /// <summary>
        /// Путь к файлу модели детали
        /// </summary>
        [Required]
        public string BaseModelPath
        {
            set
            {
                _baseModelPath = value;
                NotifyPropertyChanged();
            }
            get
            {
                if (_baseModelPath == null)
                    return null;

                return _baseModelPath.Substring(_baseModelPath.LastIndexOf('\\') + 1);
            }
        }
        /// <summary>
        /// Путь к файлу модели сопла
        /// </summary>
        [Required]
        public string NozzleModelPath
        {
            set
            {
                _nozzleModelPath = value;
                NotifyPropertyChanged();
            }
            get
            {
                if (_nozzleModelPath == null)
                    return null;

                return _nozzleModelPath.Substring(_nozzleModelPath.LastIndexOf('\\') + 1);
            }
        }
        #endregion
        #region ISavable
        public bool CanSave()
        {
            NotifyProperties();
            ValidateProperties();
            return !HasErrors;
        }

        public void Load(string path)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Unicode))
                {
                    Dictionary<string, string> records = new Dictionary<string, string>();
                    while (!sr.EndOfStream)
                    {
                        var record = sr.ReadLine();
                        var key = record.Substring(0, record.IndexOf(':'));
                        var value = record.Substring(record.IndexOf(':') + 1);
                        records[key] = value;
                    }
                    BaseModelPath = records[nameof(BaseModelPath)];
                    NozzleModelPath = records[nameof(NozzleModelPath)];
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                if (ex is IOException || ex is KeyNotFoundException || ex is IndexOutOfRangeException || ex is FormatException)
                {
                    throw new DataLoadException();
                }
                throw;
            }
            NotifyProperties();
            ValidateProperties();
        }

        public void Save(string path)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Unicode))
                {
                    sw.WriteLine("{0}:{1}", nameof(BaseModelPath), _baseModelPath);
                    sw.WriteLine("{0}:{1}", nameof(NozzleModelPath), _nozzleModelPath);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    throw new DataSaveException();
                }
                throw;
            }
        }

        public string GetExtension() => ".modelinfo";
        #endregion

        private void SetBaseModelPathExecuted(object obj)
        {
            string extension = ModelExtension;
            string filter = string.Format("Файл модели(*{0}) | *{0}", extension);
            string path = LoadFile(extension, filter);
            if (path == null)
                return;

            BaseModelPath = path;
        }

        private void SetNozzleModelPathExecuted(object obj)
        {
            string extension = ModelExtension;
            string filter = string.Format("Файл модели(*{0}) | *{0}", extension);
            string path = LoadFile(extension, filter);
            if (path == null)
                return;

            NozzleModelPath = path;
        }

        private string LoadFile(string extention, string filter)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = extention;
            dlg.Filter = filter;
            return dlg.ShowDialog() == true ? dlg.FileName : null;
        }

        private void NotifyProperties()
        {
            RaisePropertyChanged(nameof(BaseModelPath));
            RaisePropertyChanged(nameof(NozzleModelPath));
        }

        private void ValidateProperties()
        {
            ValidateModelProperty(this, BaseModelPath, nameof(BaseModelPath));
            ValidateModelProperty(this, NozzleModelPath, nameof(NozzleModelPath));
        }

    }

    public enum ModelType
    {
        Base,
        Nozzle
    }
}
