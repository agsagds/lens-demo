﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace LENSAnalytics.ViewModels
{
    public class MaterialViewModel : InputDataBaseViewModel
    {
        private Material _model;

        public MaterialViewModel(Material model) : base(model)
        {
            _model = model;
        }

        /// <summary>
        /// Теплопроводность
        /// </summary>
        public ObservableCollection<Parameter> Transcalency { get { return _model.Transcalency; } }
        /// <summary>
        /// Теплоёмкость
        /// </summary>
        public ObservableCollection<Parameter> HeatCapacity { get { return _model.HeatCapacity; } }
        /// <summary>
        /// Плотность
        /// </summary>
        public ObservableCollection<Parameter> Density { get { return _model.Density; } }
        /// <summary>
        /// Степень черноты поверхности
        /// </summary>
        public decimal? Emissivity
        {
            get { return _model.Emissivity; }
            set
            {
                _model.Emissivity = value;
                NotifyPropertyChanged();
            }
        }

        public override bool CanSave()
        {
            NotifyProperties();            
            ValidateProperties();
            return base.CanSave();
        }

        public override void Load(string path)
        {
            base.Load(path);          
            NotifyProperties();
            ValidateProperties();
        }

        private void NotifyProperties()
        {
            RaisePropertyChanged(nameof(Emissivity));
        }

        private void ValidateProperties()
        {
            ValidateModelProperty(_model, _model.Emissivity, nameof(_model.Emissivity));
        }
    }
}
