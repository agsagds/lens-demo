﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model;
using LENSAnalytics.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace LENSAnalytics.ViewModels
{
    public class GasViewModel : InputDataBaseViewModel
    {
        private Gas _model;

        public GasViewModel(Gas model):base(model)
        {
            _model = model;
        }

        /// <summary>
        /// Температура
        /// </summary>
        public decimal? Temperature
        {
            get { return _model.Temperature; }
            set
            {
                _model.Temperature = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Вязкость
        /// </summary>
        public decimal? Viscosity
        {
            get { return _model.Viscosity; }
            set
            {
                _model.Viscosity = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Теплопроводность
        /// </summary>
        public decimal? Transcalency
        {
            get { return _model.Transcalency; }
            set
            {
                _model.Transcalency = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Теплоёмкость
        /// </summary>
        public decimal? HeatCapacity
        {
            get { return _model.HeatCapacity; }
            set
            {
                _model.HeatCapacity = value;
                NotifyPropertyChanged();
            }
        }

        public override bool CanSave()
        {
            NotifyProperties();
            ValidateProperties();
            return base.CanSave();
        }

        public override void Load(string path)
        {
            base.Load(path);
            NotifyProperties();
            ValidateProperties();
        }

        private void NotifyProperties()
        {
            RaisePropertyChanged(nameof(Temperature));
            RaisePropertyChanged(nameof(Viscosity));
            RaisePropertyChanged(nameof(Transcalency));
            RaisePropertyChanged(nameof(HeatCapacity));
        }

        private void ValidateProperties()
        {
            ValidateModelProperty(_model, _model.Temperature, nameof(_model.Temperature));
            ValidateModelProperty(_model, _model.Viscosity, nameof(_model.Viscosity));
            ValidateModelProperty(_model, _model.Transcalency, nameof(_model.Transcalency));
            ValidateModelProperty(_model, _model.HeatCapacity, nameof(_model.HeatCapacity));
        }
    }
}
