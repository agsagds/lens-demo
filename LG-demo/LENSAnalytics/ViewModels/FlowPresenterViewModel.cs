﻿using LENSAnalytics.Model;
using LENSAnalytics.Model.Calculated;
using LENSAnalytics.MVVM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace LENSAnalytics.ViewModels
{
    public class FlowPresenterViewModel : DrawableWindowViewModel
    {
        private IDrawable _gasFlow;
        private IDrawable _powderFlow;
        private bool _flowPowderType;
        private const string SaveImagePath = "temp.jpg";

        public FlowPresenterViewModel(IDrawable gasFlow, IDrawable powderFlow, string title):base(title)
        {
            _gasFlow = gasFlow;
            _powderFlow = powderFlow;
            //Propetries
            var data = gasFlow.GetData().Union(powderFlow.GetData());
            Angle = new MinMaxValue(data.Min(p => p.Angle), data.Max(p => p.Angle));
            Time = new MinMaxValue(data.Min(p => p.Time), data.Max(p => p.Time));
            Angle.ValueChanged += UpdateChartData;
            Time.ValueChanged += UpdateChartData;
            //get first image
            UpdateChartData();
        }
           

        public bool FlowType
        {
            get { return _flowPowderType; }
            set
            {
                if (_flowPowderType == value)
                    return;
                _flowPowderType = value;
                NotifyPropertyChanged();
                UpdateChartData();
            }
        }

        #region Properties
        public MinMaxValue Angle { get; private set; }
        public MinMaxValue Time { get; private set; }
        #endregion


        public BitmapSource CutImage { get; private set; }

        public override void UpdateChartData()
        {
            try
            {
                if (_flowPowderType)
                    CutImage=_powderFlow.GetCut(Angle.Value, Time.Value);
                else
                    CutImage= _gasFlow.GetCut(Angle.Value, Time.Value);
                NotifyPropertyChanged(nameof(CutImage));               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override void AddToWordExecuted(object obj)
        {
            var source = obj as BitmapSource;
            if (source == null)
                return;

            using (var fileStream = new FileStream(SaveImagePath, FileMode.Create))
            {
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(source));
                encoder.Save(fileStream);
            }

            StringBuilder comment = new StringBuilder(Title);
            comment.Append((_flowPowderType ? " порошка. " : " газа. "));
            comment.AppendFormat("Угол(рад): {0:0.00} ", Angle.Value);
            //comment.AppendFormat("Время, с: {0:e2} ", Time.Value);

            AddImageToWord(SaveImagePath, comment.ToString());
            File.Delete(SaveImagePath);
        }
    }
}
