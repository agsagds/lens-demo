﻿using LENSAnalytics.Model;
using LENSAnalytics.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.ViewModels
{
    public abstract class DrawableWindowViewModel : NotifyModel
    {
        #region Events
        public delegate void AddImageEventHandler(string imagePath, string comment);
        public event AddImageEventHandler AddImageInstance;
        #endregion

        public DrawableWindowViewModel(string title)
        {
            Title = title;
            AddToWordCommand = new RelayCommand(AddToWordExecuted);
        }

        #region Commands
        public RelayCommand AddToWordCommand { get; private set; }
        #endregion

        #region Properties
        public string Title { get; private set; }
        #endregion

        public abstract void UpdateChartData();

        protected abstract void AddToWordExecuted(object obj);

        protected void AddImageToWord(string imagePath, string comment)
        {
            AddImageInstance.Invoke(imagePath, comment);
        }
    }
}
