﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.ViewModels
{
    public class PowderFlowViewModel : InputDataBaseViewModel
    {
        private PowderFlow _model;

        public PowderFlowViewModel(PowderFlow model) : base(model)
        {
            _model = model;
        }

        /// <summary>
        /// Функция распределения частиц при подаче
        /// </summary>
        public ObservableCollection<Parameter> SpreadFunction { get { return _model.SpreadFunction; } }
        /// <summary>
        /// Начальная температура
        /// </summary>
        public decimal? InitialTemperature
        {
            get { return _model.InitialTemperature; }
            set
            {
                _model.InitialTemperature = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Массовый поток частиц в точке подачи
        /// </summary>
        public decimal? MassFlow
        {
            get { return _model.MassFlow; }
            set
            {
                _model.MassFlow = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Средняя подача порошка в еденицу времени
        /// </summary>
        public decimal? MidFlow
        {
            get { return _model.MidFlow; }
            set
            {
                _model.MidFlow = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Средней характерный радиус разлета частиц
        /// </summary>
        public decimal? MidDiffRadius
        {
            get { return _model.MidDiffRadius; }
            set
            {
                _model.MidDiffRadius = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Коэффициент, характеризующий степень разлета
        /// </summary>
        public decimal? SpreadValue
        {
            get { return _model.SpreadValue; }
            set
            {
                _model.SpreadValue = value;
                NotifyPropertyChanged();
            }
        }

        public override bool CanSave()
        {
            NotifyProperties();
            ValidateProperties();
            return base.CanSave();
        }      

        public override void Load(string path)
        {            
            base.Load(path);
            AfterLoad();
        }

        public void AfterLoad()
        {
            NotifyProperties();
            ValidateProperties();
            AdditionParameters.Clear();
            foreach (var parameter in _model.AdditionParameters)
                AdditionParameters.Add(new ReservDataViewModel(parameter));
        }

        private void NotifyProperties()
        {
            RaisePropertyChanged(nameof(InitialTemperature));
            RaisePropertyChanged(nameof(MassFlow));
            RaisePropertyChanged(nameof(MidFlow));
            RaisePropertyChanged(nameof(MidDiffRadius));
            RaisePropertyChanged(nameof(SpreadValue));
        }

        private void ValidateProperties()
        {
            ValidateModelProperty(_model, _model.InitialTemperature, nameof(_model.InitialTemperature));
            ValidateModelProperty(_model, _model.MassFlow, nameof(_model.MassFlow));
            ValidateModelProperty(_model, _model.MidFlow, nameof(_model.MidFlow));
            ValidateModelProperty(_model, _model.MidDiffRadius, nameof(_model.MidDiffRadius));
            ValidateModelProperty(_model, _model.SpreadValue, nameof(_model.SpreadValue));
        }
    }
}
