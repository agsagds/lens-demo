﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.ViewModels
{
    public class PowderViewModel : InputDataBaseViewModel
    {
        private Powder _model;

        public PowderViewModel(Powder model) : base(model)
        {
            _model = model;
            Flow = new PowderFlowViewModel(_model.Flow);
        }
        /// <summary>
        /// Параметры потока частиц порошка
        /// </summary>
        public PowderFlowViewModel Flow { get; private set; }        
        /// <summary>
        /// Конвективный коэффициент теплоотдачи поверхности в жидком состоянии
        /// </summary>
        public decimal? LiquidConvectionHeatTransfer
        {
            get { return _model.LiquidConvectionHeatTransfer; }
            set
            {
                _model.LiquidConvectionHeatTransfer = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Конвективный коэффициент теплоотдачи поверхности в твёрдом состоянии
        /// </summary>
        public decimal? ConvectionHeatTransfer
        {
            get { return _model.ConvectionHeatTransfer; }
            set
            {
                _model.ConvectionHeatTransfer = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Удельная теплота плавления
        /// </summary>
        public decimal? HeatMelting
        {
            get { return _model.HeatMelting; }
            set
            {
                _model.HeatMelting = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Теплопроводность в жидком состоянии
        /// </summary>
        public decimal? LiquidTranscalency
        {
            get { return _model.LiquidTranscalency; }
            set
            {
                _model.LiquidTranscalency = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Вязкость в жидком состоянии
        /// </summary>
        public decimal? LiquidViscosity
        {
            get { return _model.LiquidViscosity; }
            set
            {
                _model.LiquidViscosity = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Температура плавления
        /// </summary>
        public decimal? MeltingPoint
        {
            get { return _model.MeltingPoint; }
            set
            {
                _model.MeltingPoint = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Состав частиц: диаметр, доля
        /// </summary>
        public ObservableCollection<Parameter> ParticlesComposition { get { return _model.ParticlesComposition; } }
        /// <summary>
        /// Плотность
        /// </summary>
        public ObservableCollection<Parameter> Density { get { return _model.Density; } }
        /// <summary>
        /// Степень черноты поверхности
        /// </summary>
        public ObservableCollection<Parameter> Emissivity { get { return _model.Emissivity; } }
        /// <summary>
        /// Теплопроводность
        /// </summary>
        public ObservableCollection<Parameter> Transcalency { get { return _model.Transcalency; } }
        /// <summary>
        /// Теплоёмкость
        /// </summary>
        public ObservableCollection<Parameter> HeatCapacity { get { return _model.HeatCapacity; } }

        public override bool CanSave()
        {
            NotifyProperties();
            ValidateProperties();
            bool flowStatus = Flow.CanSave();
            return base.CanSave() && flowStatus;
        }

        public override void Load(string path)
        {
            base.Load(path);            
            NotifyProperties();
            ValidateProperties();

            Flow.AfterLoad();
        }
        

        private void NotifyProperties()
        {
            RaisePropertyChanged(nameof(LiquidConvectionHeatTransfer));
            RaisePropertyChanged(nameof(ConvectionHeatTransfer));
            RaisePropertyChanged(nameof(HeatMelting));
            RaisePropertyChanged(nameof(LiquidTranscalency));
            RaisePropertyChanged(nameof(LiquidViscosity));
            RaisePropertyChanged(nameof(MeltingPoint));
        }

        private void ValidateProperties()
        {
            ValidateModelProperty(_model, _model.LiquidConvectionHeatTransfer, nameof(_model.LiquidConvectionHeatTransfer));
            ValidateModelProperty(_model, _model.ConvectionHeatTransfer, nameof(_model.ConvectionHeatTransfer));
            ValidateModelProperty(_model, _model.HeatMelting, nameof(_model.HeatMelting));
            ValidateModelProperty(_model, _model.LiquidTranscalency, nameof(_model.LiquidTranscalency));
            ValidateModelProperty(_model, _model.LiquidViscosity, nameof(_model.LiquidViscosity));
            ValidateModelProperty(_model, _model.MeltingPoint, nameof(_model.MeltingPoint));
        }        
    }
}
