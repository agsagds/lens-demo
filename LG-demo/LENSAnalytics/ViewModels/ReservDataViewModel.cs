﻿using LENSAnalytics.Model;
using LENSAnalytics.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.ViewModels
{
    public class ReservDataViewModel : NotifyErrorsModel
    {
        private ReservData _data;

        public ReservDataViewModel(ReservData data)
        {
            _data = data;
        }

        /// <summary>
        /// Название резервного поля
        /// </summary>
        public string Name { get { return _data.Name; } }
        /// <summary>
        /// Значение
        /// </summary>
        public decimal? Value
        {
            get { return _data.Value; }
            set
            {
                _data.Value = value;
                ValidateModelProperty(_data, _data.Value, nameof(_data.Value));
            }
        }

        public bool IsValide()
        {
            ValidateModelProperty(_data, _data.Value, nameof(_data.Value));
            return !HasErrors;
        }

    }
}
