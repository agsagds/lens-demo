﻿using LENSAnalytics.Model;
using LENSAnalytics.Model.Calculated;
using LENSAnalytics.Model.Containers;
using LENSAnalytics.Model.Geometry;
using LENSAnalytics.MVVM;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace LENSAnalytics.ViewModels
{
    public class ChartViewModel : DrawableWindowViewModel
    {
        private string _variablePropertyName;
        private TempTimeCylindricalPointsContainer _container;
        private float _lowerR;
        private float _upperR;

        public ChartViewModel(IEnumerable<TempTimeCylindricalPoint> data, string title) : base(title)
        {
            _container = new TempTimeCylindricalPointsContainer(ref data);
            Time = new MinMaxValue(data.Min(p => p.Time), data.Max(p => p.Time));
            Z = new MinMaxValue(data.Min(p => p.Z), data.Max(p => p.Z));
            Angle = new MinMaxValue(0, (float)Math.PI * 2);
            R = _container.GetRadiusRange(Z.Value, Angle.Value);
            _variablePropertyName = nameof(TempTimeCylindricalPoint.Time);            
            //Event handlers
            R.ValueChanged += UpdateChartData;
            Angle.ValueChanged += UpdateAngleValue;
            Z.ValueChanged += UpdateZValue;
            Time.ValueChanged += UpdateChartData;
            //configure series
            SeriesFill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
            Mapper = Mappers.Xy<TempTimeCylindricalPoint>().X(v => (float)v.GetType().GetProperty(VariablePropertyName).GetValue(v)).Y(v => v.Temp);
            ChartData = new ChartValues<TempTimeCylindricalPoint>();
            XMin = -1;
            XMax = 1;
            TMin = 0;
            TMax = 1;
            //set commands
            RadiusRangeChangedCommand = new RelayCommand(RaduisRangeChangeExecuted);
            UpdateChartData();
        }

        #region Properties
        public MinMaxValue R { get; private set; }
        public MinMaxValue Angle { get; private set; }
        public MinMaxValue Z { get; private set; }
        public MinMaxValue Time { get; private set; }
        public float StartR
        {
            get
            {
                return _lowerR;
            }
            set
            {
                if (value == _lowerR)
                    return;
                _lowerR = value;
                NotifyPropertyChanged();
            }
        }
        public float EndR
        {
            get
            {
                return _upperR;
            }
            set
            {
                if (value == _upperR)
                    return;
                _upperR = value;
                NotifyPropertyChanged();
            }
        }
        public float XMin { get; private set; }
        public float XMax { get; private set; }
        public float TMin { get; private set; }
        public float TMax { get; private set; }
        public string XTitle { get; private set; }
        public ChartValues<TempTimeCylindricalPoint> ChartData { get; private set; }
        public CartesianMapper<TempTimeCylindricalPoint> Mapper { get; private set; }
        public System.Windows.Media.Brush SeriesFill { get; private set; }
        public string VariablePropertyName
        {
            get
            {
                return _variablePropertyName;
            }

            set
            {
                if (value == null || _variablePropertyName == value)
                    return;
                _variablePropertyName = value;
                Mapper = Mappers.Xy<TempTimeCylindricalPoint>().X(v => (float)v.GetType().GetProperty(VariablePropertyName).GetValue(v)).Y(v => v.Temp);
                UpdateChartData();
                NotifyPropertyChanged();
            }
        }
        public string RadiusLabel { get; set; } = "Радиус(м)";
        public string ZLabel { get; set; } = "Высота(м)";
        #endregion
        #region Commands
        public RelayCommand RadiusRangeChangedCommand;
        #endregion

        private void UpdateZValue()
        {
            if (VariablePropertyName != nameof(TempTimeCylindricalPoint.Angle))
                R.CopyRange(_container.GetRadiusRange(Z.Value, Angle.Value));
            else
                R.CopyRange(_container.GetRadiusRangeByZ(Z.Value));
        }

        private void UpdateAngleValue()
        {

            if (VariablePropertyName != nameof(TempTimeCylindricalPoint.Z))
                R.CopyRange(_container.GetRadiusRange(Z.Value, Angle.Value));
            else
                R.CopyRange(_container.GetRadiusRangeByAngle(Angle.Value));            
        }

        private void RaduisRangeChangeExecuted(object obj)
        {
            UpdateChartData();
        }

        public override void UpdateChartData()
        {
            var freezeValue = new TempTimeCylindricalPoint()
            {
                Angle = Angle.Value,
                R = R.Value,
                Z = Z.Value,
                Time = Time.Value
            };
            var filteredData = _container.GetNearestData(freezeValue, VariablePropertyName, StartR, EndR);
            if (filteredData == null || filteredData.Count() == 0)
                return;

            ChartData.Clear();                        
            ChartData.AddRange(filteredData);
            UpdateAxis(filteredData);
        }

        private void UpdateAxis(IEnumerable<TempTimeCylindricalPoint> points)
        {
            switch (VariablePropertyName)
            {
                case nameof(TempTimeCylindricalPoint.R):
                    XMin = points.Min(p => p.R);
                    XMax = points.Max(p => p.R);
                    XTitle = RadiusLabel;
                    break;
                case nameof(TempTimeCylindricalPoint.Angle):
                    XMin = points.Min(p => p.Angle);
                    XMax = points.Max(p => p.Angle);
                    XTitle = "Угол(рад)";
                    break;
                case nameof(TempTimeCylindricalPoint.Z):
                    XMin = points.Min(p => p.Z);
                    XMax = points.Max(p => p.Z);
                    XTitle = ZLabel;
                    break;
                case nameof(TempTimeCylindricalPoint.Time):
                    XMin = points.Min(p => p.Time);
                    XMax = points.Max(p => p.Time);
                    XTitle = "Время(с)";
                    break;
                default:
                    throw new ArgumentException(string.Format("Unknown '{0}' parameter value: {1}", nameof(VariablePropertyName), VariablePropertyName));
            }
            var dx = XMax - XMin;
            XMin -= dx * 0.05f + (dx <= 1e-9 ? (XMax <= 1e-9 ? (float)(1e-5) : XMax * 0.05f) : 0);
            XMax += dx * 0.05f + (dx <= 1e-9 ? (XMax <= 1e-9 ? (float)(1e-5) : XMax * 0.05f) : 0);
            TMin = points.Min(p => p.Temp);
            TMax = points.Max(p => p.Temp);
            var dt = TMax - TMin;
            TMin -= dt * 0.05f + (dt == 0 ? 1 : 0);
            TMax += dt * 0.05f + (dt == 0 ? 1 : 0);

            NotifyPropertyChanged(nameof(XMin));
            NotifyPropertyChanged(nameof(XMax));
            NotifyPropertyChanged(nameof(TMin));
            NotifyPropertyChanged(nameof(TMax));
            NotifyPropertyChanged(nameof(XTitle));
        }

        protected override void AddToWordExecuted(object obj)
        {
            var path = obj as string;
            if (path == null)
                return;
            StringBuilder comment = new StringBuilder(Title);
            comment.Append(". График температурного поля. ");
            switch (VariablePropertyName)
            {
                case nameof(TempTimeCylindricalPoint.Angle):
                    comment.AppendFormat(RadiusLabel+": [{0:0.00E0};{1:0.00E0}]; ", StartR, EndR);
                    comment.AppendFormat(ZLabel+": {0:0.00E0}; ", Z.Value);
                    comment.AppendFormat("Время(с): {0:0.00E0} ", Time.Value);
                    break;
                case nameof(TempTimeCylindricalPoint.R):
                    comment.AppendFormat("Угол(рад): {0:0.00}; ", Angle.Value);
                    comment.AppendFormat(ZLabel + ": {0:0.00E0}; ", Z.Value);
                    comment.AppendFormat("Время(с): {0:0.00E0} ", Time.Value);
                    break;
                case nameof(TempTimeCylindricalPoint.Z):
                    comment.AppendFormat(RadiusLabel+": [{0:0.00E0};{1:0.00E0}]; ", StartR, EndR);
                    comment.AppendFormat("Угол(рад): {0:0.00}; ", Angle.Value);
                    comment.AppendFormat("Время(с): {0:0.00E0} ", Time.Value);
                    break;
                case nameof(TempTimeCylindricalPoint.Time):
                    comment.AppendFormat(RadiusLabel + ": [{0:0.00E0};{1:0.00E0}]; ", StartR, EndR);
                    comment.AppendFormat("Угол(рад): {0:0.00}; ", Angle.Value);
                    comment.AppendFormat(ZLabel + ": {0:0.00E0} ", Z.Value);
                    break;
            }

            AddImageToWord(path, comment.ToString());
            File.Delete(path);
        }
    }
}
