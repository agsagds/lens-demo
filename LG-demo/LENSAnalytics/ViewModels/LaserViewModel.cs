﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model;
using LENSAnalytics.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace LENSAnalytics.ViewModels
{
    public class LaserViewModel : InputDataBaseViewModel
    {
        private Laser _model;

        public LaserViewModel(Laser model) : base(model)
        {
            _model = model;
        }

        /// <summary>
        /// Диаметр пучка
        /// </summary>
        public decimal? Diameter
        {
            get { return _model.Diameter; }
            set
            {
                _model.Diameter = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Мощность пучка
        /// </summary>
        public decimal? Power
        {
            get { return _model.Power; }
            set
            {
                _model.Power = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Скорость движения пучка
        /// </summary>
        public decimal? Velocity
        {
            get { return _model.Velocity; }
            set
            {
                _model.Velocity = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Шаг пучка
        /// </summary>
        public decimal? Step
        {
            get { return _model.Step; }
            set
            {
                _model.Step = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Степень расходимости
        /// </summary>
        public decimal? Dispersion
        {
            get { return _model.Dispersion; }
            set
            {
                _model.Dispersion = value;
                NotifyPropertyChanged();
            }
        }

        public override bool CanSave()
        {
            NotifyProperties();
            ValidateProperties();
            return base.CanSave();
        }

        public override void Load(string path)
        {
            base.Load(path);
            NotifyProperties();
            ValidateProperties();
        }

        private void NotifyProperties()
        {
            RaisePropertyChanged(nameof(Diameter));
            RaisePropertyChanged(nameof(Power));
            RaisePropertyChanged(nameof(Velocity));
            RaisePropertyChanged(nameof(Step));
            RaisePropertyChanged(nameof(Dispersion));
        }

        private void ValidateProperties()
        {
            ValidateModelProperty(_model, _model.Diameter, nameof(_model.Diameter));
            ValidateModelProperty(_model, _model.Power, nameof(_model.Power));
            ValidateModelProperty(_model, _model.Velocity, nameof(_model.Velocity));
            ValidateModelProperty(_model, _model.Step, nameof(_model.Step));
            ValidateModelProperty(_model, _model.Dispersion, nameof(_model.Dispersion));
        }
    }
}
