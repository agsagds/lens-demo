﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Interfaces;
using LENSAnalytics.Model;
using LENSAnalytics.MVVM;
using LENSAnalytics.Properties;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using LG.GeneralSolve;

namespace LENSAnalytics.ViewModels
{
    public class UserDataViewModel : NotifyModel
    {
        #region Events
        public delegate void CreatePresenterInstanceEventHandler(PresenterViewModel presenter);
        public event CreatePresenterInstanceEventHandler CreatePresenterInstance;
        public delegate void CloseWindowEventHandler();
        public delegate void CreateFillingWindowInstance(string title, string text);
        public event CreateFillingWindowInstance OpenFillingWindow;
        public event CloseWindowEventHandler CloseFillingWindow;
        #endregion

        #region Constants
        private static string CalcPath = AppDomain.CurrentDomain.BaseDirectory + Resources.CalculationFolderName + "\\";
        private static string DefaultDataName = "data";
        private static string OutputExtension = ".in";
        private static string CalculatorName = "calc.exe";
        private static string ResultExtension = ".result";
        private static string InputDataExtension = ".inputinfo";
        private static string SlicedModelExtension = ".sliced";

        #endregion
        private ISaver _saver;

        public UserDataViewModel()
        {
            Gas = new GasViewModel(new Gas());
            Laser = new LaserViewModel(new Laser());
            Material = new MaterialViewModel(new Material());
            Melt = new MeltViewModel(new Melt());
            Powder = new PowderViewModel(new Powder());
            STLModel = new STLViewModel();
            //configure saver|loader
            _saver = new Saver();
            //set commands
            LoadDataCommand = new RelayCommand(LoadDataExecuted);
            SaveDataCommand = new RelayCommand(SaveDataExecuted);
            SaveAllDataCommand = new RelayCommand(SaveAllDataExecuted);
            LoadAllDataCommand = new RelayCommand(LoadAllDataExecuted);
            CalcCommand = new RelayCommand(OnCalcExecuted);
            LoadResultsCommand = new RelayCommand(LoadResultsExecuted);
        }

        #region Commands
        public RelayCommand LoadDataCommand { get; private set; }
        public RelayCommand SaveDataCommand { get; private set; }
        public RelayCommand SaveAllDataCommand { get; private set; }
        public RelayCommand LoadAllDataCommand { get; private set; }
        public RelayCommand CalcCommand { get; private set; }
        public RelayCommand LoadResultsCommand { get; private set; }
        #endregion
        #region Properties
        public InputDataBaseViewModel Gas { get; private set; }
        public InputDataBaseViewModel Laser { get; private set; }
        public InputDataBaseViewModel Material { get; private set; }
        public InputDataBaseViewModel Melt { get; private set; }
        public InputDataBaseViewModel Powder { get; private set; }
        public STLViewModel STLModel { get; private set; }
        #endregion
        #region Calculation
        private async void OnCalcExecuted(object obj)
        {
            try
            {
                if (!AllInputComplete())
                {
                    ShowIncorrectInputMessage();
                    return;
                }
                var header = "Подготовка данных";
                var message = "Идет подготовка данных к расчету. Пожалуйста подождите...";
                OpenFillingWindow.Invoke(header, message);
                await Task.Run(() =>
                {
                    Directory.CreateDirectory(CalcPath);
                    _saver.Save(CalcPath + DefaultDataName + OutputExtension,
                            Gas.GetModel() as Gas,
                            Laser.GetModel() as Laser,
                            Material.GetModel() as Material,
                            Melt.GetModel() as Melt,
                            Powder.GetModel() as Powder);
                    CreateSlicedModels();
                });

                CloseFillingWindow.Invoke();
                RunCalc(CalcPath);
                SaveResults();
                SaveAllData(CalcPath + DefaultDataName);
                CreatePresenter(CalcPath + DefaultDataName);
            }
            catch (DataSaveException)
            {
                MessageBox.Show("Не удалось обработать данные. Расчет не произведён. Проверьте правильность заполнения полей.",
                                    "Ошибка сохранения!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private async void CreatePresenter(string path)
        {
            var header = "Подготовка результатов";
            var message = "Идет обработка результатов расчета. Пожалуйста подождите...";
            OpenFillingWindow.Invoke(header, message);
            PresenterViewModel pwm = null;
            try
            {
                await Task.Run(() =>
                {
                    pwm = new PresenterViewModel(path);
                });
                CloseFillingWindow.Invoke();
                CreatePresenterInstance.Invoke(pwm);
            }
            catch (Exception ex)
            {
                CloseFillingWindow.Invoke();
                MessageBox.Show(ex.Message + '\n' + ex.StackTrace, "Ошибка обработки", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CreateSlicedModels()
        {
            string modelPath = CalcPath + DefaultDataName;
            STLModel.CopyBaseModel(modelPath);
            STLModel.CopyNozzleModel(modelPath);
            STLModel.SliceModel(modelPath + STLViewModel.DefaultBaseModelName + STLViewModel.ModelExtension,
                CalcPath + STLViewModel.DefaultBaseModelName + SlicedModelExtension, ModelType.Base);
            STLModel.SliceModel(modelPath + STLViewModel.DefaultNozzleModelName + STLViewModel.ModelExtension,
                CalcPath + STLViewModel.DefaultNozzleModelName + SlicedModelExtension, ModelType.Nozzle);
        }

        private void RunCalc(string calcPath)
        {
            try
            {
               // var filepath = Path.Combine(calcPath, CalculatorName);
                Module1.Main(CalcPath);
              //  Process.Start(filepath).WaitForExit();
            }
            catch (Exception)
            {
                MessageBox.Show(@"Не удалось запустить программу расчета. Расчет не произведён.",
                                  "Ошибка исполнения!", MessageBoxButton.OK, MessageBoxImage.Warning);
                throw new DataSaveException();
            }
        }
        #endregion
        #region SaversAndLoaders

        private void SaveResults()
        {
            string extension = ResultExtension;
            string filter = string.Format("Результаты расчета(*{0}) | *{0}", extension);
            string initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string path = SaveFile(extension, filter, initialDirectory);
            if (path == null)
                return;

            try
            {
                File.Create(path);
                path = path.Substring(0, path.LastIndexOf(extension));
                File.Copy(CalcPath + DefaultDataName + PresenterViewModel.BaseTempFieldExtension, path + PresenterViewModel.BaseTempFieldExtension, true);
                File.Copy(CalcPath + DefaultDataName + PresenterViewModel.MeltTempFieldExtension, path + PresenterViewModel.MeltTempFieldExtension, true);
                File.Copy(CalcPath + DefaultDataName + PresenterViewModel.MeltTempFieldForChartExtension, path + PresenterViewModel.MeltTempFieldForChartExtension, true);
                File.Copy(CalcPath + DefaultDataName + PresenterViewModel.FlowExtension, path + PresenterViewModel.FlowExtension, true);
                STLModel.CopyBaseModel(path);
                STLModel.CopyNozzleModel(path);
                SaveAllData(path);
            }
            catch
            {
                MessageBox.Show("Не удалось обработать данные.",
                             "Ошибка сохранения!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LoadResultsExecuted(object obj)
        {
            string extension = ResultExtension;
            string filter = string.Format("Результаты расчета(*{0}) | *{0}", extension);
            string initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string path = LoadFile(extension, filter, initialDirectory);
            if (path == null)
                return;

            CreatePresenter(path.Substring(0, path.LastIndexOf(extension)));
        }

        private void LoadDataExecuted(object obj)
        {
            var model = obj as ISavable;
            if (model == null)
                return;

            string extension = model.GetExtension();
            string filter = string.Format("Файл характеристик(*{0}) | *{0}", extension);
            string initialDirectory = Path.GetFullPath(Resources.SampleInputFolderName + "\\");
            string path = LoadFile(extension, filter, initialDirectory);
            if (path == null)
                return;

            try
            {
                model.Load(path);
            }
            catch (DataLoadException)
            {
                MessageBox.Show("Данные загружены не полностью.", "Ошибка загрузки!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveDataExecuted(object obj)
        {
            var model = obj as ISavable;
            if (model == null)
                return;
            if (!CanSaveData(model))
            {
                ShowIncorrectInputMessage();
                return;
            }

            string extension = model.GetExtension();
            string filter = string.Format("Файл характеристик(*{0}) | *{0}", extension);
            string initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string path = SaveFile(extension, filter, initialDirectory);
            if (path == null)
                return;

            try
            {
                model.Save(path);
            }
            catch (DataSaveException)
            {
                MessageBox.Show("Данные сохранены не полностью.", "Ошибка сохранения!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private bool CanSaveData(ISavable viewModel)
        {
            if (viewModel.CanSave())
                return true;

            return false;
        }

        private void SaveAllDataExecuted(object obj)
        {
            if (!AllInputComplete())
            {
                ShowIncorrectInputMessage();
                return;
            }

            string extension = InputDataExtension;
            string filter = string.Format("Файл характеристик(*{0}) | *{0}", extension);
            string initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string path = SaveFile(extension, filter, initialDirectory);
            if (path == null)
                return;
            File.OpenWrite(path).Close();
            path = path.Substring(0, path.LastIndexOf(extension));
            SaveAllData(path);
        }

        private void SaveAllData(string path)
        {
            try
            {
                Gas.Save(path + Gas.GetExtension());
                Laser.Save(path + Laser.GetExtension());
                Material.Save(path + Material.GetExtension());
                Melt.Save(path + Melt.GetExtension());
                Powder.Save(path + Powder.GetExtension());
                STLModel.Save(path + STLModel.GetExtension());
            }
            catch (DataSaveException)
            {
                MessageBox.Show("Данные сохранены не полностью.", "Ошибка сохранения!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private bool AllInputComplete()
        {
            CanSaveData(Gas);
            CanSaveData(Laser);
            CanSaveData(Material);
            CanSaveData(Melt);
            CanSaveData(Powder);
            CanSaveData(STLModel);

            return !(Gas.HasErrors
                || Laser.HasErrors
                || Material.HasErrors
                || Melt.HasErrors
                || Powder.HasErrors
                || STLModel.HasErrors);
        }

        private void LoadAllDataExecuted(object obj)
        {
            string extension = InputDataExtension;
            string filter = string.Format("Файл характеристик(*{0}) | *{0}", extension);
            string initialDirectory = Path.GetFullPath(Resources.SampleInputFolderName + "\\");
            string path = LoadFile(extension, filter, initialDirectory);
            if (path == null)
                return;
            path = path.Substring(0, path.LastIndexOf(extension));
            LoadAllData(path);
        }

        private void LoadAllData(string path)
        {
            try
            {
                Gas.Load(path + Gas.GetExtension());
                Laser.Load(path + Laser.GetExtension());
                Material.Load(path + Material.GetExtension());
                Melt.Load(path + Melt.GetExtension());
                Powder.Load(path + Powder.GetExtension());
                STLModel.Load(path + STLModel.GetExtension());
            }
            catch (DataLoadException)
            {
                MessageBox.Show("Данные загружены не полностью.", "Ошибка загрузки!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShowIncorrectInputMessage()
        {
            MessageBox.Show("Для выполнения операции необходимо заполнить выделенные поля.", "Незавершенный ввод!", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private string LoadFile(string extention, string filter, string initialDirectory)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            //System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = extention;
            dlg.RestoreDirectory = true;
            dlg.InitialDirectory = initialDirectory;
            dlg.Filter = filter;
            return dlg.ShowDialog() == true ? dlg.FileName : null;
        }

        private string SaveFile(string extention, string filter, string initialDirectory)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = extention;
            //  dlg.RestoreDirectory = true;
            dlg.InitialDirectory = initialDirectory;
            dlg.Filter = filter;
            return dlg.ShowDialog() == true ? dlg.FileName : null;
        }
        #endregion
    }
}
