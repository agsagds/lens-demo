﻿using LENSAnalytics.Interfaces;
using LENSAnalytics.Model;
using LENSAnalytics.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.ViewModels
{
    public abstract class InputDataBaseViewModel : NotifyErrorsModel, ISavable
    {
        private InputDataBase _model;

        public InputDataBaseViewModel(InputDataBase model)
        {
            _model = model;
            AdditionParameters = new ObservableCollection<ReservDataViewModel>();
            foreach (var parameter in _model.AdditionParameters)
                AdditionParameters.Add(new ReservDataViewModel(parameter));
        }
        /// <summary>
        /// Дополнительные параметры
        /// </summary>
        public ObservableCollection<ReservDataViewModel> AdditionParameters
        {
            get; private set;
        }

        public virtual bool CanSave()
        {
            bool additionParametersIsValide=true;
            foreach (var parameter in AdditionParameters)
                additionParametersIsValide&=parameter.IsValide();
            return !HasErrors && additionParametersIsValide;
        }

        public InputDataBase GetModel()
        {
            return _model;
        }

        public void Save(string path)
        {
            _model.Save(path);
        }

        public virtual void Load(string path)
        {
            _model.Load(path);
            AdditionParameters.Clear();
            foreach (var parameter in _model.AdditionParameters)
                AdditionParameters.Add(new ReservDataViewModel(parameter));
        }

        public string GetExtension()
        {
            return _model.GetExtension();
        }

    }
}
