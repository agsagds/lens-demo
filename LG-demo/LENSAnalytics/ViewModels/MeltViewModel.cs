﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.ViewModels
{
    public class MeltViewModel : InputDataBaseViewModel
    {
        private Melt _model;

        public MeltViewModel(Melt model) : base(model)
        {
            _model = model;
        }
        /// <summary>
        /// Степень черноты поверхности
        /// </summary>
        public decimal? Emissivity
        {
            get { return _model.Emissivity; }
            set
            {
                _model.Emissivity = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Теплоёмкость
        /// </summary>
        public decimal? HeatCapacity
        {
            get { return _model.HeatCapacity; }
            set
            {
                _model.HeatCapacity = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Плотность
        /// </summary>
        public decimal? Density
        {
            get { return _model.Density; }
            set
            {
                _model.Density = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Удельная теплота плавления
        /// </summary>
        public decimal? HeatMelting
        {
            get { return _model.HeatMelting; }
            set
            {
                _model.HeatMelting = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Удельная теплота испарения
        /// </summary>
        public decimal? HeatBoiling
        {
            get { return _model.HeatBoiling; }
            set
            {
                _model.HeatBoiling = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Молярная масса
        /// </summary>
        public decimal? MolarMass
        {
            get { return _model.MolarMass; }
            set
            {
                _model.MolarMass = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Молярная теплота испарения
        /// </summary>
        public decimal? MolarHeatBoiling
        {
            get { return _model.MolarHeatBoiling; }
            set
            {
                _model.MolarHeatBoiling = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Краевой угол
        /// </summary>
        public decimal? WettingAngle
        {
            get { return _model.WettingAngle; }
            set
            {
                _model.WettingAngle = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Коэффициент поверхностного натяжения
        /// </summary>
        public decimal? SurfaceTension
        {
            get { return _model.SurfaceTension; }
            set
            {
                _model.SurfaceTension = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Линейный коэффициент поглощения лазерного излучения
        /// </summary>
        public decimal? LaserEmissionAbsorbability
        {
            get { return _model.LaserEmissionAbsorbability; }
            set
            {
                _model.LaserEmissionAbsorbability = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Теплопроводность
        /// </summary>
        public decimal? Transcalency
        {
            get { return _model.Transcalency; }
            set
            {
                _model.Transcalency = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Cтепень черноты поверхности расплава
        /// </summary>
        public decimal? MeltEmissivity
{
            get { return _model.MeltEmissivity; }
            set
            {
                _model.MeltEmissivity = value;
                NotifyPropertyChanged();
            }
        }
        /// <summary>
        /// Радиус лазерного пятна
        /// </summary>
        public decimal? LaserSpotRadius
        {
            get { return _model.LaserSpotRadius; }
            set
            {
                _model.LaserSpotRadius = value;
                NotifyPropertyChanged();
            }
        }
        public override bool CanSave()
        {
            NotifyProperties();
            ValidateProperties();
            return base.CanSave();
        }

        public override void Load(string path)
        {
            base.Load(path);
            NotifyProperties();
            ValidateProperties();
        }

        private void NotifyProperties()
        {
            RaisePropertyChanged(nameof(Emissivity));
            RaisePropertyChanged(nameof(HeatCapacity));
            RaisePropertyChanged(nameof(Density));
            RaisePropertyChanged(nameof(HeatMelting));
            RaisePropertyChanged(nameof(HeatBoiling));
            RaisePropertyChanged(nameof(MolarMass));
            RaisePropertyChanged(nameof(MolarHeatBoiling));
            RaisePropertyChanged(nameof(WettingAngle));
            RaisePropertyChanged(nameof(SurfaceTension));
            RaisePropertyChanged(nameof(LaserEmissionAbsorbability));
            RaisePropertyChanged(nameof(Transcalency));
            RaisePropertyChanged(nameof(MeltEmissivity));
            RaisePropertyChanged(nameof(LaserSpotRadius));
        }

        private void ValidateProperties()
        {
            ValidateModelProperty(_model, _model.Emissivity, nameof(_model.Emissivity));
            ValidateModelProperty(_model, _model.HeatCapacity, nameof(_model.HeatCapacity));
            ValidateModelProperty(_model, _model.Density, nameof(_model.Density));
            ValidateModelProperty(_model, _model.HeatMelting, nameof(_model.HeatMelting));
            ValidateModelProperty(_model, _model.HeatBoiling, nameof(_model.HeatBoiling));
            ValidateModelProperty(_model, _model.MolarMass, nameof(_model.MolarMass));
            ValidateModelProperty(_model, _model.MolarHeatBoiling, nameof(_model.MolarHeatBoiling));
            ValidateModelProperty(_model, _model.WettingAngle, nameof(_model.WettingAngle));
            ValidateModelProperty(_model, _model.SurfaceTension, nameof(_model.SurfaceTension));
            ValidateModelProperty(_model, _model.LaserEmissionAbsorbability, nameof(_model.LaserEmissionAbsorbability));
            ValidateModelProperty(_model, _model.Transcalency, nameof(_model.Transcalency));
            ValidateModelProperty(_model, _model.MeltEmissivity, nameof(_model.MeltEmissivity));
            ValidateModelProperty(_model, _model.LaserSpotRadius, nameof(_model.LaserSpotRadius));
        }
    }
}
