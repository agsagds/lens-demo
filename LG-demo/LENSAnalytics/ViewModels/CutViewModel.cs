﻿using LENSAnalytics.Model;
using LENSAnalytics.Model.Calculated;
using LENSAnalytics.Model.Geometry;
using LENSAnalytics.MVVM;
using LiveCharts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace LENSAnalytics.ViewModels
{
    public class CutViewModel : DrawableWindowViewModel
    {

        private IDrawable _drawer;        
        private const string SaveImagePath="temp.jpg";

        public CutViewModel(IDrawable drawer, string title):base(title)
        {
            _drawer = drawer;
            //Propetries
            var data = drawer.GetData();
            Angle = new MinMaxValue(data.Min(p => p.Angle), data.Max(p => p.Angle));
            Time = new MinMaxValue(data.Min(p => p.Time), data.Max(p => p.Time));
            Angle.ValueChanged += UpdateChartData;
            Time.ValueChanged += UpdateChartData;
            //get first image
            UpdateChartData();
        }
           
        #region Properties
        public MinMaxValue Angle { get; private set; }
        public MinMaxValue Time { get; private set; }
        #endregion


        public BitmapSource CutImage { get; private set; }

        public override void UpdateChartData()
        {
            try
            {
                CutImage = _drawer.GetCut(Angle.Value, Time.Value);
                NotifyPropertyChanged(nameof(CutImage));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace ,ex.Message);
            }
        }

        protected override void AddToWordExecuted(object obj)
        {
            var source = obj as BitmapSource;
            if (source == null)
                return;

            using (var fileStream = new FileStream(SaveImagePath, FileMode.Create))
            {
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(source));
                encoder.Save(fileStream);
            }

            StringBuilder comment = new StringBuilder(Title);
            comment.Append(". ");
            comment.AppendFormat("Угол(рад): {0:0.00}; ", Angle.Value);
            comment.AppendFormat("Время(с): {0:0.00E0} ", Time.Value);
                        
            AddImageToWord(SaveImagePath, comment.ToString());
            File.Delete(SaveImagePath);
        }
    }
}
