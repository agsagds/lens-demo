﻿using LENSAnalytics.Interfaces;
using LENSAnalytics.Model;
using LENSAnalytics.Model.Calculated;
using LENSAnalytics.Model.Geometry;
using LENSAnalytics.Model.STLModel;
using LENSAnalytics.MVVM;
using LENSAnalytics.Printing;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace LENSAnalytics.ViewModels
{
    public class PresenterViewModel : NotifyModel, IDisposable
    {
        #region Events
        public delegate void CreatePresenterInstanceEventHandler(NotifyModel presenter);
        public event CreatePresenterInstanceEventHandler CreateCutPresenterInstance;
        public event CreatePresenterInstanceEventHandler CreateFlowPresenterInstance;
        public event CreatePresenterInstanceEventHandler CreateChartPresenterInstance;
        #endregion
        #region Constants
        public static string FlowExtension = ".flowresult";
        public static string MeltTempFieldExtension = ".meltresult";
        public static string MeltTempFieldForChartExtension = ".meltforchartresult";
        public static string BaseTempFieldExtension = ".baseresult";
        private const int ImageSize = 750;
        private const uint MeltZNetSize = 30;
        private const float MeltPointDuplicateRadiusEps = (float)1e-6;
        #endregion
        public CutViewModel Melt { get; private set; }
        public CutViewModel Base { get; private set; }
        public FlowPresenterViewModel Flow { get; private set; }
        public ChartViewModel MeltChart { get; private set; }
        public ChartViewModel BaseChart { get; private set; }
        public ChartViewModel GasFlowChart { get; private set; }
        public ChartViewModel PowderFlowChart { get; private set; }

        #region Word
        private IWordExporter _word;
        private void AddImage(string relativeImagePath, string comment)
        {
            var fullPath = AppDomain.CurrentDomain.BaseDirectory + relativeImagePath;
            _word.PrintPlot(fullPath, comment);
        }
        #endregion


        public PresenterViewModel(string path)
        {
            _word = new WordExporter();
            InsertInputInfo(_word, path);
            //Drawing
            IDrawable meltDrawer, meltChartDrawer, baseDrawer, gasFlowDrawer, powderFlowDrawer;
            DrawersFactory.CreateMeltCutter(path + MeltTempFieldExtension, out meltDrawer, null, ImageSize);
            DrawersFactory.CreateMeltCutter(path + MeltTempFieldForChartExtension, out meltChartDrawer, null, ImageSize);
            var baseModel = Parser.ParseModel(path + STLViewModel.DefaultBaseModelName + STLViewModel.ModelExtension);
            DrawersFactory.CreateCutter(path + BaseTempFieldExtension, out baseDrawer, baseModel, ImageSize);
            var nozzleModel = Parser.ParseModel(path + STLViewModel.DefaultNozzleModelName + STLViewModel.ModelExtension);
            DrawersFactory.CreateFlowDrawers(path + FlowExtension, out gasFlowDrawer, out powderFlowDrawer, nozzleModel, ImageSize);
            Melt = new CutViewModel(meltDrawer, "Температурное поле расплава");
            Base = new CutViewModel(baseDrawer, "Температурное поле детали");
            Flow = new FlowPresenterViewModel(gasFlowDrawer, powderFlowDrawer, "Температурное поле потока");
            Melt.AddImageInstance += AddImage;
            Base.AddImageInstance += AddImage;
            Flow.AddImageInstance += AddImage;
            //Charts
            var meltChartData = meltChartDrawer.GetData().ToList();
            Calculation.NormalizeDataByZ(meltChartData, MeltZNetSize);
            MeltChart = new ChartViewModel(Calculation.RemoveDuplicatesByRadius(meltChartData, MeltPointDuplicateRadiusEps), "Расплав")
            {
                RadiusLabel = "R/Rmax",
                ZLabel ="H/Hmax"
            };
            BaseChart = new ChartViewModel(baseDrawer.GetData(), "Деталь");
            GasFlowChart = new ChartViewModel(gasFlowDrawer.GetData(), "Поток газа");
            PowderFlowChart = new ChartViewModel(powderFlowDrawer.GetData(), "Поток порошка");
            MeltChart.AddImageInstance += AddImage;
            BaseChart.AddImageInstance += AddImage;
            GasFlowChart.AddImageInstance += AddImage;
            PowderFlowChart.AddImageInstance += AddImage;
            //set commands
            ShowMeltCommand = new RelayCommand(ShowMeltExecuted);
            ShowBaseCommand = new RelayCommand(ShowBaseExecuted);
            ShowFlowCommand = new RelayCommand(ShowFlowExecuted);
            ShowChartCommand = new RelayCommand(ShowChartExecuted);
        }

        #region Commands
        public RelayCommand ShowMeltCommand { get; private set; }
        public RelayCommand ShowBaseCommand { get; private set; }
        public RelayCommand ShowFlowCommand { get; private set; }
        public RelayCommand ShowChartCommand { get; private set; }
        #endregion
        private void ShowMeltExecuted(object obj)
        {
            CreateCutPresenterInstance?.Invoke(Melt);
        }

        private void ShowChartExecuted(object obj)
        {
            var viewModel = obj as NotifyModel;
            if (viewModel == null)
                return;
            CreateChartPresenterInstance?.Invoke(viewModel);
        }

        private void ShowBaseExecuted(object obj)
        {
            CreateCutPresenterInstance?.Invoke(Base);
        }

        private void ShowFlowExecuted(object obj)
        {
            CreateFlowPresenterInstance?.Invoke(Flow);
        }

        public void Dispose()
        {
            MeltChart.AddImageInstance -= AddImage;
            BaseChart.AddImageInstance -= AddImage;
            GasFlowChart.AddImageInstance -= AddImage;
            PowderFlowChart.AddImageInstance -= AddImage;
            Melt.AddImageInstance -= AddImage;
            Base.AddImageInstance -= AddImage;
            Flow.AddImageInstance -= AddImage;
            _word.CloseAndSave();
        }

        private void InsertInputInfo(IWordExporter word, string path)
        {
            var Gas = new Gas();
            var Laser = new Laser();
            var Material = new Material();
            var Melt = new Melt();
            var Powder = new Powder();
            Gas.Load(path + Gas.GetExtension());
            Laser.Load(path + Laser.GetExtension());
            Material.Load(path + Material.GetExtension());
            Melt.Load(path + Melt.GetExtension());
            Powder.Load(path + Powder.GetExtension());
            var defaultHeader = new string[] { "Параметр", "Значение" };
            string[] header;
            IList<string[]> data;
            word.PrintTitle("Параметры газа");
            word.InsertTable(defaultHeader, Gas.ToDictionary().Select(p => new string[] { p.Key, p.Value }).ToList());
            word.PrintTitle("Параметры лазера");
            word.InsertTable(defaultHeader, Laser.ToDictionary().Select(p => new string[] { p.Key, p.Value }).ToList());
            word.PrintTitle("Параметры расплава");
            word.InsertTable(defaultHeader, Melt.ToDictionary().Select(p => new string[] { p.Key, p.Value }).ToList());
            //--------------Материал
            word.PrintTitle("Параметры материала матрицы");
            word.InsertTable(defaultHeader, Material.ToDictionary().Select(p => new string[] { p.Key, p.Value }).ToList());
            //таблица плотности
            word.PrintSubtitle(Resources.DensityTitle);
            header = new string[] { Resources.Temperature, Resources.Density };
            data = Material.Density.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
            //таблица теплопроводности
            word.PrintSubtitle(Resources.TranscalencyTitle);
            header = new string[] { Resources.Temperature, Resources.Transcalency };
            data = Material.Transcalency.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
            //таблица теплоёмкости
            word.PrintSubtitle(Resources.HeatCapacityTitle);
            header = new string[] { Resources.Temperature, Resources.HeatCapacity };
            data = Material.HeatCapacity.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
            //----------------Порошок
            word.PrintTitle("Параметры материала порошка");
            word.InsertTable(defaultHeader, Powder.ToDictionary().Select(p => new string[] { p.Key, p.Value }).ToList());
            //таблица состава
            word.PrintSubtitle(Resources.ParticlesComposition);
            header = new string[] { Resources.Diameter, Resources.Part };
            data = Powder.ParticlesComposition.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
            //таблица плотности
            word.PrintSubtitle(Resources.DensityTitle);
            header = new string[] { Resources.Temperature, Resources.Density };
            data = Powder.Density.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
            //таблица степени черноты поверхности
            word.PrintSubtitle(Resources.EmissivityTitle);
            header = new string[] { Resources.Temperature, Resources.Emissivity };
            data = Powder.Emissivity.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
            //таблица теплопроводности
            word.PrintSubtitle(Resources.TranscalencyTitle);
            header = new string[] { Resources.Temperature, Resources.Transcalency };
            data = Powder.Transcalency.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
            //таблица теплоёмкости
            word.PrintSubtitle(Resources.HeatCapacityTitle);
            header = new string[] { Resources.Temperature, Resources.HeatCapacity };
            data = Powder.HeatCapacity.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
            //----------------Подача порошка
            word.PrintTitle("Параметры подачи порошка");
            word.InsertTable(defaultHeader, Powder.Flow.ToDictionary().Select(p => new string[] { p.Key, p.Value }).ToList());
            //таблица функции распределения частиц при подаче
            word.PrintSubtitle(Resources.SpreadFunction);
            header = new string[] { Resources.SpreadFunctionValue, Resources.SpreadFunctionDiameter };
            data = Powder.Flow.SpreadFunction.Select(p => new string[] { p.Key.ToString(), p.Value.ToString() }).ToList();
            word.InsertTable(header, data);
        }


    }
}
