﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using LENSAnalytics.ViewModels;
using LENSAnalytics.Views.Presenter;
using LENSAnalytics.Views;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace LENSAnalytics
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MaxHeight = Height;
            MinHeight = Height;
            MaxWidth = Width;
            MinWidth = Width;
            DataContext = new UserDataViewModel();
            ((UserDataViewModel)DataContext).CreatePresenterInstance += CreatePresenterView;
            ((UserDataViewModel)DataContext).OpenFillingWindow += OpenFillingWindow;
            ((UserDataViewModel)DataContext).CloseFillingWindow += CloseFillingWindow;
        }

        FillingWindow _fillingWindow;

        private void OpenFillingWindow(string title, string content)
        {
            _fillingWindow = new FillingWindow(title, content);
            IsEnabled = false;
            Hide();
            _fillingWindow.Show();            
        }

        private void CloseFillingWindow()
        {            
            if (_fillingWindow != null)
                _fillingWindow.Close();
            IsEnabled = true;
            Show();
            Activate();
        }

        private void CreatePresenterView(PresenterViewModel presenter)
        {
            var view = new PresenterWindow();
            view.DataContext = presenter;
            view.ConfigureEvents();

            this.Hide();

            view.Closed += PresenterViewClosed;
            view.Show();
        }

        private void PresenterViewClosed(object sender, EventArgs e)
        {
            var view = sender as PresenterWindow;
            view.Closed -= PresenterViewClosed;
            this.Show();
            this.Activate();
        }
    }
}
