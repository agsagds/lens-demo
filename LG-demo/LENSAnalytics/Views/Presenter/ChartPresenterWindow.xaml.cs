﻿using LENSAnalytics.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace LENSAnalytics.Views.Presenter
{
    /// <summary>
    /// Логика взаимодействия для ChartPresenterWindow.xaml
    /// </summary>
    public partial class ChartPresenterWindow : Window
    {
        public ChartPresenterWindow()
        {
            InitializeComponent();
            WordSaveButton.Click += SaveChartAsImage;
            this.Closed += ChartPresenterWindowClosed;
        }

        private void ChartPresenterWindowClosed(object sender, EventArgs e)
        {
            WordSaveButton.Click -= SaveChartAsImage;
        }

        public const string PathKey = "ImagePath";

        private void SaveChartAsImage(object sender, RoutedEventArgs e)
        {
            var position = ChartControl.PointToScreen(new System.Windows.Point(0, 0));            
            Rectangle r = new Rectangle((int)position.X, (int)position.Y, (int)ChartControl.ActualWidth, (int)ChartControl.ActualHeight);
            Bitmap bmp = new Bitmap(r.Width, r.Height, PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(bmp);
            g.CopyFromScreen(r.Left, r.Top, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);
            bmp.Save(TryFindResource(PathKey) as string);
        }

        private void RadiusRangeChanged(object sender, EventArgs e)
        {
            ((ChartViewModel)DataContext).RadiusRangeChangedCommand.Execute(null);
        }
    }
}
