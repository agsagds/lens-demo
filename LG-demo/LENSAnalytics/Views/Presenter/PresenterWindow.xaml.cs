﻿using LENSAnalytics.MVVM;
using LENSAnalytics.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LENSAnalytics.Views.Presenter
{
    /// <summary>
    /// Логика взаимодействия для PresenterWindow.xaml
    /// </summary>
    public partial class PresenterWindow : Window
    {
        public PresenterWindow()
        {
            InitializeComponent();
            MaxHeight = Height;
            MinHeight = Height;
            MaxWidth = Width;
            MinWidth = Width;                    
        }

        public void ConfigureEvents()
        {
            ((PresenterViewModel)DataContext).CreateCutPresenterInstance += CreateCutPresenterView;
            ((PresenterViewModel)DataContext).CreateFlowPresenterInstance += CreateFlowPresenterView;
            ((PresenterViewModel)DataContext).CreateChartPresenterInstance += CreateChartPresenterView;
            this.Closed += WindowClosed;
        }

        private void WindowClosed(object sender, EventArgs e)
        {
            this.Closed -= WindowClosed;
            ((IDisposable)DataContext).Dispose();
        }

        private void CreateCutPresenterView(NotifyModel presenter)
        {
            var view = new CutPresenterWindow();
            view.DataContext = presenter;
            ((DrawableWindowViewModel)presenter).UpdateChartData();
            this.Hide();

            view.Closed += ChildWindowClosed;
            view.Show();
        }

        private void CreateFlowPresenterView(NotifyModel presenter)
        {
            var view = new FlowPresenterWindow();
            view.DataContext = presenter;
            ((DrawableWindowViewModel)presenter).UpdateChartData();
            this.Hide();

            view.Closed += ChildWindowClosed;
            view.Show();
        }

        private void CreateChartPresenterView(NotifyModel presenter)
        {
            var view = new ChartPresenterWindow();
            view.DataContext = presenter;

            this.Hide();

            view.Closed += ChildWindowClosed;
            view.Show();
        }

        private void ChildWindowClosed(object sender, EventArgs e)
        {
            var view = sender as Window;
            view.Closed -= ChildWindowClosed;
            GC.Collect();
            this.Show();
            this.Activate();
        }
    }
}
