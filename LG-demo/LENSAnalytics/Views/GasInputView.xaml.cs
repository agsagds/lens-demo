﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UnitResources=LENSAnalytics.Properties.Resources;

namespace LENSAnalytics.Views
{
    /// <summary>
    /// Логика взаимодействия для GasInputView.xaml
    /// </summary>
    public partial class GasInputView : UserControl
    {
        public GasInputView()
        {
            InitializeComponent();
            Temperature.Text = UnitResources.Temperature;
            Viscosity.Text = UnitResources.Viscosity;
            Transcalency.Text = UnitResources.Transcalency;
            HeatCapacity.Text = UnitResources.HeatCapacity;
        }
    }
}
   
