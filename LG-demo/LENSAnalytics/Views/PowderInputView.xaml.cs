﻿using LENSAnalytics.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UnitResources = LENSAnalytics.Properties.Resources;

namespace LENSAnalytics.Views
{
    /// <summary>
    /// Логика взаимодействия для PowderInputView.xaml
    /// </summary>
    public partial class PowderInputView : UserControl
    {
        public PowderInputView()
        {
            InitializeComponent();
            //Powder
            LiquidConvectionHeatTransfer.Text = UnitResources.LiquidConvectionHeatTransfer;
            ConvectionHeatTransfer.Text = UnitResources.ConvectionHeatTransfer;
            HeatMelting.Text = UnitResources.HeatMelting;
            LiquidTranscalency.Text = UnitResources.LiquidTranscalency;
            LiquidViscosity.Text = UnitResources.LiquidViscosity;
            MeltingPoint.Text = UnitResources.MeltingPoint;            
            //Density
            Density.Header = UnitResources.DensityTitle;
            var grid = Density.Content as ParametersView;
            grid.KeysHeader=UnitResources.Temperature;
            grid.ValuesHeader = UnitResources.Density;
            //ParticlesComposition
            ParticlesComposition.Header = UnitResources.ParticlesComposition;
            grid = ParticlesComposition.Content as ParametersView;
            grid.KeysHeader = UnitResources.Diameter;
            grid.ValuesHeader = UnitResources.Part;
            //Emissivity
            Emissivity.Header = UnitResources.EmissivityTitle;
            grid = Emissivity.Content as ParametersView;
            grid.KeysHeader = UnitResources.Temperature;
            grid.ValuesHeader = UnitResources.Emissivity;
            //Transcalency
            Transcalency.Header = UnitResources.TranscalencyTitle;
            grid = Transcalency.Content as ParametersView;
            grid.KeysHeader = UnitResources.Temperature;
            grid.ValuesHeader = UnitResources.Transcalency;
            //HeatCapacity
            HeatCapacity.Header = UnitResources.HeatCapacityTitle;
            grid = HeatCapacity.Content as ParametersView;
            grid.KeysHeader = UnitResources.Temperature;
            grid.ValuesHeader = UnitResources.HeatCapacity;
            //PowderFlow
            InitialTemperature.Text = UnitResources.InitialTemperature;
            MassFlow.Text = UnitResources.MassFlow;
            MidFlow.Text = UnitResources.MidFlow;
            MidDiffRadius.Text = UnitResources.MidDiffRadius;            
            SpreadValue.Text = UnitResources.SpreadValue;
            //--spread function
            SpreadFunctionHeader.Text = UnitResources.SpreadFunction;
            grid = SpreadFunction as ParametersView;
            grid.KeysHeader = UnitResources.SpreadFunctionValue;
            grid.ValuesHeader = UnitResources.SpreadFunctionDiameter;
        }
    }
}
