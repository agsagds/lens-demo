﻿using LENSAnalytics.Model.STLModel;
using LENSAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LENSAnalytics.Model.Geometry;

namespace LENSAnalytics.Views
{
    /// <summary>
    /// Логика взаимодействия для SlicerWindow.xaml
    /// </summary>
    public partial class SlicerWindow : Window
    {
        private STLModel _model;
        private const int ANGLESTEPS = 72;

        public SlicerWindow()
        {
            InitializeComponent();
            string path = "model.STL";
            _model = Parser.ParseModel(path);
            double minZ, maxZ;
            minZ = _model.Triangles.OrderBy(t => t.Vertexes.OrderBy(v => v.Z).First().Z)
                    .First().Vertexes.OrderBy(v => v.Z).First().Z;
            maxZ = _model.Triangles.OrderByDescending(t => t.Vertexes.OrderByDescending(v => v.Z).First().Z)
                    .First().Vertexes.OrderByDescending(v => v.Z).First().Z;

            // slider.Minimum = minZ;
            // slider.Maximum = maxZ;

            slider.Minimum = 0;
            slider.Maximum = Math.PI;
            slider.ValueChanged += Slicer_ValueChanged;

            canvas.RenderTransform = new MatrixTransform(2, 0, 0, -2, canvas.Width / 2, canvas.Height-100);
            
        }

        private void Slicer_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //textBoxSliderValue.Text = String.Format("Z: {0:f2}", slider.Value);
            //var planeZ = slider.Value;
            //var lines = Slicer.GetPlaneAndTrianglesInrersections(planeZ, _model.Triangles);
            textBoxSliderValue.Text = String.Format("Angle: {0:f2}", slider.Value);
            var angle = slider.Value;
            var lines = Slicer.GetAxialCut(angle, _model.Triangles);
            canvas.Children.Clear();
            if (checkBoxLines.IsChecked == true)
                foreach (var ls in lines)
                {
                    Line l = new Line();
                    l.Stroke = System.Windows.Media.Brushes.Black;

                    l.X1 = ls.P1.X;
                    l.Y1 = ls.P1.Y;
                    l.X2 = ls.P2.X;
                    l.Y2 = ls.P2.Y;
                    canvas.Children.Add(l);
                }
            else
                for (double rayAngle = 0; rayAngle < Math.PI * 2; rayAngle += 2 * Math.PI / ANGLESTEPS)
                {
                    var points = new List<PolarPoint2D>(Slicer.GetRayAndLineSegmentsIntersections(rayAngle, ref lines).OrderBy(p => p.R));

                    for (int i = 0; i < points.Count(); i += 2)
                    {
                        Line l = new Line();
                        l.Stroke = System.Windows.Media.Brushes.Black;

                        if (i == 0)
                            if (points.Count() % 2 == 1)
                            {

                                l.X1 = 0;
                                l.Y1 = 0;
                                l.X2 = points[i].X;
                                l.Y2 = points[i].Y;
                                canvas.Children.Add(l);
                                i = -1;
                                continue;
                            }


                        l.X1 = points[i].X;
                        l.Y1 = points[i].Y;
                        l.X2 = points[i + 1].X;
                        l.Y2 = points[i + 1].Y;
                        canvas.Children.Add(l);

                    }
                }
        }
    }
}
