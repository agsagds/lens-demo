﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UnitResources = LENSAnalytics.Properties.Resources;

namespace LENSAnalytics.Views
{
    /// <summary>
    /// Логика взаимодействия для MaterialInputView.xaml
    /// </summary>
    public partial class MaterialInputView : UserControl
    {
        public MaterialInputView()
        {
            InitializeComponent();
            Emissivity.Text = UnitResources.Emissivity;
            //Density
            Density.Header = UnitResources.DensityTitle;
            var grid = Density.Content as ParametersView;
            grid.KeysHeader = UnitResources.Temperature;
            grid.ValuesHeader = UnitResources.Density;
            //Transcalency
            Transcalency.Header = UnitResources.TranscalencyTitle;
            grid = Transcalency.Content as ParametersView;
            grid.KeysHeader = UnitResources.Temperature;
            grid.ValuesHeader = UnitResources.Transcalency;
            //HeatCapacity
            HeatCapacity.Header = UnitResources.HeatCapacityTitle;
            grid = HeatCapacity.Content as ParametersView;
            grid.KeysHeader = UnitResources.Temperature;
            grid.ValuesHeader = UnitResources.HeatCapacity;
        }
    }
}
