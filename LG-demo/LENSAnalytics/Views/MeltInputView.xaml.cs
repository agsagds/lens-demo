﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UnitResources = LENSAnalytics.Properties.Resources;

namespace LENSAnalytics.Views
{
    /// <summary>
    /// Логика взаимодействия для MeltInputView.xaml
    /// </summary>
    public partial class MeltInputView : UserControl
    {
        public MeltInputView()
        {
            InitializeComponent();
            Emissivity.Text = UnitResources.Emissivity;
            HeatCapacity.Text = UnitResources.HeatCapacity;
            Density.Text = UnitResources.Density;
            HeatMelting.Text = UnitResources.HeatMelting;
            HeatBoiling.Text = UnitResources.HeatBoiling;
            MolarMass.Text = UnitResources.MolarMass;
            MolarHeatBoiling.Text = UnitResources.MolarHeatBoiling;
            WettingAngle.Text = UnitResources.WettingAngle;
            SurfaceTension.Text = UnitResources.SurfaceTension;
            LaserEmissionAbsorbability.Text = UnitResources.LaserEmissionAbsorbability;
            Transcalency.Text = UnitResources.Transcalency;
            MeltEmissivity.Text = UnitResources.MeltEmissivity;
            LaserSpotRadius.Text = UnitResources.LaserSpotRadius;
        }
    }
}
