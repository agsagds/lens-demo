﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UnitResources = LENSAnalytics.Properties.Resources;

namespace LENSAnalytics.Views
{
    /// <summary>
    /// Логика взаимодействия для LaserInputView.xaml
    /// </summary>
    public partial class LaserInputView : UserControl
    {
        public LaserInputView()
        {
            InitializeComponent();
            Diameter.Text = UnitResources.Diameter;
            Power.Text = UnitResources.BeamPower;
            Velocity.Text = UnitResources.BeamVelocity;
            Step.Text = UnitResources.BeamStep;
            Dispersion.Text = UnitResources.Dispersion;
        }
    }
}
