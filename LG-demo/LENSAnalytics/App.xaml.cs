﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace LENSAnalytics
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        //initialize NLog here
        protected override void OnStartup(StartupEventArgs e)
        {
            //configure logger here
            //Log.Info("Starting application");


            // TODO: Register custom types in the ServiceLocator
            //Log.Info("Registering custom types");
            /*
                        var serviceLocator = ServiceLocator.Default;
                        Bootstrapper.Configure(serviceLocator);
                        Bootstrapper.ConfigureExceptionsHandling(serviceLocator);

                        Catel.ExceptionHandling.IExceptionService exceptionService =
                            serviceLocator.ResolveType<Catel.ExceptionHandling.IExceptionService>();
                        System.AppDomain.CurrentDomain.UnhandledException += (sender, earg) => exceptionService.HandleException((System.Exception)earg.ExceptionObject);

                        LocalizationManager.Manager = new ADispatcher.Services.TelerikLocalizationManager();
                        //StyleManager.ApplicationTheme = new Office_BlackTheme();
                        StyleHelper.CreateStyleForwardersForDefaultStyles();

                        Log.Info("Calling base.OnStartup");
                        */
            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }
    }
}
