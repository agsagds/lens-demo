﻿using LENSAnalytics.Model.Geometry;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Calculated
{
    public static class DrawersFactory
    {
        /// <summary>
        /// Создает клас рисователя разрезов объекта
        /// </summary>
        /// <param name="path">Путь к файлу с данными</param>
        /// <param name="drawer">Объект для экземпляра рисователя</param>
        public static void CreateCutter(string path, out IDrawable drawer, STLModel.STLModel backgroundModel, int imageWidth)
        {
            var points = new List<TempTimeCylindricalPoint>();

            using (BinaryReader sr = new BinaryReader(File.OpenRead(path)))
            {
                while (sr.BaseStream.Position != sr.BaseStream.Length)
                {
                    var buff = sr.ReadBytes(TempTimeCylindricalPoint.BytesSize);
                    var point = new TempTimeCylindricalPoint(buff);
                    points.Add(point);
                }
            }
            drawer = new Cutter(ref points, backgroundModel, imageWidth * 2);
        }

        /// <summary>
        /// Создает клас рисователя разрезов капли
        /// </summary>
        /// <param name="path">Путь к файлу с данными</param>
        /// <param name="drawer">Объект для экземпляра рисователя</param>
        public static void CreateMeltCutter(string path, out IDrawable drawer, STLModel.STLModel backgroundModel, int imageWidth)
        {
            var points = new List<TempTimeCylindricalPoint>();

            using (BinaryReader sr = new BinaryReader(File.OpenRead(path)))
            {
                while (sr.BaseStream.Position != sr.BaseStream.Length)
                {
                    var buff = sr.ReadBytes(TempTimeCylindricalPoint.BytesSize);
                    var point = new TempTimeCylindricalPoint(buff);
                    points.Add(point);
                }
            }          

            drawer = new MeltCutter(ref points, backgroundModel, imageWidth * 2);
        }

        /// <summary>
        /// Создает классы рисователей для потоков газа и порошка
        /// </summary>
        /// <param name="path">Путь к файлу с данными</param>
        /// <param name="gasFlowDrawer">Объект для экземпляра рисователя потока газа</param>
        /// <param name="powderFlowDrawer">Объект для экземпляра рисователя потока порошка</param>
        public static void CreateFlowDrawers(string path, out IDrawable gasFlowDrawer, out IDrawable powderFlowDrawer, STLModel.STLModel backgroundModel, int imageWidth)
        {
            var gasPoints = new List<FlowLine>();
            var powderPoints = new List<FlowLine>();
            float modelHeigth;
            using (BinaryReader sr = new BinaryReader(File.OpenRead(path)))
            {
                modelHeigth = sr.ReadSingle();
                //modelHeigth = 0.02f;
                while (sr.BaseStream.Position != sr.BaseStream.Length)
                {
                    var buff = sr.ReadBytes(FlowLine.BytesSize);
                    var point = new FlowLine(buff);
                    var type = sr.ReadSingle();
                    if (type == 0)
                        gasPoints.Add(point);
                    else
                        powderPoints.Add(point);
                }
            }
            gasFlowDrawer = new FlowDrawer(ref gasPoints, backgroundModel, modelHeigth, imageWidth * 2);
            powderFlowDrawer = new FlowDrawer(ref powderPoints, backgroundModel, modelHeigth, imageWidth * 2);
        }
    }
}
