﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Drawing.Drawing2D;
using LENSAnalytics.Model.Geometry;
using LENSAnalytics.Model.STLModel;
using LiveCharts;

namespace LENSAnalytics.Model.Calculated
{
    public class FlowDrawer : AbstractDrawer<FlowLine>
    {
        protected float _modelHeigth;
        protected const float BaseLineLevel = 50;
        protected AdjustableArrowCap _arrow = new AdjustableArrowCap(5, 5);

        public FlowDrawer(ref List<FlowLine> points, STLModel.STLModel backgroundModel, float modelHeigth, int imageWidth) : base(ref points, backgroundModel, imageWidth)
        {
            _modelHeigth = modelHeigth;
            CalculateBackroundTransform(ref points);
        }

        protected override void CalculateMainTransform(ref List<FlowLine> points)
        {
            _mainLayerTransform.Reset();
            float canvasSize = GetOffsetXY() * 2;
            float minY = points.Min(p => p.RZProjection().Y);
            float maxX = points.Max(p => Math.Abs(p.RZProjection().X));
            float maxY = points.Max(p => p.RZProjection().Y);
            float scaleCoefficient = canvasSize / Math.Max(maxX * 2, maxY - minY);
            //учет отрисовки векторов скоростей
            minY = points
                .Min(p => p.RZProjection().Y * scaleCoefficient
                + (p.Speed.RZProjection().Y < 0 ? p.Speed.RZProjection().Y : 0));
            maxX = points
                .Max(p => Math.Abs(p.RZProjection().X * scaleCoefficient
                + (p.Speed.RZProjection(p.Angle).X * p.RZProjection().X >= 0 ? p.Speed.RZProjection(p.Angle).X : 0)));
            maxY = points
                .Max(p => p.RZProjection().Y * scaleCoefficient
                + (p.Speed.RZProjection().Y > 0 ? p.Speed.RZProjection().Y : 0) + BaseLineLevel);
            scaleCoefficient *= canvasSize / Math.Max(maxX * 2, maxY - minY);
            _mainLayerTransform.Translate(canvasSize / 2, -minY);
            _mainLayerTransform.Scale(scaleCoefficient, scaleCoefficient);
        }

        protected override void CalculateBackroundTransform(ref List<FlowLine> points)
        {
            _backgroundTransform.Reset();
            if (_backgroundModel == null)
                return;

            const float ZeroAngle = 0;
            const float MagicScaleCoefficient = 1f;
            float canvasSize = GetOffsetXY() * 2;
            var lines = Slicer.GetAxialCut(ZeroAngle, _backgroundModel.Triangles);
            float minY = (float)lines.Min(p => Math.Min(p.P1.Y, p.P2.Y));
            float maxY = (float)lines.Max(p => Math.Max(p.P1.Y, p.P2.Y));
            float maxX = (float)lines.Max(p => Math.Max(Math.Abs(p.P1.X), Math.Abs(p.P2.X)));
            float modelScaleY = GetScaleYTransform(_mainLayerTransform);
            float scale = _modelHeigth * modelScaleY / (maxY - minY) * MagicScaleCoefficient;
            minY *= scale;
            //Set background position at left down corner. 
            _backgroundTransform.Translate(canvasSize, -minY+_modelHeigth*3/4);
            _backgroundTransform.Scale(scale/6, scale/6);
        }

        protected override void DrawBackground(ref Graphics gr, float angle, ref IEnumerable<FlowLine> data)
        {
            gr.ResetTransform();
            //draw base
            var drawingSize = GetOffsetXY() * 2;
            PointF p1 = new PointF(0, drawingSize - BaseLineLevel);
            SizeF size = new SizeF(drawingSize, BaseLineLevel);
            RectangleF baseRect = new RectangleF(p1, size);
            HatchBrush brush = new HatchBrush(HatchStyle.ForwardDiagonal, Color.White);
            gr.FillRectangle(brush, baseRect);
            //draw nozzle
            var scaleX = GetScaleXTransform(_backgroundTransform);
            var scaleY = GetScaleYTransform(_backgroundTransform);
            var lines = Slicer.GetAxialCut(angle, _backgroundModel.Triangles);

            gr.TranslateTransform(_backgroundTransform.OffsetX, _backgroundTransform.OffsetY);         
            using (Pen pen = new Pen(Brushes.Black, 2f))
            {
                foreach (var ls in lines)
                    gr.DrawLine(pen, (float)ls.P1.X*scaleX, (float)ls.P1.Y*scaleY, (float)ls.P2.X*scaleX, (float)ls.P2.Y*scaleY);
            }
            gr.Flush();
            gr.ResetTransform();
        }

        protected override void DrawContoursLayer(ref Graphics gr, ref IEnumerable<FlowLine> points)
        {

        }

        

        protected override void DrawMainLayer(ref Graphics gr, ref IEnumerable<FlowLine> data, float minValue, float maxValue)
        {
            gr.ResetTransform();
            gr.TranslateTransform(_mainLayerTransform.OffsetX, _mainLayerTransform.OffsetY);
            float scaleX = GetScaleXTransform(_mainLayerTransform);
            float scaleY = GetScaleYTransform(_mainLayerTransform);
            foreach (var point in data)
            {
                using (Pen pen = new Pen(RGBFromTemp(minValue, maxValue, point.Temp)))
                {
                    var projecton = point.RZProjection();
                    var x = projecton.X * scaleX;
                    var y = projecton.Y * scaleY;
                    DrawObject(pen, x, y, ref gr, point);
                }
            }
            gr.ResetTransform();
        }

        protected override void DrawCutOutline(ref Graphics gr, ref IEnumerable<FlowLine> points)
        {
            
        }

        protected void DrawObject(Pen pen, float x, float y, ref Graphics gr, FlowLine obj)
        {
            pen.CustomEndCap = _arrow;
            pen.Width = 4f;

            var endPoint = obj.Speed.RZProjection(obj.Angle);
            endPoint.X *= 1f;
            endPoint.Y *= 1f;
            endPoint.X += x;
            endPoint.Y += y;
            gr.DrawLine(pen, x, y, endPoint.X, endPoint.Y);
        }        
    }
}
