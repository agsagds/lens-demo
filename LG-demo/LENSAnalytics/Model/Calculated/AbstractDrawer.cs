﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Drawing.Drawing2D;
using LENSAnalytics.Model.Geometry;
using LENSAnalytics.Model.Converters;
using LiveCharts;

namespace LENSAnalytics.Model.Calculated
{
    public abstract class AbstractDrawer<T> : IDrawable where T : TempTimeCylindricalPoint
    {
        /// <summary>
        /// Time(by value)->[Angle(by step range)->Point]
        /// </summary>
        protected Dictionary<float, Dictionary<float, T[]>> _storage;
        protected int _imageWidth;
        protected STLModel.STLModel _backgroundModel;
        protected Matrix _mainLayerTransform = new Matrix();
        protected Matrix _backgroundTransform = new Matrix();

        public AbstractDrawer(ref List<T> points, STLModel.STLModel backgroundModel, int imageWidth)
        {
            _imageWidth = imageWidth;
            GenerateStrorage(ref points);
            CalculateMainTransform(ref points);
            _backgroundModel = backgroundModel;
            CalculateBackroundTransform(ref points);
        }

        protected abstract void CalculateBackroundTransform(ref List<T> points);

        protected virtual void CalculateMainTransform(ref List<T> points)
        {
            _mainLayerTransform.Reset();
            float canvasSize = GetOffsetXY()*2;
            float minY = points.Min(p => p.RZProjection().Y);
            float maxX = points.Max(p => Math.Abs(p.RZProjection().X));
            float maxY = points.Max(p => p.RZProjection().Y);
            float midY = minY + (maxY - minY) / 2;
            float scaleCoefficient = canvasSize / Math.Max(maxX *2, maxY - minY);      
            midY *= scaleCoefficient;
            _mainLayerTransform.Translate(canvasSize / 2, canvasSize / 2 + midY);
            _mainLayerTransform.Scale(scaleCoefficient, -scaleCoefficient);
        }

        public IEnumerable<TempTimeCylindricalPoint> GetData()
        {
            return _storage.SelectMany(r => r.Value).SelectMany(r => r.Value).Select(p=>new TempTimeCylindricalPoint(p)).ToArray();
        }

        #region Drawing
        protected IEnumerable<T> GetData(float angle, float time)
        {
            var dict = _storage
                .OrderBy(r => Math.Abs(r.Key - time))
                .First().Value;
            var data = dict
                .Where(r => r.Key < Math.PI)
                .OrderBy(r => Math.Abs(r.Key - angle))
                .First()
                .Value
                .Concat(
                    dict
                    .Where(r => r.Key >= Math.PI)
                    .OrderBy(r => Math.Abs(r.Key - (angle + Math.PI)))
                    .First().Value
                );
            return data;
        }

        protected float GetOffsetXY()
        {
            return 3.5f * _imageWidth / 8;
        }

        protected abstract void DrawMainLayer(ref Graphics gr, ref IEnumerable<T> data, float minValue, float maxValue);        
        protected abstract void DrawBackground(ref Graphics gr, float angle, ref IEnumerable<T> data);

        /// <summary>
        /// Преобразует радианы в градусы
        /// </summary>
        /// <param name="angle">Значение угла в радианах</param>
        /// <returns></returns>
        protected float RadianToDegree(double angle)
        {
            return (float)(angle * 180 / Math.PI);
        }

        protected float GetScaleXTransform(Matrix transform)
        {
            return transform.Elements[0];
        }

        protected float GetScaleYTransform(Matrix transform)
        {
            return transform.Elements[3];
        }

        protected virtual void DrawCutOutline(ref Graphics gr, ref IEnumerable<T> points)
        {
            var defaultState = gr.Save();
            gr.ResetTransform();
            //draw cut outline
            var angles = points.GroupBy(p => p.Angle).Select(p => p.Key).ToArray();
            using (Pen pen = new Pen(Brushes.Black, 2))
            {
                int radius = 45;
                gr.TranslateTransform(_mainLayerTransform.OffsetX*2+radius, radius * 1.5f);

                PolarPoint2D p1 = new PolarPoint2D(radius * 1.2f, angles[0]);
                PolarPoint2D p2 = new PolarPoint2D(radius * 1.2f, angles[1]);
                gr.DrawEllipse(pen, -radius, -radius, radius * 2, radius * 2);
                gr.DrawLine(pen, 0, 0, p1.X, p1.Y);
                gr.DrawLine(pen, 0, 0, p2.X, p2.Y);
                var font = new Font(FontFamily.GenericMonospace.Name, 16, FontStyle.Bold);

                var state = gr.Save();

                gr.TranslateTransform(p1.X, p1.Y);
                gr.RotateTransform(RadianToDegree(angles[0] + Math.PI / 2));
                var sx = gr.MeasureString(angles[0].ToString("0.00"), font);
                gr.DrawString(angles[0].ToString("0.00"), font, pen.Brush, -sx.Width / 2, -sx.Height / 2);

                gr.Restore(state);
                gr.TranslateTransform(p2.X, p2.Y);
                gr.RotateTransform(RadianToDegree(angles[1] + Math.PI / 2));
                sx = gr.MeasureString(angles[1].ToString("0.00"), font);
                gr.DrawString(angles[1].ToString("0.00"), font, pen.Brush, -sx.Width / 2, -sx.Height / 2);
            }
            gr.Restore(defaultState);
        }

        protected void FillPolygons(ref Graphics gr, Dictionary<float, TempTimeCylindricalPoint[]> data, float minValue, float maxValue)
        {
            float scaleX = GetScaleXTransform(_mainLayerTransform);
            float scaleY = GetScaleYTransform(_mainLayerTransform);
            var keys = data.Keys.OrderBy(k => k).ToArray();
            for (int k = 0; k < keys.Count() - 1; k++)
            {
                var upLine = data[keys[k]];
                var downLine = data[keys[k + 1]];
                if (upLine.Count() == 0 || downLine.Count() == 0)
                    continue;
                var maxCount = Math.Max(upLine.Count(), downLine.Count());
                List<TempTimeCylindricalPoint> drawingPoints = new List<TempTimeCylindricalPoint>();
                for (int i = 0; i < maxCount - 1; i++)
                {
                    drawingPoints.Clear();
                    drawingPoints.Add(upLine[Math.Min(i, upLine.Count() - 1)]);
                    drawingPoints.Add(upLine[Math.Min(i + 1, upLine.Count() - 1)]);
                    drawingPoints.Add(downLine[Math.Min(i + 1, downLine.Count() - 1)]);
                    drawingPoints.Add(downLine[Math.Min(i, downLine.Count() - 1)]);
                    var midTemp = drawingPoints.Sum(p => p.Temp) / drawingPoints.Count();
                    using (Pen pen = new Pen(Color.Black, 0.5f))
                    {
                        var drawingData = drawingPoints.Select(p => new PointF(p.RZProjection().X * scaleX, p.RZProjection().Y * scaleY)).ToArray();
                        gr.FillPolygon(new SolidBrush(RGBFromTemp(minValue, maxValue, midTemp)), drawingData);
                    }
                }
            }
        }

        protected void DrawPolygons(ref Graphics gr, Dictionary<float, TempTimeCylindricalPoint[]> data, float minValue, float maxValue)
        {
            float scaleX = GetScaleXTransform(_mainLayerTransform);
            float scaleY = GetScaleYTransform(_mainLayerTransform);
            var keys = data.Keys.OrderBy(k => k).ToArray();
            for (int k = 0; k < keys.Count() - 1; k++)
            {
                var upLine = data[keys[k]];
                var downLine = data[keys[k + 1]];
                if (upLine.Count() == 0 || downLine.Count() == 0)
                    continue;
                var maxCount = Math.Max(upLine.Count(), downLine.Count());
                List<TempTimeCylindricalPoint> drawingPoints = new List<TempTimeCylindricalPoint>();
                for (int i = 0; i < maxCount - 1; i++)
                {
                    drawingPoints.Clear();
                    drawingPoints.Add(upLine[Math.Min(i, upLine.Count() - 1)]);
                    drawingPoints.Add(upLine[Math.Min(i + 1, upLine.Count() - 1)]);
                    drawingPoints.Add(downLine[Math.Min(i + 1, downLine.Count() - 1)]);
                    drawingPoints.Add(downLine[Math.Min(i, downLine.Count() - 1)]);
                    var midTemp = drawingPoints.Sum(p => p.Temp) / drawingPoints.Count();
                    using (Pen pen = new Pen(Color.Black, 0.5f))
                    {
                        var drawingData = drawingPoints.Select(p => new PointF(p.RZProjection().X * scaleX, p.RZProjection().Y * scaleY)).ToArray();
                        gr.DrawPolygon(pen, drawingData);
                    }
                }
            }
        }

        public virtual BitmapSource GetCut(float angle, float time)
        {
            var data = GetData(angle, time);
            //find extremal temp values
            var maxTemp = data.Max(p => p.Temp);
            var minTemp = data.Min(p => p.Temp);

            Bitmap bmp = GreateCanvas();
            Graphics gr = Graphics.FromImage(bmp);
            DrawBackground(ref gr, angle, ref data);
            DrawCutOutline(ref gr, ref data);
            DrawMainLayer(ref gr, ref data, minTemp, maxTemp);
            DrawContoursLayer(ref gr, ref data);
            DrawPalleteLegend(ref gr, minTemp, maxTemp);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return BitmapToBitmapSourceConverter.Convert(ref bmp);
        }

        protected virtual Bitmap GreateCanvas()
        {
            return new Bitmap(_imageWidth, (int)GetOffsetXY() * 2);
        }

        protected abstract void DrawContoursLayer(ref Graphics gr, ref IEnumerable<T> points);


        protected virtual void DrawPalleteLegend(ref Graphics gr, float minValue, float maxValue)
        {
            var legendWidth = _imageWidth - GetOffsetXY() * 2;
            var legendHeigth = GetOffsetXY();
            gr.DrawImage(GetPaletteLegend(minValue, maxValue, (int)legendWidth, (int)legendHeigth), 
                new PointF(GetOffsetXY() * 2 + 20, GetOffsetXY() * 0.5f));
        }

        protected Image GetPaletteLegend(float min, float max, int width, int height)
        {
            Bitmap bmp = new Bitmap(width + 1, height + 25);

            Graphics gr = Graphics.FromImage(bmp);
            Pen pen;
            float paletteWidth = width / 4f;
            int countOfValueParts = 4;
            int countOfPalleteParts = 16;
            gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            //pallete
            for (int i = 0; i < countOfPalleteParts; i++)
            {
                var value = max - i * (max - min) / countOfPalleteParts;
                pen = new Pen(RGBFromTemp(min, max, value));
                var y = i * (float)height / countOfPalleteParts;
                gr.FillRectangle(pen.Brush, 0, y, paletteWidth, (float)height / countOfPalleteParts);
            }
            //palette contours
            pen = new Pen(Brushes.Black, 1f);
            gr.DrawRectangle(pen, 0, 0, paletteWidth, height);
            for (int i = 0; i <= countOfValueParts; i++)
            {
                var p1 = new PointF(0, i * height / countOfValueParts);
                var p2 = new PointF(paletteWidth + 10, i * height / countOfValueParts);
                gr.DrawLine(pen, p1, p2);
                var value = max - i * (max - min) / countOfValueParts;
                var font = new Font(FontFamily.GenericMonospace.Name, 16, FontStyle.Bold);
                gr.DrawString(value.ToString("0"), font, pen.Brush, p2);
            }

            return bmp;
        }

        protected Color RGBFromTemp(float min, float max, float value)
        {
            if (min == max)
                return Color.Black;

            max -= min;
            value -= min;
            min -= min;

            int red = 0, green = 0, blue = 0;
            float quarter = (max - min) / 4;
            //blue
            if (value <= quarter)
                blue = 255;
            else if (value < quarter * 2)
            {
                float k = -(255 / (quarter));
                float b = 255 - k * quarter;
                blue = (int)(k * value + b);
            }
            //green
            if (value < quarter)
            {
                float k = (255 / (quarter));
                float b = 0;
                green = (int)(k * value + b);
            }
            else if (value > quarter * 3)
            {
                float k = -(255 / (quarter));
                float b = 255 - k * quarter * 3;
                green = (int)(k * value + b);
            }
            else
                green = 255;
            //red
            if (value >= quarter * 3)
                red = 255;
            else if (value > quarter * 2)
            {
                float k = (255 / (quarter));
                float b = -k * quarter * 2;
                red = (int)(k * value + b);
            }
            //result
            return Color.FromArgb(red, green, blue);
        }


        #endregion

        private void GenerateStrorage(ref List<T> points)
        {
            _storage = points
                    .GroupBy(p => p.Time,
                             p => p,
                             (key, values) => new { Key = key, Values = values.ToArray() })
                    .ToDictionary(
                        grp => grp.Key,
                        grp => grp.Values
                            .GroupBy(p => p.Angle,
                                     p => p,
                                     (key, values) => new { Key = key, Values = values.ToArray() })
                                .ToDictionary(
                                    angGrp => angGrp.Key,
                                    angGrp => angGrp.Values
                    ));
        }
    }
}
