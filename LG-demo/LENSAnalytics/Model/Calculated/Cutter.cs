﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Drawing.Drawing2D;
using LENSAnalytics.Model.Geometry;
using System.Diagnostics;
using LENSAnalytics.Model.STLModel;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Configurations;

namespace LENSAnalytics.Model.Calculated
{
    public class Cutter : AbstractDrawer<TempTimeCylindricalPoint>
    {

        public Cutter(ref List<TempTimeCylindricalPoint> points, STLModel.STLModel backgroundModel, int imageWidth) : base(ref points, backgroundModel, imageWidth)
        { }

        protected override void CalculateBackroundTransform(ref List<TempTimeCylindricalPoint> points)
        {
            _backgroundTransform.Reset();
            float canvasSize = GetOffsetXY() * 2;
            if (_backgroundModel == null)
                return;
            var cylindricalPoints = _backgroundModel.Triangles
                .SelectMany(t => t.Vertexes
                .Select(v => new CylindricalPoint(v)));
            var maxR = cylindricalPoints.Max(p => p.R);
            var minZ = cylindricalPoints.Min(p => p.Z);
            var maxZ = cylindricalPoints.Max(p => p.Z);
            float midZ = minZ + (maxZ - minZ) / 2;
            float scaleCoefficient = canvasSize / Math.Max(maxR * 2, maxZ - minZ);
            midZ *= scaleCoefficient;
            _backgroundTransform.Translate(canvasSize / 2, canvasSize / 2 + midZ);
            _backgroundTransform.Scale(scaleCoefficient, -scaleCoefficient);
        }

        protected override void DrawBackground(ref Graphics gr, float angle, ref IEnumerable<TempTimeCylindricalPoint> data)
        {
            return;
            if (_backgroundModel == null)
                return;

            var lines = Slicer.GetAxialCut(angle, _backgroundModel.Triangles);
            gr.ResetTransform();
            gr.TranslateTransform(_backgroundTransform.OffsetX, _backgroundTransform.OffsetY);
            float scaleX = GetScaleXTransform(_backgroundTransform);
            float scaleY = GetScaleYTransform(_backgroundTransform);

            using (Pen pen = new Pen(Brushes.Black, 4f))
            {
                foreach (var ls in lines)
                    gr.DrawLine(pen, (float)ls.P1.X * scaleX, (float)ls.P1.Y * scaleY, (float)ls.P2.X * scaleX, (float)ls.P2.Y * scaleY);
            }
            gr.ResetTransform();
        }

        protected override void DrawContoursLayer(ref Graphics gr, ref IEnumerable<TempTimeCylindricalPoint> points)
        {

        }

        protected override void DrawMainLayer(ref Graphics gr, ref IEnumerable<TempTimeCylindricalPoint> data, float minValue, float maxValue)
        {
            gr.ResetTransform();
            gr.TranslateTransform(_mainLayerTransform.OffsetX, _mainLayerTransform.OffsetY);
            /*  var leftSide = data
                  .Where(p => p.Angle >= Math.PI)
                  .GroupBy(p => p.Z,
                              p => p,
                              (key, values) => new { Key = key, Value = values.OrderBy(v => v.R).ToArray() })
                  .ToDictionary(grp => grp.Key,
                                  grp => grp.Value);
              var rightSide = data
                  .Where(p => p.Angle < Math.PI)
                  .GroupBy(p => p.Z,
                          p => p,
                          (key, values) => new { Key = key, Value = values.OrderBy(v => v.R).ToArray() })
                  .ToDictionary(grp => grp.Key,
                              grp => grp.Value);

              FillPolygons(ref gr, leftSide, minValue, maxValue);
              FillPolygons(ref gr, rightSide, minValue, maxValue);
              DrawPolygons(ref gr, leftSide, minValue, maxValue);
              DrawPolygons(ref gr, rightSide, minValue, maxValue);*/
            data = data.OrderBy(p => p.Z).ThenBy(p => p.R);

            float scaleX = GetScaleXTransform(_mainLayerTransform);
            float scaleY = GetScaleYTransform(_mainLayerTransform);
            float pointSize = 1f;
            float rWidth = 16f;
            float rHeigth = 25f;
            foreach (var point in data)
            {
                using (Pen pen = new Pen(RGBFromTemp(minValue, maxValue, point.Temp), pointSize / 2))
                {
                    var p = point.RZProjection();
                    gr.FillRectangle(pen.Brush,
                        p.X * scaleX - rWidth / 2,
                        p.Y * scaleY - rHeigth / 2,
                        rWidth,
                        rHeigth);
                    gr.DrawRectangle(new Pen(Color.Black, 1f),
                        p.X * scaleX - rWidth / 2,
                        p.Y * scaleY - rHeigth / 2,
                        rWidth,
                        rHeigth);
                    //  gr.DrawEllipse(pen, );
                }
            }
            gr.ResetTransform();
        }
    }
}
