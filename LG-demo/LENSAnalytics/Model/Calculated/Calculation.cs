﻿using LENSAnalytics.Model.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Calculated
{
    public static class Calculation
    {
        /// <summary>
        /// Возвращает среднее значение ближайшего сектора
        /// </summary>
        /// <param name="value">Значение, для которого ищется ближайшее</param>
        /// <param name="sectorsCount">Количество секторов</param>
        /// <param name="bottomBound">Нижняя граница значений</param>
        /// <param name="upperBound">Верхняя граница значений</param>
        /// <returns>Среднее значение ближайшего сектора</returns>
        public static double GetSector(double value, uint sectorsCount, double bottomBound, double upperBound)
        {
            if (sectorsCount == 0 || upperBound == bottomBound)
                throw new ArgumentException();
            var sectorSize = (upperBound - bottomBound) / sectorsCount;
            bottomBound -= sectorSize / 2;
            int nearestSector = (int)Math.Round((value - bottomBound) / sectorSize);
            return bottomBound + nearestSector * sectorSize;
        }

        /// <summary>
        /// Нормализует высоты точек к уровням сетки
        /// </summary>
        /// <param name="points">Набор точек</param>
        /// <param name="zNetSize">Количество уровней сетки по Z</param>
        /// <returns></returns>
        public static void NormalizeDataByZ(IEnumerable<TempTimeCylindricalPoint> points, uint zNetSize)
        {
            var timeDict = points
                  .GroupBy(p => p.Z > 0);
            foreach (var record in timeDict)
            {
                float zMin = record.Min(p => p.Z);
                float zMax = record.Max(p => p.Z);
                foreach (var p in record)
                    p.Z = (float)GetSector(p.Z, zNetSize, zMin, zMax);
            }
        }

        /// <summary>
        /// Удаляет дубликаты точек. Температура оставшейся точки приводится к среднему арифметическому дубликатов
        /// </summary>
        /// <param name="points">Набор точек</param>
        /// <param name="radiusEps">Разница между радиусами двух точек, при которой они считаются различными</param>
        /// <returns>Набор точек без дубликатов</returns>
        public static IEnumerable<TempTimeCylindricalPoint> RemoveDuplicatesByRadius(IEnumerable<TempTimeCylindricalPoint> points, float radiusEps = 0)
        {
            var pointsList = points
                .OrderBy(p => p.Time)
                .ThenBy(p => p.Angle)
                .ThenBy(p => p.Z)
                .ThenBy(p => p.R)
                .ToList();
            for (int i = 0; i < pointsList.Count; i++)
            {
                float temp = pointsList[i].Temp;
                for (int j = i + 1; j < pointsList.Count; j++)
                {
                    if (Math.Abs(pointsList[j].R - pointsList[i].R) > radiusEps
                        || pointsList[j].Z != pointsList[i].Z
                        || pointsList[j].Angle != pointsList[i].Angle
                        || pointsList[j].Time != pointsList[i].Time
                        || j + 1 == pointsList.Count)
                    {
                        int duplicateElementsCount = j - i;
                        pointsList.RemoveRange(i + 1, duplicateElementsCount - 1);
                        pointsList[i].Temp = temp / duplicateElementsCount;
                        break;
                    }
                    temp += pointsList[j].Temp;
                }
            }
            return pointsList;
        }

        /// <summary>
        /// Удаляет дубликаты точек по значению поля. Температура оставшейся точки приводится к среднему арифметическому дубликатов
        /// </summary>
        /// <param name="points">Набор точек</param>
        /// <param name="variable">Имя поля, по которому определяются дубликаты</param>
        /// <param name="eps">Разница между значениями поля двух точек, при которой они считаются различными</param>
        /// <returns>Набор точек без дубликатов</returns>
        public static IEnumerable<TempTimeCylindricalPoint> RemoveDuplicates(IEnumerable<TempTimeCylindricalPoint> points, string field, float eps = 0)
        {
            var pointsList = points
                .OrderBy(p => GetValueByKey(p, field))
                .ToList();
            for (int i = 0; i < pointsList.Count; i++)
            {
                float temp = pointsList[i].Temp;
                for (int j = i + 1; j < pointsList.Count; j++)
                {
                    if (Math.Abs(GetValueByKey(pointsList[j], field) - GetValueByKey(pointsList[i], field)) > eps
                        || j + 1 == pointsList.Count)
                    {
                        int duplicateElementsCount = j - i;
                        pointsList.RemoveRange(i + 1, duplicateElementsCount - 1);
                        pointsList[i].Temp = temp / duplicateElementsCount;
                        break;
                    }
                    temp += pointsList[j].Temp;
                }
            }         

            return pointsList;
        }

        public static float GetValueByKey(TempTimeCylindricalPoint point, string key)
        {
            switch (key)
            {
                case nameof(TempTimeCylindricalPoint.R):
                    return point.R;
                case nameof(TempTimeCylindricalPoint.Angle):
                    return point.Angle;
                case nameof(TempTimeCylindricalPoint.Z):
                    return point.Z;
                case nameof(TempTimeCylindricalPoint.Time):
                    return point.Time;
                default:
                    throw new ArgumentException(string.Format("Unknown '{0}' parameter value: {1}", nameof(key), key));
            }
        }


    }
}
