﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Drawing.Drawing2D;
using LENSAnalytics.Model.Geometry;
using System.Diagnostics;
using LENSAnalytics.Model.STLModel;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Configurations;

namespace LENSAnalytics.Model.Calculated
{
    public class MeltCutter : AbstractDrawer<TempTimeCylindricalPoint>
    {
        private const int GraduationOffset = 80;

        public MeltCutter(ref List<TempTimeCylindricalPoint> points, STLModel.STLModel backgroundModel, int imageWidth) : base(ref points, backgroundModel, imageWidth)
        {

        }

        protected override Bitmap GreateCanvas()
        {
            return new Bitmap(_imageWidth + GraduationOffset, (int)GetOffsetXY() * 2 + GraduationOffset);
        }

        protected override void CalculateMainTransform(ref List<TempTimeCylindricalPoint> points)
        {
            base.CalculateMainTransform(ref points);
            float offX = _mainLayerTransform.OffsetX + GraduationOffset;
            float offY = _mainLayerTransform.OffsetY;
            float scaleX = GetScaleXTransform(_mainLayerTransform);
            float scaleY = GetScaleYTransform(_mainLayerTransform);
            _mainLayerTransform.Reset();
            _mainLayerTransform.Translate(offX, offY);
            _mainLayerTransform.Scale(scaleX, scaleY);
        }

        protected override void CalculateBackroundTransform(ref List<TempTimeCylindricalPoint> points)
        {
            float canvasSize = GetOffsetXY() * 2;
            if (_backgroundModel == null)
                return;
        }

        protected override void DrawBackground(ref Graphics gr, float angle, ref IEnumerable<TempTimeCylindricalPoint> data)
        {
            gr.ResetTransform();
            gr.TranslateTransform(_mainLayerTransform.OffsetX, _mainLayerTransform.OffsetY);
            const int AxisResolution = 10;
            float offsetX = GetOffsetXY() + GraduationOffset / 2;
            float offsetY = GetOffsetXY()*2 - (_mainLayerTransform.OffsetY + GraduationOffset / 4);
            float scaleX = GetScaleXTransform(_mainLayerTransform);
            float scaleY = GetScaleYTransform(_mainLayerTransform);
            float graduationLineHeigth = 10f;
            AdjustableArrowCap _arrow = new AdjustableArrowCap(5, 5);
            Pen pen = new Pen(Brushes.Black, 4f) { CustomEndCap = _arrow };
            Pen netPen = new Pen(Brushes.Gray, 2f);
           
            PointF xAxisLeft = new PointF(-offsetX, offsetY);
            PointF xAxisRight = new PointF(offsetX, offsetY);
            PointF yAxisUp = new PointF(-offsetX, -offsetY + GraduationOffset / 2);
            PointF yAxisBottom = new PointF(xAxisLeft.X, xAxisLeft.Y);
            //axisX
            gr.DrawLine(pen, xAxisLeft, xAxisRight);
            using (Pen graduationPen = new Pen(pen.Brush, pen.Width))
            {
                Font font = new Font(FontFamily.GenericMonospace, AxisResolution * 2, FontStyle.Bold);
                float graduationStep = (xAxisRight.X - xAxisLeft.X) / (AxisResolution + 1);
                for (int i = 1; i <= AxisResolution; i++)
                {
                    var x = graduationStep*(i-AxisResolution/2);
                    var y1 = xAxisLeft.Y - graduationLineHeigth / 2;
                    var y2 = xAxisLeft.Y + graduationLineHeigth / 2;
                    gr.DrawLine(graduationPen, x, y1, x, y2);
                    gr.DrawLine(netPen, x, y2, x, yAxisUp.Y);
                    var value = (x*1e6 / scaleX).ToString("##0");
                    value = value.Split('e').First().Replace(',', '.');
                    gr.DrawString(value, font, pen.Brush, x - gr.MeasureString(value, font).Width / 2, y2);
                }
                string axisLegend = "R(мкм)";// + (graduationStep / scaleX).ToString("e2").Split('e').Last() + ")";
                var textSize = gr.MeasureString(axisLegend, font);
                gr.DrawString(axisLegend, font, pen.Brush, xAxisRight.X - graduationStep / 2, xAxisRight.Y - (textSize.Height + 10f));
            }
            //axisY           
            gr.DrawLine(pen, yAxisBottom, yAxisUp);
            using (Pen graduationPen = new Pen(pen.Brush, pen.Width))
            {
                Font font = new Font(FontFamily.GenericMonospace, AxisResolution * 2, FontStyle.Bold);
                float graduationStep = (yAxisBottom.Y - yAxisUp.Y) / (AxisResolution + 1);
                for (int i = 1; i <= AxisResolution; i++)
                {
                    var x1 = yAxisBottom.X - graduationLineHeigth / 2;
                    var x2 = yAxisBottom.X + graduationLineHeigth / 2;
                    var y = graduationStep * (i - AxisResolution / 2);
                    gr.DrawLine(graduationPen, x1, y, x2, y);
                    gr.DrawLine(netPen, x2, y, xAxisRight.X, y);
                    var value = (y*1e6 / scaleY).ToString("##0");
                    value = value.Split('e').First().Replace(',', '.');
                    gr.DrawString(value, font, pen.Brush,
                        yAxisBottom.X + graduationPen.Width / 2,
                        y - gr.MeasureString(value, font).Height - graduationPen.Width / 2);
                }
                string axisLegend = "Z(мкм)"; //+ Math.Abs(graduationStep / scaleY).ToString("e2").Split('e').Last() + ")";
                var textSize = gr.MeasureString(axisLegend, font);
                gr.DrawString(axisLegend, font, pen.Brush,
                    yAxisUp.X +10f,
                    yAxisUp.Y);
            }
        }

        protected override void DrawContoursLayer(ref Graphics gr, ref IEnumerable<TempTimeCylindricalPoint> points)
        {

        }

        protected override void DrawMainLayer(ref Graphics gr, ref IEnumerable<TempTimeCylindricalPoint> data, float minValue, float maxValue)
        {
            gr.ResetTransform();
            gr.TranslateTransform(_mainLayerTransform.OffsetX, _mainLayerTransform.OffsetY);
            DrawCircles(ref gr, data, minValue, maxValue);
            gr.ResetTransform();
        }

        protected override void DrawPalleteLegend(ref Graphics gr, float minValue, float maxValue)
        {
            var legendWidth = _imageWidth - GetOffsetXY() * 2;
            var legendHeigth = GetOffsetXY();
            gr.DrawImage(GetPaletteLegend(minValue, maxValue, (int)legendWidth, (int)legendHeigth),
                new PointF(GetOffsetXY() * 2 + GraduationOffset + 20, GetOffsetXY() * 0.5f + GraduationOffset / 2));
        }

        private void DrawCircles(ref Graphics gr, IEnumerable<TempTimeCylindricalPoint> data, float minValue, float maxValue)
        {
            float scaleX = GetScaleXTransform(_mainLayerTransform);
            float scaleY = GetScaleYTransform(_mainLayerTransform);
            const float MaxPointSizeOffset = 22f;
            const float MinPointSize = 12f;
            var linearData = data.Where(p => p.Z <= 0);
            float pointSize = 55f;
            foreach (var point in linearData)
            {
                using (Brush pen = new SolidBrush(RGBFromTemp(minValue, maxValue, point.Temp)))
                {
                    if (point.Angle >= Math.PI)
                        gr.FillPie(pen,
                            point.RZProjection().X * scaleX - pointSize / 2,
                            point.RZProjection().Y * scaleY - pointSize / 2,
                            pointSize, pointSize,
                            100,
                            -110);
                    else
                        gr.FillPie(pen,
                        point.RZProjection().X * scaleX - pointSize / 2,
                        point.RZProjection().Y * scaleY - pointSize / 2,
                        pointSize, pointSize,
                        80,
                        110);
                }
            }
            const int AngleSectorsCount = 20;
            var polarDataGroup = data
                .Where(p => p.Z > 0)
                .OrderByDescending(p => p.Z)
                .Select(p => new { Temp = p.Temp, Position = new PolarPoint2D(p.RZProjection()) })
                .GroupBy(p => Calculation.GetSector(p.Position.Angle, AngleSectorsCount, 0, Math.PI));
            Dictionary<double, float> sectorMaxR = new Dictionary<double, float>();
            foreach (var record in polarDataGroup)
            {
                var polarData = record.ToArray();
                float maxR = polarData.Max(p => p.Position.R);
                if (maxR == 0) maxR = 1;
                sectorMaxR.Add(record.Key, maxR);
            }
            foreach (var point in polarDataGroup.SelectMany(g => g.ToArray()).OrderByDescending(p => p.Position.Y))
            {
                var maxR = sectorMaxR[Calculation.GetSector(point.Position.Angle, AngleSectorsCount, 0, Math.PI)];
                pointSize = MinPointSize + (point.Position.R / maxR) * MaxPointSizeOffset;
                using (Brush pen = new SolidBrush(RGBFromTemp(minValue, maxValue, point.Temp)))
                {
                    gr.FillPie(pen, point.Position.X * scaleX - pointSize / 2,
                        point.Position.Y * scaleY - pointSize / 2,
                        pointSize, pointSize,
                        0,
                        -180);
                }
            }
        }
    }
}
