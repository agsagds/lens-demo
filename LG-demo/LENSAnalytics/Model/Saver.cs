﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class Saver : ISaver
    {
        public void Save(string path, Gas gas, Laser laser, Material material, Melt melt, Powder powder)
        {
            try
            {
                StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default);
                //Gas
                //--single properties
                sw.WriteLine("{0}", gas.Temperature);
                sw.WriteLine("{0}", gas.Transcalency);
                sw.WriteLine("{0}", gas.Viscosity);
                sw.WriteLine("{0}", gas.HeatCapacity);
                //--addition parameters
                sw.WriteLine("{0}", gas.AdditionParameters.Count);
                foreach(var parameter in gas.AdditionParameters)
                    sw.WriteLine("{0}", parameter.Value);
                //Powder
                //--density
                sw.WriteLine("{0}", powder.Density.Count);
                foreach(var record in powder.Density)
                    sw.WriteLine("{0} {1}", record.Value, record.Key);
                //--heat capacity
                sw.WriteLine("{0}", powder.HeatCapacity.Count);
                foreach (var record in powder.HeatCapacity)
                    sw.WriteLine("{0} {1}", record.Value, record.Key);
                //--transcalency
                sw.WriteLine("{0}", powder.Transcalency.Count);
                foreach (var record in powder.Transcalency)
                    sw.WriteLine("{0} {1}", record.Value, record.Key);
                //--emissivity
                sw.WriteLine("{0}", powder.Emissivity.Count);
                foreach (var record in powder.Emissivity)
                    sw.WriteLine("{0} {1}", record.Value, record.Key);
                //--single properties
                sw.WriteLine("{0}", powder.MeltingPoint);
                sw.WriteLine("{0}", powder.LiquidViscosity);
                sw.WriteLine("{0}", powder.LiquidTranscalency);
                sw.WriteLine("{0}", powder.HeatMelting);
                sw.WriteLine("{0}", powder.ConvectionHeatTransfer);
                sw.WriteLine("{0}", powder.LiquidConvectionHeatTransfer);
                sw.WriteLine("{0}", powder.Flow.InitialTemperature);
                //--particles composition
                sw.WriteLine("{0}", powder.ParticlesComposition.Count);
                foreach (var record in powder.ParticlesComposition)
                    sw.WriteLine("{0} {1}", record.Key, record.Value);
                //--powder flow parameters
                sw.WriteLine("{0}", powder.Flow.MassFlow);
                sw.WriteLine("{0}", powder.Flow.MidFlow);
                sw.WriteLine("{0}", powder.Flow.MidDiffRadius);
                //--powder flow function
                sw.WriteLine("{0}", powder.Flow.SpreadFunction.Count);
                foreach (var record in powder.Flow.SpreadFunction)
                    sw.WriteLine("{0} {1}", record.Key, record.Value);
                //--powder flow single parameter
                sw.WriteLine("{0}", powder.Flow.SpreadValue);
                //--addition parameters
                sw.WriteLine("{0}", powder.AdditionParameters.Count+powder.Flow.AdditionParameters.Count);
                foreach (var parameter in powder.AdditionParameters)
                    sw.WriteLine("{0}", parameter.Value);
                foreach (var parameter in powder.Flow.AdditionParameters)
                    sw.WriteLine("{0}", parameter.Value);
                //Laser
                //--single parameters
                sw.WriteLine("{0}", laser.Diameter);
                sw.WriteLine("{0}", laser.Power);
                sw.WriteLine("{0}", laser.Velocity);
                sw.WriteLine("{0}", laser.Step);
                sw.WriteLine("{0}", laser.Dispersion);
                //--addition parameters
                sw.WriteLine("{0}", laser.AdditionParameters.Count);
                foreach (var parameter in laser.AdditionParameters)
                    sw.WriteLine("{0}", parameter.Value);
                //Melt
                //--single parameters
                sw.WriteLine("{0}", melt.Density);
                sw.WriteLine("{0}", melt.HeatCapacity);
                sw.WriteLine("{0}", melt.Transcalency);
                sw.WriteLine("{0}", melt.Emissivity);
                sw.WriteLine("{0}", melt.LaserEmissionAbsorbability);
                sw.WriteLine("{0}", melt.LaserSpotRadius);
                sw.WriteLine("{0}", melt.MeltEmissivity);
                sw.WriteLine("{0}", melt.HeatMelting);
                sw.WriteLine("{0}", melt.HeatBoiling);
                sw.WriteLine("{0}", melt.MolarMass);
                sw.WriteLine("{0}", melt.MolarHeatBoiling);
                sw.WriteLine("{0}", melt.WettingAngle);
                sw.WriteLine("{0}", melt.SurfaceTension);
                //--addition parameters
                sw.WriteLine("{0}", melt.AdditionParameters.Count);
                foreach (var parameter in melt.AdditionParameters)
                    sw.WriteLine("{0}", parameter.Value);
                //Material
                //--density
                sw.WriteLine("{0}", material.Density.Count);
                foreach (var record in material.Density)
                    sw.WriteLine("{0} {1}", record.Key, record.Value);
                //--heat capacity
                sw.WriteLine("{0}", material.HeatCapacity.Count);
                foreach (var record in material.HeatCapacity)
                    sw.WriteLine("{0} {1}", record.Key, record.Value);
                //--transcalency
                sw.WriteLine("{0}", material.Transcalency.Count);
                foreach (var record in material.Transcalency)
                    sw.WriteLine("{0} {1}", record.Key, record.Value);
                //--single parameters
                sw.WriteLine("{0}", material.Emissivity);
                //--addition parameters
                sw.WriteLine("{0}", material.AdditionParameters.Count);
                foreach (var parameter in material.AdditionParameters)
                    sw.WriteLine("{0}", parameter.Value);
                sw.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    throw new DataSaveException();
                }
            }
        }
    }
}
