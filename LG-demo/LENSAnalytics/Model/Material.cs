﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model.Validators;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class Material : InputDataBase
    {
        public Material()
        {
            Transcalency = new ObservableCollection<Parameter>();
            HeatCapacity = new ObservableCollection<Parameter>();
            Density = new ObservableCollection<Parameter>();
        }

        /// <summary>
        /// Теплопроводность
        /// </summary>
        public ObservableCollection<Parameter> Transcalency { get; private set; }
        /// <summary>
        /// Теплоёмкость
        /// </summary>
        public ObservableCollection<Parameter> HeatCapacity { get; private set; }
        /// <summary>
        /// Плотность
        /// </summary>
        public ObservableCollection<Parameter> Density { get; private set; }
        /// <summary>
        /// Степень черноты поверхности
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Emissivity { get; set; }

        public override void Save(string path)
        {
            try
            {
                base.Save(path);
                StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Unicode);
                //Transcalency
                sw.WriteLine("{0}", Transcalency.Count);
                foreach (var parameter in Transcalency)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                //Heat Capacity
                sw.WriteLine("{0}", HeatCapacity.Count);
                foreach (var parameter in HeatCapacity)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                //Density
                sw.WriteLine("{0}", Density.Count);
                foreach (var parameter in Density)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                sw.WriteLine("{0}:{1}", nameof(Emissivity), Emissivity);
                sw.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    throw new DataSaveException();
                }
                throw;
            }
        }

        public override void Load(string path)
        {
            try
            {
                base.Load(path);
                StreamReader sr = new StreamReader(path, System.Text.Encoding.Unicode);
                int count = Int16.Parse(sr.ReadLine());
                while (count-- > 0)
                    sr.ReadLine();
                //Transcalency
                count = Int16.Parse(sr.ReadLine());
                Transcalency.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    Transcalency.Add(parameter);
                }
                //Heat Capacity
                count = Int16.Parse(sr.ReadLine());
                HeatCapacity.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    HeatCapacity.Add(parameter);
                }
                //Density
                count = Int16.Parse(sr.ReadLine());
                Density.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    Density.Add(parameter);
                }
                Dictionary<string, decimal?> records = new Dictionary<string, decimal?>();
                while (!sr.EndOfStream)
                {
                    var record = sr.ReadLine().Split(':');
                    records[record[0]] = decimal.Parse(record[1]);
                }
                Emissivity = records[nameof(Emissivity)];
                sr.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException || ex is KeyNotFoundException || ex is IndexOutOfRangeException || ex is FormatException)
                {
                    throw new DataLoadException();
                }
                throw;
            }
        }

        public override string GetExtension() => ".matinfo";

        public override Dictionary<string, string> ToDictionary()
        {
            var dict = base.ToDictionary();
            dict.Add(Resources.Emissivity, Emissivity.ToString());           
            return dict;
        }
    }
}
