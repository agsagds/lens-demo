﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model.Validators;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class Powder : InputDataBase
    {        
        public Powder()
        {
            Flow = new PowderFlow();
            ParticlesComposition = new ObservableCollection<Parameter>();
            Density = new ObservableCollection<Parameter>();
            Emissivity = new ObservableCollection<Parameter>();
            Transcalency = new ObservableCollection<Parameter>();
            HeatCapacity = new ObservableCollection<Parameter>();
        }
        /// <summary>
        /// Параметры потока частиц порошка
        /// </summary>
        public PowderFlow Flow { get; private set; }
        /// <summary>
        /// Конвективный коэффициент теплоотдачи поверхности в жидком состоянии
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? LiquidConvectionHeatTransfer { get; set; }
        /// <summary>
        /// Конвективный коэффициент теплоотдачи поверхности в твёрдом состоянии
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? ConvectionHeatTransfer { get; set; }
        /// <summary>
        /// Удельная теплота плавления
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? HeatMelting { get; set; }
        /// <summary>
        /// Теплопроводность в жидком состоянии
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? LiquidTranscalency { get; set; }
        /// <summary>
        /// Вязкость в жидком состоянии
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? LiquidViscosity { get; set; }
        /// <summary>
        /// Температура плавления
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? MeltingPoint { get; set; }
        /// <summary>
        /// Состав частиц: диаметр, доля
        /// </summary>
        public ObservableCollection<Parameter> ParticlesComposition { get; private set; }
        /// <summary>
        /// Плотность
        /// </summary>
        public ObservableCollection<Parameter> Density { get; private set; }
        /// <summary>
        /// Степень черноты поверхности
        /// </summary>
        public ObservableCollection<Parameter> Emissivity { get; private set; }
        /// <summary>
        /// Теплопроводность
        /// </summary>
        public ObservableCollection<Parameter> Transcalency { get; private set; }
        /// <summary>
        /// Теплоёмкость
        /// </summary>
        public ObservableCollection<Parameter> HeatCapacity { get; private set; }

        public override void Save(string path)
        {
            try
            {
                base.Save(path);
                StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Unicode);
                //Particles composition
                sw.WriteLine("{0}", ParticlesComposition.Count);
                foreach(var parameter in ParticlesComposition)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                //Density
                sw.WriteLine("{0}", Density.Count);
                foreach (var parameter in Density)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                //Emissivity
                sw.WriteLine("{0}", Emissivity.Count);
                foreach (var parameter in Emissivity)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                //Transcalency
                sw.WriteLine("{0}", Transcalency.Count);
                foreach (var parameter in Transcalency)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                //HeatCapacity
                sw.WriteLine("{0}", HeatCapacity.Count);
                foreach (var parameter in HeatCapacity)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                //Fields
                sw.WriteLine("{0}:{1}", nameof(LiquidConvectionHeatTransfer), LiquidConvectionHeatTransfer);
                sw.WriteLine("{0}:{1}", nameof(ConvectionHeatTransfer), ConvectionHeatTransfer);
                sw.WriteLine("{0}:{1}", nameof(HeatMelting), HeatMelting);
                sw.WriteLine("{0}:{1}", nameof(LiquidTranscalency), LiquidTranscalency);
                sw.WriteLine("{0}:{1}", nameof(LiquidViscosity), LiquidViscosity);
                sw.WriteLine("{0}:{1}", nameof(MeltingPoint), MeltingPoint);
                sw.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    throw new DataSaveException();
                }
                throw;
            }            
            Flow.Save(path.Replace(GetExtension(), Flow.GetExtension()));
        }

        public override void Load(string path)
        {
            try
            {
                base.Load(path);
                StreamReader sr = new StreamReader(path, System.Text.Encoding.Unicode);
                //Addition parameters
                int count = Int16.Parse(sr.ReadLine());
                while (count-- > 0)
                    sr.ReadLine();
                //Particles composition
                count = Int16.Parse(sr.ReadLine());
                ParticlesComposition.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    ParticlesComposition.Add(parameter);
                }               
                //Density
                count = Int16.Parse(sr.ReadLine());
                Density.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    Density.Add(parameter);
                }
                //Emissivity
                count = Int16.Parse(sr.ReadLine());
                Emissivity.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    Emissivity.Add(parameter);
                }
                //Transcalency
                count = Int16.Parse(sr.ReadLine());
                Transcalency.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    Transcalency.Add(parameter);
                }
                //HeatCapacity
                count = Int16.Parse(sr.ReadLine());
                HeatCapacity.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    HeatCapacity.Add(parameter);
                }
                //Fields
                Dictionary<string, decimal?> records = new Dictionary<string, decimal?>();
                while (!sr.EndOfStream)
                {
                    var record = sr.ReadLine().Split(':');
                    records[record[0]] = decimal.Parse(record[1]);
                }
                LiquidConvectionHeatTransfer = records[nameof(LiquidConvectionHeatTransfer)];
                ConvectionHeatTransfer = records[nameof(ConvectionHeatTransfer)];
                LiquidViscosity = records[nameof(LiquidViscosity)];
                HeatMelting = records[nameof(HeatMelting)];
                LiquidTranscalency = records[nameof(LiquidTranscalency)];
                MeltingPoint = records[nameof(MeltingPoint)];
                sr.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException || ex is KeyNotFoundException || ex is IndexOutOfRangeException || ex is FormatException)
                {
                    throw new DataLoadException();
                }
                throw;
            }
            Flow.Load(path.Replace(GetExtension(), Flow.GetExtension()));
        }

        public override string GetExtension() => ".powderinfo";

        public override Dictionary<string, string> ToDictionary()
        {
            var dict = base.ToDictionary();
            dict.Add(Resources.LiquidConvectionHeatTransfer, LiquidConvectionHeatTransfer.ToString());
            dict.Add(Resources.ConvectionHeatTransfer, ConvectionHeatTransfer.ToString());
            dict.Add(Resources.LiquidViscosity, LiquidViscosity.ToString());
            dict.Add(Resources.HeatMelting, HeatMelting.ToString());
            dict.Add(Resources.LiquidTranscalency, LiquidTranscalency.ToString());
            dict.Add(Resources.MeltingPoint, MeltingPoint.ToString());
            return dict;
        }
    }
}
