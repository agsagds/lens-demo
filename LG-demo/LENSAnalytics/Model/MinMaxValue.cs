﻿using LENSAnalytics.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class MinMaxValue : NotifyModel
    {
        public delegate void ValueWasChanged();
        public event ValueWasChanged ValueChanged;

        private float _value;
        public float Min { get; private set; }
        public float Max { get; private set; }
        public float Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (_value == value)
                    return;
                _value = value;
                NotifyPropertyChanged();
                ValueChanged?.Invoke();
            }
        }

        public void CopyRange(MinMaxValue value)
        {
           
            Max = value.Max;
            NotifyPropertyChanged(nameof(Max));
            Min = value.Min;
            NotifyPropertyChanged(nameof(Min));
            if (Value == value.Value)
                ValueChanged?.Invoke();
            Value = value.Value;
            

        }

        public MinMaxValue(float min, float max)
        {
            Min = min;
            Max = max;
            Value = min;
        }
    }
}
