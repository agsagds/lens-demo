﻿using LENSAnalytics.Model.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class ReservData
    {
        /// <summary>
        /// Название резервного поля
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Value { get; set; }
    }
}
