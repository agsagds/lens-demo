﻿using LENSAnalytics.Model.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class Parameter
    {
        /// <summary>
        /// Значение параметра
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal Value { get; set; }
        /// <summary>
        /// Ключ, для которого определено значение
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal Key { get; set; }
    }
}
