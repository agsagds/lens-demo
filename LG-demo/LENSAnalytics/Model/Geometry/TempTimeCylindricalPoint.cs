﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class TempTimeCylindricalPoint : CylindricalPoint
    {
        public static int BytesSize = 20;

        public float Temp { get; set; }
        public float Time { get; set; }

        public TempTimeCylindricalPoint()
        {
            Temp = 0;
            Time = 0;
        }

        public TempTimeCylindricalPoint(byte[] bytes)
        {
            if (bytes.Count() != BytesSize)
                throw new ArgumentException(String.Format("Размер переданного в конструктор массива не равен {0}.", BytesSize));

            Temp = BitConverter.ToSingle(bytes, 0);
            R = BitConverter.ToSingle(bytes, 4);
            Angle = BitConverter.ToSingle(bytes, 8);
            Z = BitConverter.ToSingle(bytes, 12);
            Time = BitConverter.ToSingle(bytes, 16);
        }

        public TempTimeCylindricalPoint(TempTimeCylindricalPoint point)
        {
            Temp = point.Temp;
            R = point.R;
            Angle = point.Angle;
            Z = point.Z;
            Time = point.Time;
        }
    }
}
