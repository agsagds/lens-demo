﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class CylindricalVertex : CylindricalPoint
    {
        public CylindricalVertex(float radius, float angle, float z) : base(radius, angle, z)
        { }

        /// <summary>
        /// Создаёт структуру из массива байтов.
        /// </summary>
        /// <param name="bytes">Байтовый массив [Radius,Angle,Z]</param>
        public CylindricalVertex(byte[] bytes) : base(bytes)
        { }

    }
}
