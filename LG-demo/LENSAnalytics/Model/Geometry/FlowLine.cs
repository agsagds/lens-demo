﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class FlowLine : TempTimeCylindricalPoint
    {
        public static int BytesSize = 28;

        public CylindricalVertex Speed { get; private set; }

        public FlowLine(byte[] bytes)
        {
            if (bytes.Count() != BytesSize)
                throw new ArgumentException(String.Format("Размер переданного в конструктор массива не равен {0}.", BytesSize));

            Temp = BitConverter.ToSingle(bytes, 0);
            R = BitConverter.ToSingle(bytes, 4 + CylindricalVertex.BytesSize);
            Angle = BitConverter.ToSingle(bytes, 8 + CylindricalVertex.BytesSize);
            Z = BitConverter.ToSingle(bytes, 12 + CylindricalVertex.BytesSize);

            byte[] vertexBuf = new byte[CylindricalVertex.BytesSize];
            Array.Copy(bytes, 4, vertexBuf, 0, CylindricalVertex.BytesSize);
            Speed = new CylindricalVertex(vertexBuf);
        }

    }
}
