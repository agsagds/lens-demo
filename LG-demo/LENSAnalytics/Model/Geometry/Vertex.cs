﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class Vertex : Point3D
    {
        public Vertex(double x, double y, double z) : base(x, y, z) { }
        public Vertex(Vertex v) : base(v.X, v.Y, v.Z) { }
    }
}
