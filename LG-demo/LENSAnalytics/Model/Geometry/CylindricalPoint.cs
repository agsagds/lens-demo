﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class CylindricalPoint : PolarPoint2D
    {
        public static int BytesSize=12;

        /// <summary>
        /// Высота
        /// </summary>
        public float Z { get; set; }

        public CylindricalPoint()
        {
            Z = 0;
        }

        public CylindricalPoint(float radius, float angle, float z) : base(radius, angle)
        {
            Z = z;
        }

        public CylindricalPoint(Point3D point):base(point)
        {
            Z = (float)point.Z;
        }

        /// <summary>
        /// Создаёт структуру из массива байтов.
        /// </summary>
        /// <param name="bytes">Байтовый массив [Radius,Angle,Z]</param>
        public CylindricalPoint(byte[] bytes)
        {
            if (bytes.Count() != BytesSize)
                throw new ArgumentException(String.Format("Размер переданного в конструктор массива не равен {0}.", BytesSize));

            R = BitConverter.ToSingle(bytes, 0);
            Angle = BitConverter.ToSingle(bytes, 4);
            Z = BitConverter.ToSingle(bytes, 8);
        }

        /// <summary>
        /// Возвращает картезианские координаты на плоскости RZ (R->X, Z->Y), которой принадлежит данная точка
        /// </summary>
        /// <returns></returns>
        public PointF RZProjection()
        {
            return new PointF(Angle < Math.PI ? R : -R, Z);
        }

        /// <summary>
        /// Возвращает картезианские координаты на плоскости RZ (R->X, Z->Y), которой принадлежит данная точка
        /// </summary>
        /// <param name="externalAngle">угол точки начала вектора относительно оси</param>
        /// <returns></returns>
        public PointF RZProjection(float externalAngle)
        {
            var p = new PointF(Angle < Math.PI ? R : -R, Z);
            if (externalAngle > Math.PI)
                p.X *= -1;
            return p;
        }
    }
}
