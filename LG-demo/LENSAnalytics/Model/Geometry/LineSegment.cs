﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class LineSegment
    {
        public Point3D P1 { get; private set; }
        public Point3D P2 { get; private set; }
        
        public LineSegment(Point3D p1, Point3D p2)
        {
            P1 = p1;
            P2 = p2;
        }
    }
}
