﻿using LENSAnalytics.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class Triangle
    {
        public Triangle(Vertex[] vertexes)
        {
            Vertexes = vertexes;
        }

        public Vertex[] Vertexes { get; private set; }

        public Vector Normal()
        {
            Vector a = new Vector(Vertexes[0], Vertexes[1]);
            Vector b = new Vector(Vertexes[0], Vertexes[2]);            
            return new Vector(  a.Y * b.Z - a.Z * b.Y,
                                a.Z * b.X - a.X * b.Z,
                                a.X * b.Y - a.Y * b.X);
        }

        /// <summary>
        /// Проверяет факт пересечения с плоскостью перпендикулярной OZ
        /// </summary>
        /// <param name="planeZ">Высота плоскости по OZ</param>
        /// <returns>true - трeугольник пересекается(касается) с плоскостью</returns>
        public bool IntersectPlaneZ(double planeZ)
        {
            double d1 = Vertexes[0].Z - planeZ,
                    d2 = Vertexes[1].Z - planeZ,
                    d3 = Vertexes[2].Z - planeZ;
            if (d1 == d2 && d2 == d3 && d1 == 0)
                throw new PlanesEqualException();
            return Math.Max(d1, Math.Max(d2, d3)) * Math.Min(d1, Math.Min(d2, d3)) > 0;
        }
    }
}
