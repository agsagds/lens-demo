﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class Vector
    {

        public Point3D From { get; private set; }

        public Point3D To { get; private set; }

        public double X
        {
            get
            {
                return To.X - From.X;
            }
        }

        public double Y
        {
            get
            {
                return To.Y - From.Y;
            }
        }

        public double Z
        {
            get
            {
                return To.Z - From.Z;
            }
        }

        public Vector(double lx, double ly, double lz)
        {
            From = new Point3D(0, 0, 0);
            To = new Point3D(lx, ly, lz);
        }

        public Vector(Point3D from, Point3D to)
        {
            From = from;
            To = to;
        }

        public double ScalarMul(Vector v) => v.X * X + v.Y * Y + v.Z * Z;
        
    }
}
