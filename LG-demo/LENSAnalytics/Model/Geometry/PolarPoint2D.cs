﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Geometry
{
    public class PolarPoint2D
    {
        /// <summary>
        /// Радиус
        /// </summary>
        public float R { get; set; }
        /// <summary>
        /// Угол, радианы
        /// </summary>
        public float Angle { get; set; }

        public float X
        {
            get
            {
                return R * (float)Math.Cos(Angle);
            }
        }

        public float Y
        {
            get
            {
                return R * (float)Math.Sin(Angle);
            }
        }

        public PolarPoint2D()
        {
            R = 1;
            Angle = 0;
        }

        public PolarPoint2D(float radius, float angle)
        {
            R = radius;
            Angle = angle;
        }

        /// <summary>
        /// Получение точки в полярных координатах из точки в декартовых
        /// взято отсюда: https://ru.wikipedia.org/wiki/Полярная_система_координат
        /// </summary>
        /// <param name="pointF">Точка в декартовых координатах</param>
        public PolarPoint2D(System.Drawing.PointF point)
        {
            R = (float)Math.Sqrt(point.X * point.X + point.Y * point.Y);
            if (point.X == 0)
            {
                if (point.Y > 0)
                    Angle = (float)Math.PI / 2;
                else if (point.Y < 0)
                    Angle = 3 * (float)Math.PI / 2;
                else
                    Angle = 0;
            }
            else
            {
                if (point.X < 0)
                    Angle = (float)Math.Atan(point.Y / point.X) + (float)Math.PI;
                else
                {
                    if (point.Y < 0)
                        Angle = (float)Math.Atan(point.Y / point.X) + (float)Math.PI * 2;
                    else
                        Angle = (float)Math.Atan(point.Y / point.X);
                }
            }
        }

        /// <summary>
        /// Получение точки в полярных координатах из проекции точки в декартовых
        /// взято отсюда: https://ru.wikipedia.org/wiki/Полярная_система_координат
        /// </summary>
        /// <param name="point3D">Точка в декартовых координатах</param>
        public PolarPoint2D(Point3D point3D) : this(new System.Drawing.PointF((float)point3D.X, (float)point3D.Y))
        { }

        public PolarPoint2D(PolarPoint2D point)
        {
            Angle = point.Angle;
            R = point.R;
        }
    }
}
