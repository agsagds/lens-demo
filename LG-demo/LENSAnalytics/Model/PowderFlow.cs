﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model.Validators;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class PowderFlow : InputDataBase
    {
        public PowderFlow()
        {
            SpreadFunction = new ObservableCollection<Parameter>();
        }

        /// <summary>
        /// Начальная температура
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? InitialTemperature { get; set; }
        /// <summary>
        /// Массовый поток частиц в точке подачи
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? MassFlow { get; set; }
        /// <summary>
        /// Средняя подача порошка в еденицу времени
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? MidFlow { get; set; }
        /// <summary>
        /// Средней характерный радиус разлета частиц
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? MidDiffRadius { get; set; }
        /// <summary>
        /// Функция распределения частиц при подаче
        /// </summary>
        public ObservableCollection<Parameter> SpreadFunction { get; private set; }
        /// <summary>
        /// Коэффициент, характеризующий степень разлета
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? SpreadValue { get; set; }

        public override void Save(string path)
        {
            try
            {
                base.Save(path);
                StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Unicode);
                //Spread function
                sw.WriteLine("{0}", SpreadFunction.Count);
                foreach (var parameter in SpreadFunction)
                    sw.WriteLine("{0}:{1}", parameter.Key, parameter.Value);
                sw.WriteLine("{0}:{1}", nameof(InitialTemperature), InitialTemperature);
                sw.WriteLine("{0}:{1}", nameof(MassFlow), MassFlow);
                sw.WriteLine("{0}:{1}", nameof(MidFlow), MidFlow);
                sw.WriteLine("{0}:{1}", nameof(MidDiffRadius), MidDiffRadius);
                sw.WriteLine("{0}:{1}", nameof(SpreadValue), SpreadValue);
                sw.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    throw new DataSaveException();
                }
                throw;
            }
        }

        public override void Load(string path)
        {
            try
            {
                base.Load(path);
                StreamReader sr = new StreamReader(path, System.Text.Encoding.Unicode);
                int count = Int16.Parse(sr.ReadLine());
                while (count-- > 0)
                    sr.ReadLine();
                //Spread Function
                count = Int16.Parse(sr.ReadLine());
                SpreadFunction.Clear();
                while (count-- > 0)
                {
                    var record = sr.ReadLine().Split(':');
                    var parameter = new Parameter();
                    parameter.Key = Decimal.Parse(record[0]);
                    parameter.Value = Decimal.Parse(record[1]);
                    SpreadFunction.Add(parameter);
                }
                //single parameters
                Dictionary<string, decimal?> records = new Dictionary<string, decimal?>();
                while (!sr.EndOfStream)
                {
                    var record = sr.ReadLine().Split(':');
                    records[record[0]] = decimal.Parse(record[1]);
                }                
                InitialTemperature = records[nameof(InitialTemperature)];
                MassFlow = records[nameof(MassFlow)];
                MidFlow = records[nameof(MidFlow)];
                MidDiffRadius = records[nameof(MidDiffRadius)];
                SpreadValue = records[nameof(SpreadValue)];
                sr.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException || ex is KeyNotFoundException || ex is IndexOutOfRangeException || ex is FormatException)
                {
                    throw new DataLoadException();
                }
                throw;
            }
        }

        public override string GetExtension() => ".flowinfo";

        public override Dictionary<string, string> ToDictionary()
        {
            var dict = base.ToDictionary();
            dict.Add(Resources.InitialTemperature, InitialTemperature.ToString());
            dict.Add(Resources.MassFlow, MassFlow.ToString());
            dict.Add(Resources.MidFlow, MidFlow.ToString());
            dict.Add(Resources.MidDiffRadius, MidDiffRadius.ToString());
            dict.Add(Resources.SpreadValue, SpreadValue.ToString());
            return dict;
        }
    }
}
