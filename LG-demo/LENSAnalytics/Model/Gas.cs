﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model.Validators;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace LENSAnalytics.Model
{
    public class Gas : InputDataBase
    {
        /// <summary>
        /// Температура
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Temperature
        {
            get; set;
        }
        /// <summary>
        /// Вязкость
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Viscosity
        {
            get; set;
        }
        /// <summary>
        /// Теплопроводность
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Transcalency
        {
            get; set;
        }
        /// <summary>
        /// Теплоёмкость
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? HeatCapacity
        {
            get; set;
        }

        public override void Save(string path)
        {
            try
            {
                base.Save(path);
                StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Unicode);
                sw.WriteLine("{0}:{1}", nameof(Temperature), Temperature);
                sw.WriteLine("{0}:{1}", nameof(Viscosity), Viscosity);
                sw.WriteLine("{0}:{1}", nameof(Transcalency), Transcalency);
                sw.WriteLine("{0}:{1}", nameof(HeatCapacity), HeatCapacity);
                sw.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    throw new DataSaveException();
                }
                throw;
            }
        }

        public override void Load(string path)
        {
            try
            {
                base.Load(path);
                StreamReader sr = new StreamReader(path, System.Text.Encoding.Unicode);
                int count = Int16.Parse(sr.ReadLine());
                while (count-- > 0)
                    sr.ReadLine();
                Dictionary<string, decimal?> records = new Dictionary<string, decimal?>();
                while (!sr.EndOfStream)
                {
                    var record = sr.ReadLine().Split(':');
                    records[record[0]] = decimal.Parse(record[1]);
                }
                Temperature = records[nameof(Temperature)];
                Viscosity = records[nameof(Viscosity)];
                Transcalency = records[nameof(Transcalency)];
                HeatCapacity = records[nameof(HeatCapacity)];
                sr.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException || ex is KeyNotFoundException || ex is IndexOutOfRangeException || ex is FormatException)
                {
                    throw new DataLoadException();
                }
                throw;
            }
        }

        public override string GetExtension() => ".gasinfo";

        public override Dictionary<string, string> ToDictionary()
        {
            var dict=base.ToDictionary();
            dict.Add(Resources.Temperature, Temperature.ToString());
            dict.Add(Resources.Viscosity, Viscosity.ToString());
            dict.Add(Resources.Transcalency, Transcalency.ToString());
            dict.Add(Resources.HeatCapacity, HeatCapacity.ToString());
            return dict;
        }
    }
}
