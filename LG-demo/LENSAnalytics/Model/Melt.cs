﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model.Validators;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class Melt : InputDataBase
    {

        /// <summary>
        /// Степень черноты поверхности
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Emissivity { get; set; }
        /// <summary>
        /// Теплоёмкость
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? HeatCapacity { get; set; }
        /// <summary>
        /// Плотность
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Density { get; set; }
        /// <summary>
        /// Удельная теплота плавления
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? HeatMelting { get; set; }
        /// <summary>
        /// Удельная теплота испарения
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? HeatBoiling { get; set; }
        /// <summary>
        /// Молярная масса
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? MolarMass { get; set; }
        /// <summary>
        /// Молярная теплота испарения
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? MolarHeatBoiling { get; set; }
        /// <summary>
        /// Краевой угол
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? WettingAngle { get; set; }
        /// <summary>
        /// Коэффициент поверхностного натяжения
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? SurfaceTension { get; set; }
        /// <summary>
        /// Линейный коэффициент поглощения лазерного излучения
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? LaserEmissionAbsorbability { get; set; }
        /// <summary>
        /// Теплопроводность
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Transcalency { get; set; }
        /// <summary>
        /// Cтепень черноты поверхности расплава
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? MeltEmissivity { get; set; }
        /// <summary>
        /// Радиус лазерного пятна
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? LaserSpotRadius { get; set; }

        public override void Save(string path)
        {
            try
            {
                base.Save(path);
                StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Unicode);
                sw.WriteLine("{0}:{1}", nameof(Emissivity), Emissivity);
                sw.WriteLine("{0}:{1}", nameof(HeatCapacity), HeatCapacity);
                sw.WriteLine("{0}:{1}", nameof(Density), Density);
                sw.WriteLine("{0}:{1}", nameof(HeatMelting), HeatMelting);
                sw.WriteLine("{0}:{1}", nameof(HeatBoiling), HeatBoiling);
                sw.WriteLine("{0}:{1}", nameof(MolarMass), MolarMass);
                sw.WriteLine("{0}:{1}", nameof(MolarHeatBoiling), MolarHeatBoiling);
                sw.WriteLine("{0}:{1}", nameof(WettingAngle), WettingAngle);
                sw.WriteLine("{0}:{1}", nameof(SurfaceTension), SurfaceTension);
                sw.WriteLine("{0}:{1}", nameof(LaserSpotRadius), LaserSpotRadius);
                sw.WriteLine("{0}:{1}", nameof(Transcalency), Transcalency);
                sw.WriteLine("{0}:{1}", nameof(LaserEmissionAbsorbability), LaserEmissionAbsorbability);
                sw.WriteLine("{0}:{1}", nameof(MeltEmissivity), MeltEmissivity);
                sw.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    throw new DataSaveException();
                }
                throw;
            }
        }

        public override void Load(string path)
        {
            try
            {
                base.Load(path);
                StreamReader sr = new StreamReader(path, System.Text.Encoding.Unicode);
                int count = Int16.Parse(sr.ReadLine());
                while (count-- > 0)
                    sr.ReadLine();
                Dictionary<string, decimal?> records = new Dictionary<string, decimal?>();
                while (!sr.EndOfStream)
                {
                    var record = sr.ReadLine().Split(':');
                    records[record[0]] = decimal.Parse(record[1]);
                }
                Emissivity = records[nameof(Emissivity)];
                HeatCapacity = records[nameof(HeatCapacity)];
                Density = records[nameof(Density)];
                HeatMelting = records[nameof(HeatMelting)];
                HeatBoiling = records[nameof(HeatBoiling)];
                MolarMass = records[nameof(MolarMass)];
                MolarHeatBoiling = records[nameof(MolarHeatBoiling)];
                WettingAngle = records[nameof(WettingAngle)];
                SurfaceTension = records[nameof(SurfaceTension)];
                LaserSpotRadius = records[nameof(LaserSpotRadius)];
                Transcalency = records[nameof(Transcalency)];
                LaserEmissionAbsorbability = records[nameof(LaserEmissionAbsorbability)];
                MeltEmissivity = records[nameof(MeltEmissivity)];
                sr.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException || ex is KeyNotFoundException || ex is IndexOutOfRangeException || ex is FormatException)
                {
                    throw new DataLoadException();
                }
                throw;
            }
        }

        public override string GetExtension() => ".meltinfo";

        public override Dictionary<string, string> ToDictionary()
        {
            var dict = base.ToDictionary();
            dict.Add(Resources.Emissivity, Emissivity.ToString());
            dict.Add(Resources.HeatCapacity, HeatCapacity.ToString());
            dict.Add(Resources.Density, Density.ToString());
            dict.Add(Resources.HeatMelting, HeatMelting.ToString());
            dict.Add(Resources.HeatBoiling, HeatBoiling.ToString());
            dict.Add(Resources.MolarMass, MolarMass.ToString());
            dict.Add(Resources.MolarHeatBoiling, MolarHeatBoiling.ToString());
            dict.Add(Resources.WettingAngle, WettingAngle.ToString());
            dict.Add(Resources.SurfaceTension, SurfaceTension.ToString());
            dict.Add(Resources.LaserSpotRadius, LaserSpotRadius.ToString());
            dict.Add(Resources.Transcalency, Transcalency.ToString());
            dict.Add(Resources.LaserEmissionAbsorbability, LaserEmissionAbsorbability.ToString());
            dict.Add(Resources.MeltEmissivity, MeltEmissivity.ToString());
            return dict;
        }
    }
}
