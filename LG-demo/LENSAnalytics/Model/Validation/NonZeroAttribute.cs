﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Validators
{
    public class NonZeroAttribute: ValidationAttribute
    {
        public override bool IsValid(object obj)
        {
            var value = obj as decimal?;

            if (value == null)
                return false;

            if (value == decimal.Zero)
                return false;

            return true;          
        }
    }
}
