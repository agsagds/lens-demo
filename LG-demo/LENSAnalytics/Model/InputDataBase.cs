﻿using LENSAnalytics.MVVM;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace LENSAnalytics.Model
{
    public abstract class InputDataBase : NotifyModel
    {
        private static string AdditionParametersExtension = ".config";

        public InputDataBase()
        {
            LoadAdditionParameters();
        }
        /// <summary>
        /// Дополнительные параметры
        /// </summary>
        public ObservableCollection<ReservData> AdditionParameters { get; private set; }

        /// <summary>
        /// Сохранение данных в файл
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        public virtual void Save(string path)
        {
            StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Unicode);
            sw.WriteLine("{0}", AdditionParameters.Count);
            foreach (var parameter in AdditionParameters)
                sw.WriteLine("{0}:{1}", parameter.Name, parameter.Value);
            sw.Close();
        }

        /// <summary>
        /// Загрузка данных
        /// </summary>
        /// <param name="input">Поток ввода данных</param>
        public virtual void Load(String path)
        {
            StreamReader sr = new StreamReader(path, System.Text.Encoding.Unicode);
            int count = Int16.Parse(sr.ReadLine());
            AdditionParameters.Clear();
            while (count-- > 0)
            {
                var record = sr.ReadLine().Split(':');
                var parameter = new ReservData();
                parameter.Name = record[0];
                parameter.Value = Decimal.Parse(record[1]);
                AdditionParameters.Add(parameter);
            }
            sr.Close();
        }

        public virtual string GetExtension() => ".datainfo";

        /// <summary>
        /// Возвращает словарь параметров. 
        /// Названия параметров не совпадают с именами полей, получаемыми через nameof(Class.Field)!
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, string> ToDictionary()
        {
            return AdditionParameters
                    .ToDictionary(
                        p => p.Name,
                        p => p.Value.ToString());
        }

        private void LoadAdditionParameters()
        {
            AdditionParameters = new ObservableCollection<ReservData>();
            string path = Resources.ConfigFolderName+"\\"+GetExtension() + AdditionParametersExtension;
            if (!File.Exists(path))
                return;
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.UTF8))
            {
                while (!sr.EndOfStream)
                {
                    AdditionParameters.Add(new ReservData() { Name = sr.ReadLine() });
                }
                sr.Close();
            }
        }
    }
}
