﻿using LENSAnalytics.Model.Calculated;
using LENSAnalytics.Model.Geometry;
using Microsoft.Practices.Unity.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.Containers
{
    public class TempTimeCylindricalPointsContainer
    {
        private const int MaximumPointsInResponse = 100;
        /// <summary>
        /// z->angle->radius range
        /// </summary>
        private Dictionary<float, Dictionary<float, MinMaxValue>> _radiusDict;
        /// <summary>
        /// z->radius range
        /// </summary>
        private Dictionary<float, MinMaxValue> _zRadiusDict;
        /// <summary>
        /// angle->radius range
        /// </summary>
        private Dictionary<float, MinMaxValue> _angleRadiusDict;
        /// <summary>
        /// time->z->angle
        /// </summary>
        private TempTimeCylindricalPoint[] _radiusesStorage;
        private string[] _radiusesStorageKeys =
        {
            nameof(TempTimeCylindricalPoint.Time),
            nameof(TempTimeCylindricalPoint.Z),
            nameof(TempTimeCylindricalPoint.Angle)
        };
        /// <summary>
        /// time->z->radius
        /// </summary>
        private TempTimeCylindricalPoint[] _anglesStorage;
        private string[] _anglesStorageKeys =
        {
            nameof(TempTimeCylindricalPoint.Time),
            nameof(TempTimeCylindricalPoint.Z),
            nameof(TempTimeCylindricalPoint.R)
        };
        /// <summary>
        /// time->angle->radius
        /// </summary>
        private TempTimeCylindricalPoint[] _zStorage;
        private string[] _zStorageKeys =
        {
            nameof(TempTimeCylindricalPoint.Time),
            nameof(TempTimeCylindricalPoint.Angle),
            nameof(TempTimeCylindricalPoint.R)
        };
        /// <summary>
        /// z->angle->radius
        /// </summary>
        private TempTimeCylindricalPoint[] _timesStorage;
        private string[] _timesStorageKeys =
        {
            nameof(TempTimeCylindricalPoint.Z),
            nameof(TempTimeCylindricalPoint.Angle),
            nameof(TempTimeCylindricalPoint.R)
        };

        public TempTimeCylindricalPointsContainer(ref IEnumerable<TempTimeCylindricalPoint> data)
        {
            CreateDictionaries(ref data);
            GenerateStorages(ref data);
        }

        public MinMaxValue GetRadiusRangeByZ(float z)
        {
            var zKeys = _zRadiusDict.Keys.ToArray();
            var zKey = zKeys[BinarySearchFirstOrNearestDown(zKeys, z, 0, zKeys.Count())];
            return _zRadiusDict[zKey];
        }

        public MinMaxValue GetRadiusRangeByAngle(float angle)
        {
            var angleKeys = _angleRadiusDict.Keys.ToArray();
            var angleKey = angleKeys[BinarySearchFirstOrNearestDown(angleKeys, angle, 0, angleKeys.Count())];
            return _angleRadiusDict[angleKey];
        }

        public MinMaxValue GetRadiusRange(float z, float angle)
        {
            var zKeys = _radiusDict.Keys.ToArray();
            var zKey = zKeys[BinarySearchFirstOrNearestDown(zKeys, z, 0, zKeys.Count())];
            var angleKeys = _radiusDict[zKey].Keys.ToArray();
            var angleKey = angleKeys[BinarySearchFirstOrNearestDown(angleKeys, angle, 0, angleKeys.Count())];
            return _radiusDict[zKey][angleKey];
        }

        /*   public IEnumerable<TempTimeCylindricalPoint> GetNearestData(TempTimeCylindricalPoint values, string variableProperyName)
           {
               switch (variableProperyName)
               {
                   case nameof(values.R):
                       return _radiusesStorage
                        .OrderBy(r => Math.Abs(r.Key - values.Time))
                        .First().Value
                        .OrderBy(r => Math.Abs(r.Key - values.Z))
                        .First().Value
                        .OrderBy(r => Math.Abs(r.Key - values.Angle))
                        .First().Value;
                   case nameof(values.Angle):
                       return _anglesStorage
                        .OrderBy(r => Math.Abs(r.Key - values.Time))
                        .First().Value
                        .OrderBy(r => Math.Abs(r.Key - values.Z))
                        .First().Value
                        .OrderBy(r => Math.Abs(r.Key - values.R))
                        .First().Value;
                   case nameof(values.Z):
                       return _zStorage
                        .OrderBy(r => Math.Abs(r.Key - values.Time))
                        .First().Value
                        .OrderBy(r => Math.Abs(r.Key - values.Angle))
                        .First().Value
                        .OrderBy(r => Math.Abs(r.Key - values.R))
                        .First().Value;
                   case nameof(values.Time):
                       return _timesStorage
                        .OrderBy(r => Math.Abs(r.Key - values.Time))
                        .First().Value
                        .OrderBy(r => Math.Abs(r.Key - values.Z))
                        .First().Value
                        .OrderBy(r => Math.Abs(r.Key - values.Angle))
                        .First().Value;
                   default:
                       throw new ArgumentException(string.Format("Unknown '{0}' parameter value: {1}", nameof(variableProperyName), variableProperyName));
               }
           }*/

        public IEnumerable<TempTimeCylindricalPoint> GetNearestData(TempTimeCylindricalPoint values, string variableProperyName, float startR, float endR)
        {
            TempTimeCylindricalPoint[] storage;
            string[] keys;
            switch (variableProperyName)
            {
                case nameof(values.R):
                    storage = _radiusesStorage;
                    keys = _radiusesStorageKeys;
                    break;
                case nameof(values.Angle):
                    storage = _anglesStorage;
                    keys = _anglesStorageKeys;
                    break;
                case nameof(values.Z):
                    storage = _zStorage;
                    keys = _zStorageKeys;
                    break;
                case nameof(values.Time):
                    storage = _timesStorage;
                    keys = _timesStorageKeys;
                    break;
                default:
                    throw new ArgumentException(string.Format("Unknown '{0}' parameter value: {1}", nameof(variableProperyName), variableProperyName));
            }
            var pointer = GetNearestData(ref storage, values, keys, startR, endR);
            var data = SubArray(ref storage, pointer.First, pointer.Second - pointer.First);
            if (data.Count() == 0)
                return data;
            var minValue = data.Min(p => Calculation.GetValueByKey(p, variableProperyName));
            var maxValue = data.Max(p => Calculation.GetValueByKey(p, variableProperyName));
            float eps = (maxValue - minValue) / MaximumPointsInResponse;
            return Calculation.RemoveDuplicates(data, variableProperyName, eps);
        }

        public Pair<int, int> GetNearestData(ref TempTimeCylindricalPoint[] data, TempTimeCylindricalPoint values, string[] keys, float startR, float endR)
        {
            int left, right;
            left = 0;
            right = data.Count();
            foreach (var key in keys)
            {
                if (key == nameof(TempTimeCylindricalPoint.R))
                {
                    left = BinarySearchFirstOrNearestDown(ref data, startR, left, right, key);
                    right = BinarySearchFirstOrNearestDown(ref data, endR, left, right, key);
                }
                else
                {
                    left = BinarySearchFirstOrNearestDown(ref data, Calculation.GetValueByKey(values, key), left, right, key);
                    right = BinarySearchNearestUp(ref data, Calculation.GetValueByKey(data[left], key), left, right, key);
                }
            }

            return new Pair<int, int>(left, right);
        }

        #region Utils
        private TempTimeCylindricalPoint[] SubArray(ref TempTimeCylindricalPoint[] data, int index, int length)
        {
            TempTimeCylindricalPoint[] result = new TempTimeCylindricalPoint[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }



        /// <summary>
        /// Бинарный поиск индекса первого или ближайшего снизу значения в отсортированном по возрастанию массиве
        /// </summary>
        /// <param name="array">Отсортированный по возрастанию массив</param>
        /// <param name="findedValue">Искомое значение</param>
        /// <param name="left">Индекс, указывающий на начало диапазона поиска</param>
        /// <param name="right">Инекс, указывающий на следующий за последним элемент диапазона поиска</param>
        /// <param name="key">Поле, по которому производится поиск</param>
        /// <returns>Индекс первого или ближайшего снизу значения</returns>
        private int BinarySearchFirstOrNearestDown(ref TempTimeCylindricalPoint[] array, float findedValue, int left, int right, string key)
        {
            int inLeft = left, inRight = right;
            while (!(left >= right))
            {
                int mid = left + (right - left) / 2;

                if (Calculation.GetValueByKey(array[left], key) == findedValue)
                    return left;

                if (Calculation.GetValueByKey(array[mid], key) == findedValue)
                {
                    if (mid == left + 1)
                        return mid;
                    else
                        right = mid + 1;
                }

                else if (Calculation.GetValueByKey(array[mid], key) > findedValue)
                    right = mid;
                else
                    left = mid + 1;
            }

            return (left == inLeft) ? left : BinarySearchFirstOrNearestDown(ref array, Calculation.GetValueByKey(array[left - 1], key), inLeft, inRight, key);
        }

        /// <summary>
        /// Бинарный поиск индекса первого или ближайшего снизу значения в отсортированном по возрастанию массиве
        /// </summary>
        /// <param name="array">Отсортированный по возрастанию массив</param>
        /// <param name="findedValue">Искомое значение</param>
        /// <param name="left">Индекс, указывающий на начало диапазона поиска</param>
        /// <param name="right">Инекс, указывающий на следующий за последним элемент диапазона поиска</param>
        /// <returns>Индекс первого или ближайшего снизу значения</returns>
        private int BinarySearchFirstOrNearestDown(float[] array, float findedValue, int left, int right)
        {
            int inLeft = left, inRight = right;
            while (!(left >= right))
            {
                int mid = left + (right - left) / 2;

                if (array[left] == findedValue)
                    return left;

                if (array[mid] == findedValue)
                {
                    if (mid == left + 1)
                        return mid;
                    else
                        right = mid + 1;
                }

                else if (array[mid] > findedValue)
                    right = mid;
                else
                    left = mid + 1;
            }

            return (left == inLeft) ? left : BinarySearchFirstOrNearestDown(array, array[left - 1], inLeft, inRight);
        }

        /// <summary>
        /// Бинарный поиск индекса ближайшего сверху значения(не равного искомому) в отсортированном по возрастанию массиве
        /// </summary>
        /// <param name="array">Отсортированный по возрастанию массив</param>
        /// <param name="findedValue">Искомое значение</param>
        /// <param name="left">Индекс, указывающий на начало диапазона поиска</param>
        /// <param name="right">Инекс, указывающий на следующий за последним элемент диапазона поиска</param>
        /// <param name="key">Поле, по которому производится поиск</param>
        /// <returns>Индекс ближайшего сверху значения(не равного искомому)</returns>
        private int BinarySearchNearestUp(ref TempTimeCylindricalPoint[] array, float findedValue, int left, int right, string key)
        {
            while (!(left >= right))
            {
                int mid = left + (right - left) / 2;

                if (Calculation.GetValueByKey(array[mid], key) > findedValue)
                    right = mid;
                else
                    left = mid + 1;
            }
            return right;
        }
        #endregion


        #region Data prepare
        private void CreateDictionaries(ref IEnumerable<TempTimeCylindricalPoint> data)
        {
            _radiusDict = data
                .GroupBy(p => p.Z,
                             p => p,
                             (key, values) => new { Key = key, Value = values })
                    .ToDictionary(
                        grp => grp.Key,
                        grp => grp.Value
                    .GroupBy(p => p.Angle,
                             p => p,
                             (key, values) => new { Key = key, Value = new MinMaxValue(values.Min(p => p.R), values.Max(p => p.R)) })
                    .ToDictionary(
                        grp2 => grp2.Key,
                        grp2 => grp2.Value));

            _zRadiusDict = data
                .GroupBy(p => p.Z,
                             p => p,
                             (key, values) => new { Key = key, Value = new MinMaxValue(values.Min(p => p.R), values.Max(p => p.R)) })
                             .ToDictionary(
                        grp => grp.Key,
                        grp => grp.Value);

            _angleRadiusDict = data
                .GroupBy(p => p.Angle,
                             p => p,
                             (key, values) => new { Key = key, Value = new MinMaxValue(values.Min(p => p.R), values.Max(p => p.R)) })
                             .ToDictionary(
                        grp => grp.Key,
                        grp => grp.Value);
        }

        private void GenerateStorages(ref IEnumerable<TempTimeCylindricalPoint> data)
        {
            Console.WriteLine("before Memory usage: {0}", GC.GetTotalMemory(true) / 1024 / 1024);
            _radiusesStorage = GenerateStorage(ref data, _radiusesStorageKeys);
            _anglesStorage = GenerateStorage(ref data, _anglesStorageKeys);
            _zStorage = GenerateStorage(ref data, _zStorageKeys);
            _timesStorage = GenerateStorage(ref data, _timesStorageKeys);
            Console.WriteLine("after Memory usage: {0}", GC.GetTotalMemory(false) / 1024 / 1024);
        }

        private TempTimeCylindricalPoint[] GenerateStorage(ref IEnumerable<TempTimeCylindricalPoint> data, string[] keys)
        {
            /*  var dict = new Dictionary<float, Dictionary<float, Dictionary<float, IEnumerable<TempTimeCylindricalPoint>>>>();
             // Console.WriteLine("before Memory usage: {0}", GC.GetTotalMemory(true)/1024/1024);
              int i = 0;
              foreach (var p in data)
              {
                  float firstVal, secondVal, thirdVal, variableVal;
                  firstVal = (float)firstKey.GetValue(p);
                  secondVal = (float)secondKey.GetValue(p);
                  thirdVal = (float)thirdKey.GetValue(p);
                  variableVal = (float)variableKey.GetValue(p);

                  if (!dict.ContainsKey(firstVal))
                      dict.Add(firstVal, new Dictionary<float, Dictionary<float, IEnumerable<TempTimeCylindricalPoint>>>());

                  if (!dict[firstVal].ContainsKey(secondVal))
                      dict[firstVal].Add(secondVal, new Dictionary<float, IEnumerable<TempTimeCylindricalPoint>>());

                  if (!dict[firstVal][secondVal].ContainsKey(thirdVal))
                      dict[firstVal][secondVal].Add(thirdVal, new List<TempTimeCylindricalPoint>());

               //   (dict[firstVal][secondVal][thirdVal] as List<TempTimeCylindricalPoint>).Add(p);

                 // if(++i%(int)1e5==0)
                  //    Console.WriteLine("in progress Memory usage: {0}", GC.GetTotalMemory(false)/1024/1024);
              }           
              return dict;*/
            /*
          return data
                  .GroupBy(p => Calculation.GetValueByKey(p, firstKey),
                           p => p,
                           (key, values) => new { Key = key, Values = values })
                  .ToDictionary(
                      grp => grp.Key,
                      grp => grp.Values
                          .GroupBy(p => Calculation.GetValueByKey(p, secondKey),
                                   p => p,
                                   (key, values) => new { Key = key, Values = values })
                                   .ToDictionary(
                                      grp2 => grp2.Key,
                                      grp2 => grp2.Values.
                                      GroupBy(p => Calculation.GetValueByKey(p, thirdKey),
                                              p => p,
                                              (key, values) => new { Key = key, Values = values })
                                              .ToDictionary(
                                                  grp3 => grp3.Key,
                                                  grp3 => grp3.Values
                  )));*/
            return data
                .OrderBy(p => Calculation.GetValueByKey(p, keys[0]))
                .ThenBy(p => Calculation.GetValueByKey(p, keys[1]))
                .ThenBy(p => Calculation.GetValueByKey(p, keys[2]))
                .ToArray();
        }

        #endregion

    }
}
