﻿using LENSAnalytics.Exceptions;
using LENSAnalytics.Model.Validators;
using LENSAnalytics.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model
{
    public class Laser : InputDataBase
    {
        /// <summary>
        /// Диаметр пучка
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Diameter { get; set; }
        /// <summary>
        /// Мощность пучка
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Power { get; set; }
        /// <summary>
        /// Скорость движения пучка
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Velocity { get; set; }
        /// <summary>
        /// Шаг пучка
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Step { get; set; }
        /// <summary>
        /// Степень расходимости
        /// </summary>
        [Required]
        [NonZeroAttribute]
        public decimal? Dispersion { get; set; }

        public override void Save(string path)
        {
            try
            {
                base.Save(path);
                StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Unicode);
                sw.WriteLine("{0}:{1}", nameof(Diameter), Diameter);
                sw.WriteLine("{0}:{1}", nameof(Power), Power);
                sw.WriteLine("{0}:{1}", nameof(Velocity), Velocity);
                sw.WriteLine("{0}:{1}", nameof(Step), Step);
                sw.WriteLine("{0}:{1}", nameof(Dispersion), Dispersion);
                sw.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    throw new DataSaveException();
                }
                throw;
            }
        }

        public override void Load(string path)
        {
            try
            {
                base.Load(path);
                StreamReader sr = new StreamReader(path, System.Text.Encoding.Unicode);
                int count = Int16.Parse(sr.ReadLine());
                while (count-- > 0)
                    sr.ReadLine();
                Dictionary<string, decimal?> records = new Dictionary<string, decimal?>();
                while (!sr.EndOfStream)
                {
                    var record = sr.ReadLine().Split(':');
                    records[record[0]] = decimal.Parse(record[1]);
                }
                Diameter = records[nameof(Diameter)];
                Power = records[nameof(Power)];
                Velocity = records[nameof(Velocity)];
                Step = records[nameof(Step)];
                Dispersion = records[nameof(Dispersion)];
                sr.Close();
            }
            catch (Exception ex)
            {
                if (ex is IOException || ex is KeyNotFoundException || ex is IndexOutOfRangeException || ex is FormatException)
                {
                    throw new DataLoadException();
                }
                throw;
            }
        }

        public override string GetExtension() => ".laserinfo";

        public override Dictionary<string, string> ToDictionary()
        {
            var dict = base.ToDictionary();
            dict.Add(Resources.Diameter, Diameter.ToString());
            dict.Add(Resources.BeamPower, Power.ToString());
            dict.Add(Resources.BeamVelocity, Velocity.ToString());
            dict.Add(Resources.BeamStep, Step.ToString());
            dict.Add(Resources.Dispersion, Dispersion.ToString());
            return dict;
        }
    }
}
