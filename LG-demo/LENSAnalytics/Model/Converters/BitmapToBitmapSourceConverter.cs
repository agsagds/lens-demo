﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace LENSAnalytics.Model.Converters
{
    public class BitmapToBitmapSourceConverter
    {
        /// <summary>
        /// MSDN states that for Bitmap.GetHbitmap(): You are responsible for calling the GDI DeleteObject method to free the memory used by the GDI bitmap object.
        /// </summary>
        /// <param name="hObject"></param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        public static BitmapSource Convert(ref Bitmap bmp)
        {
            IntPtr hBitmap = bmp.GetHbitmap();
            BitmapSource source;
            try
            {
                source = Imaging.CreateBitmapSourceFromHBitmap(
                hBitmap,
                IntPtr.Zero,
                System.Windows.Int32Rect.Empty,
                BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
            }
            finally
            {
                DeleteObject(hBitmap);
            }

            return source;
        }
    }
}
