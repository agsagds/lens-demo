﻿using LENSAnalytics.Model.Geometry;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.STLModel
{
    /// <summary>
    /// Класс для разбора STL-файла модели
    /// информация о стандарте отсюда: http://www.fabbers.com/tech/STL_Format#Sct_binary
    /// </summary>
    public static class Parser
    {
        public static STLModel ParseModel(string path)
        {
            BinaryReader br = new BinaryReader(File.OpenRead(path), Encoding.ASCII);
            string name = new string(br.ReadChars(80));
            STLModel model = new STLModel(name);
            UInt32 count=br.ReadUInt32();
            while(count>0)
            {
                //read normal
                br.ReadSingle();
                br.ReadSingle();
                br.ReadSingle();
                //read facet
                Vertex[] vertexes=new Vertex[3];
                vertexes[0] = new Vertex(br.ReadSingle(), br.ReadSingle(), br.ReadSingle());
                vertexes[1] = new Vertex(br.ReadSingle(), br.ReadSingle(), br.ReadSingle());
                vertexes[2] = new Vertex(br.ReadSingle(), br.ReadSingle(), br.ReadSingle());
                model.Triangles.Add(new Triangle(vertexes));
                //read atribute bytes
                UInt16 attribBytesCount = br.ReadUInt16();
                while (attribBytesCount > 0)
                    br.ReadByte();
                
                count--;
            }
            return model;
        }
    }
}
