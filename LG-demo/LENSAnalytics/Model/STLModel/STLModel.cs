﻿using LENSAnalytics.Model.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.STLModel
{
    public class STLModel
    {
        public List<Triangle> Triangles { get; private set; }
        public string Name { get; private set; }

        public STLModel(string name)
        {
            Name = name;
            Triangles = new List<Triangle>();            
        }
    }
}
