﻿using LENSAnalytics.Model.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LENSAnalytics.Model.STLModel
{
    /// <summary>
    /// Класс для нарезания модели
    /// использует соображения отсюда: http://geomalgorithms.com/a06-_intersect-2.html
    /// и отсюда: http://programmingcpp.narod.ru/otrezok.htm
    /// </summary>
    public static class Slicer
    {
        /// <summary>
        /// Возвращает набор точек пересечения луча с отрезками на плоскости
        /// </summary>
        /// <param name="rayAngle">угол луча, исходящего из начала координат</param>
        /// <param name="slice">Набор отезков на плоскости</param>
        /// <returns></returns>
        public static IEnumerable<PolarPoint2D> GetRayAndLineSegmentsIntersections(double rayAngle, ref IEnumerable<LineSegment> slice)
        {
            List<PolarPoint2D> intersections = new List<PolarPoint2D>();
            foreach (var line in slice)
            {
                var intersection = GetIntersection(rayAngle, line);
                if (intersection != null)
                    intersections.Add(intersection);
            }
            return intersections;
        }

        /// <summary>
        /// Возвращает набор отрезков, образованных пересечением плоскости и набора треугольников
        /// </summary>
        /// <param name="planeZ">Высота плоскости по оси OZ</param>
        /// <param name="triangles">Треугольники в пространстве</param>
        /// <returns>Набор отрезков</returns>
        public static IEnumerable<LineSegment> GetPlaneAndTrianglesInrersections(double planeZ, List<Triangle> triangles)
        {
            List<LineSegment> segments = new List<LineSegment>();
            foreach (Triangle triangle in triangles)
            {
                var segment = GetZIntersection(planeZ, triangle);
                if (segment != null)
                    segments.Add(segment);
            }
            return segments;
        }


        /// <summary>
        /// Возвращает набор отрезков, образованных осевым сечением набора треугольников
        /// </summary>
        /// <param name="angle">Угол [0..Math.PI)</param>
        /// <param name="triangles">Треугольники в пространстве</param>
        /// <returns>Набор отрезков</returns>
        public static IEnumerable<LineSegment> GetAxialCut(double angle, List<Triangle> triangles)
        {
            List<LineSegment> segments = new List<LineSegment>();
            foreach (Triangle triangle in triangles)
            {
                //произведём преобразование системы координат (поворот на угол angle против часовой стрелки)
                //метод взят отсюда https://ru.wikipedia.org/wiki/Матрица_поворота
                var points = triangle.Vertexes.Select(v=>new Vertex(v)).ToArray();
                foreach(var point in points)
                {
                    var oldX = point.X;
                    var oldY = point.Y;
                    point.X = oldX * Math.Cos(angle) - oldY * Math.Sin(angle);
                    point.Y = oldX * Math.Sin(angle) + oldY * Math.Cos(angle);
                    //также заменим Y на Z т.к. нас интересует сечение XZ
                    var temp = point.Y;
                    point.Y = point.Z;
                    point.Z = temp;
                }
                var segment = GetZIntersection(0, new Triangle(points));
                if (segment != null)
                    segments.Add(segment);
            }
            return segments;
        }

        /// <summary>
        /// Находит отрезок пересечения трeугольника с плоскостью перпендикулярной OZ
        /// </summary>
        /// <param name="planeZ">Высота плоскости по OZ</param>
        /// <param name="triangle">Трeугольник</param>
        /// <returns>Отрезок пересечения фигур</returns>
        private static LineSegment GetZIntersection(double planeZ, Triangle triangle)
        {
            //поиск одинокой вершины (одинокая - единственная по свою сторону от плоскости)
            int i = 0;
            while (OthersidePointCount(planeZ, triangle.Vertexes[i], triangle.Vertexes) != 2)
                if (++i == 3)
                    //если такая не нашлась - отезок вырожденный
                    return null;

            //точка на плоскости
            Point3D zPoint = new Point3D(0, 0, planeZ);
            //вектор от одинокой точки до точки на плоскости
            Vector zVector = new Vector(triangle.Vertexes[i], zPoint);
            //Нормаль к плоскости
            Vector zNormal = new Vector(0, 0, 1);
            //Точки искомого отрезка
            Point3D[] points = new Point3D[2];
            //Переберём отрезки от одинокой вершины до двух других 
            //и определим точки их пересечения с плоскостью
            for (int j = 0; j < 2; j++)
            {
                int otherVertexIndex = (i + j + 1) % 3;
                //Вектор от одинокой вершины до другой вершины
                Vector triVector = new Vector(triangle.Vertexes[i], triangle.Vertexes[otherVertexIndex]);
                double K = zVector.ScalarMul(zNormal) / triVector.ScalarMul(zNormal);
                points[j] = new Point3D(triVector.From.X + triVector.X * K,
                                        triVector.From.Y + triVector.Y * K,
                                        triVector.From.Z + triVector.Z * K);
            }

            return new LineSegment(points[0], points[1]);
        }

        /// <summary>
        /// Подсчет количества точек, лежащих с другой стороны плоскости
        /// </summary>
        /// <param name="planeZ">Высота плоскости по оси OZ</param>
        /// <param name="point">Исходная точка</param>
        /// <param name="points">Набор точек</param>
        /// <returns>Колчество точек, лежащих от исходной по другую сторону плоскости</returns>
        private static int OthersidePointCount(double planeZ, Point3D point, Point3D[] points)
        {
            int count = 0;
            foreach (Point3D p in points)
            {
                var d1 = p.Z - planeZ;
                var d2 = point.Z - planeZ;
                if(d1*d2<=0 && !(d1==0 && d2==0))
                    count++;
            }
            return count;
        }

        /// <summary>
        /// Находит точку пересечения луча с отрезком
        /// </summary>
        /// <param name="rayAngle">угол луча [0;2pi], исходящего из начала координат</param>
        /// <param name="lineSegment">Отрезок</param>
        /// <returns>Пересечение луча с отрезком</returns>
        private static PolarPoint2D GetIntersection(double rayAngle, LineSegment lineSegment)
        {
            if (!HaveIntersection(rayAngle, lineSegment))
                return null;
            double A = lineSegment.P1.Y - lineSegment.P2.Y;
            double B = lineSegment.P2.X - lineSegment.P1.X;
            double C = lineSegment.P1.X * lineSegment.P2.Y - lineSegment.P2.X * lineSegment.P1.Y;
            double D = A * Math.Cos(rayAngle) + B * Math.Sin(rayAngle);
            if (D == 0)
                return null;
            double R = -C / D;
            if (R < 0)
                return null;
            return new PolarPoint2D((float)R, (float)rayAngle);
        }

        /// <summary>
        /// Функция проверки пересечения луча и отрезка
        /// </summary>
        /// <param name="rayAngle">угло луча [0;2pi]</param>
        /// <param name="lineSegment">отезок</param>
        /// <returns></returns>
        private static bool HaveIntersection(double rayAngle, LineSegment lineSegment)
        {
            //перевод точек отрезка в полярные координаты
            var P1 = new PolarPoint2D(lineSegment.P1);
            var P2 = new PolarPoint2D(lineSegment.P2);
            //сортировка по величине радиуса
            if (P1.R > P2.R)
            {
                var temp = P1;
                P1 = P2;
                P2 = temp;
            }
            //Если одна из точек оказалась в начале координат
            if (P1.R == 0)
            {
                //если отрезок совпадает с лучом - считаем что пересечения нет
                if (P2.Angle == rayAngle && P2.R != 0)
                    return false;
                //пересечение есть
                return true;
            }
            //сортировка по величине угла
            if (P1.Angle > P2.Angle)
            {
                var temp = P1;
                P1 = P2;
                P2 = temp;
            }
            //если отрезок совпадает с лучом - считаем что пересечения нет
            if (P1.Angle == rayAngle && P2.Angle == rayAngle)
                return false;
            if (P1.Angle == rayAngle && P2.Angle - Math.PI == rayAngle)
                return false;
            if (P2.Angle == rayAngle && P1.Angle + Math.PI == rayAngle)
                return false;
            //если угол луча между углами точек - пересечение есть
            if ((P1.Angle <= rayAngle && P2.Angle >= rayAngle && P2.Angle-P1.Angle<Math.PI)
                || (!(P1.Angle <= rayAngle && P2.Angle >= rayAngle) && P2.Angle-P1.Angle>Math.PI))
                return true;

            return false;
        }
    }
}
