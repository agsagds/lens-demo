﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Microsoft.Practices.Unity;
using LENSAnalytics.Model.STLModel;

namespace LENSAnalytics
{
    public static class Bootstrapper
    {
        public static void Configure(IUnityContainer container)
        {
            
        }
/*
        public static void ConfigureExceptionsHandling(IServiceLocator serviceLocator)
        {
            var exceptionsService = serviceLocator.ResolveType<IExceptionService>();
            var messageService = serviceLocator.ResolveType<IMessageService>();
            var log = LogManager.GetCurrentClassLogger();

            exceptionsService.Register<FaultException<Proxy.NsiDataService.ServiceFault>>(ex =>
            {
                messageService.ShowErrorAsync(ex);
            }, ex => true);
            exceptionsService.Register<FaultException<ServiceFault>>(ex =>
            {
                messageService.ShowErrorAsync(ex.Message);
            }, ex => true);
            exceptionsService.Register<FaultException<ValidationFault>>(ex =>
            {
                messageService.ShowWarningAsync(ex.Message);
            }, ex => true);
            exceptionsService.Register<FaultException>(ex =>
            {
                log.Error(ex);
                messageService.ShowErrorAsync(string.Format(ErrorResources.UnknownFault, ex.Message));
            }, ex => true);
            exceptionsService.Register<TimeoutException>(ex =>
            {
                log.Error(ex);
                messageService.ShowErrorAsync(ErrorResources.TimeoutError);
            }, ex => true);
            exceptionsService.Register<CommunicationException>(ex =>
            {
                log.Error(ex);
                messageService.ShowErrorAsync(ErrorResources.CommunicationError);
            }, ex => true);
            exceptionsService.Register<Exception>(ex =>
            {
                log.Error(ex);
                messageService.ShowErrorAsync(ErrorResources.UnknownError);
            }, ex => true);
        }*/
    }
}
